package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.bean.QuestionEvaluation;
import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;

/**
 * Classe de test pour le service questionEvaluation
 *
 * @author DOSI
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QuestionEvaluationServiceTest {
	/**
	 * Injection du service Qualificatif
	 */
	@Autowired
	QuestionEvaluationService questionEvaluationService;

	/**
	 * Attribut utilisé pour le test
	 */
	private Long idQuestionEvaluation;
	private Long idRubriqueEvaluation;
	private Long idQuestion;
	private short ordre;
	private String intitule;

	@Ignore
	@Test
	public void addQuestionEvaluation() {
		QuestionEvaluation questionEvaluation = new QuestionEvaluation();
		RubriqueEvaluation rubriqueEvaluation = new RubriqueEvaluation();
		Question question = new Question();
		rubriqueEvaluation.setIdRubriqueEvaluation(idRubriqueEvaluation);
		question.setIdQuestion(idQuestion);
		questionEvaluation.setIdRubriqueEvaluation(rubriqueEvaluation);
		questionEvaluation.setIdQuestion(question);
		questionEvaluation.setOrdre(ordre);
		questionEvaluation = questionEvaluationService.addQuestionEvaluation(questionEvaluation);
		Assert.assertNotNull(questionEvaluation.getIdQuestionEvaluation());
	}

	@Ignore
	@Test
	public void deleteQuestionEvaluation() {
		questionEvaluationService.deleteQuestionEvaluation(idQuestionEvaluation);
		Assert.assertNull(questionEvaluationService.findQuestionEvaluationById(idQuestionEvaluation));
	}

	@Ignore
	@Test
	public void findAllQuestionEvaluation() {
		final Iterable<QuestionEvaluation> questionEvaluations = questionEvaluationService.findAllQuestionEvaluation();
		Assert.assertNotNull(questionEvaluations);
		Assert.assertEquals(29, Iterables.size(questionEvaluations));
	}

	@Before
	public final void init() {
		this.idQuestionEvaluation = (long) 30;
		this.idRubriqueEvaluation = (long) 1;
		this.idQuestion = (long) 1;
		this.ordre = 1;
		this.intitule = "test QuestionEvaluation";
	}

	@Ignore
	@Test
	public void updateQuestionEvaluation() {
		QuestionEvaluation questionEvaluation = questionEvaluationService.findQuestionEvaluationById(34L);
		questionEvaluation.setOrdre((short) 2);
		questionEvaluation.setIntitule(intitule);
		questionEvaluation = questionEvaluationService.updateQuestionEvaluation(questionEvaluation);
		Assert.assertEquals(intitule, questionEvaluation.getIntitule());
	}
}
