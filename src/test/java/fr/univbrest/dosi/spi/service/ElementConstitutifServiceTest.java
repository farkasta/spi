package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.exception.SPIException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ElementConstitutifServiceTest {

	@Autowired
	ElementConstitutifService elementConstitutifService;

	@Test
	public final void deleteectest() {
		final ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("CPR");
		elementConstitutifService.deleteElementConstitutif(elementConstitutifPK);
		Assert.assertNull(elementConstitutifService.getElementConstitutif(elementConstitutifPK));
	}

	@Test
	public void updateectest() {

		final ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("MMM");

		final Enseignant enseignant = new Enseignant();
		final int id = 7;
		enseignant.setNoEnseignant(id);
		enseignant.setNom("Uguen");
		enseignant.setPrenom("Denis");
		enseignant.setType("INT");
		enseignant.setSexe("H");
		enseignant.setAdresse("73 Avenue Le Dantec");
		enseignant.setCodePostal("29200");
		enseignant.setVille("Brest");
		enseignant.setPays("FR");
		enseignant.setMobile("06.42.20.30.13");
		// enseignant.setTelephone("06.67.58.23.00");
		enseignant.setEmailPerso("denis.uguen@univ-brest.fr");
		// enseignant.setEmailUbo("t.t@gmail.com");

		final ElementConstitutif elementConstitutif = new ElementConstitutif();
		elementConstitutif.setElementConstitutifPK(elementConstitutifPK);
		elementConstitutif.setDesignation("t");
		elementConstitutif.setDescription("t");
		elementConstitutif.setNbhCm((short) 1);
		elementConstitutif.setNbhTd((short) 1);
		elementConstitutif.setNbhTp((short) 1);
		elementConstitutif.setNoEnseignant(enseignant);
		try {
			final ElementConstitutif newElementConstitutif = elementConstitutifService.updateEC(elementConstitutif);
			Assert.assertNotNull(elementConstitutif.getElementConstitutifPK());
			Assert.assertEquals(elementConstitutif.getDescription(), newElementConstitutif.getDescription());
			Assert.assertEquals(elementConstitutif.getDesignation(), newElementConstitutif.getDesignation());
			Assert.assertEquals(elementConstitutif.getNbhCm(), newElementConstitutif.getNbhCm());
			Assert.assertEquals(elementConstitutif.getNbhTd(), newElementConstitutif.getNbhTd());
			Assert.assertEquals(elementConstitutif.getNbhTp(), newElementConstitutif.getNbhTp());
			Assert.assertEquals(elementConstitutif.getNoEnseignant(), newElementConstitutif.getNoEnseignant());
		} catch (final SPIException ex) {
			Assert.assertEquals("l element constitutif que vous souhaitez modifier n'exsite pas", ex.getMessage());
		}
	}

	@Test
	public void addectest() {
		final ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("test");

		final Enseignant enseignant = new Enseignant();
		final int id = 7;
		enseignant.setNoEnseignant(id);
		enseignant.setNom("Uguen");
		enseignant.setPrenom("Denis");
		enseignant.setType("INT");
		enseignant.setSexe("H");
		enseignant.setAdresse("73 Avenue Le Dantec");
		enseignant.setCodePostal("29200");
		enseignant.setVille("Brest");
		enseignant.setPays("FR");
		enseignant.setMobile("06.42.20.30.13");
		// enseignant.setTelephone("06.67.58.23.00");
		enseignant.setEmailPerso("denis.uguen@univ-brest.fr");
		// enseignant.setEmailUbo("t.t@gmail.com");

		final ElementConstitutif elementConstitutif = new ElementConstitutif();
		elementConstitutif.setElementConstitutifPK(elementConstitutifPK);
		elementConstitutif.setDesignation("t");
		elementConstitutif.setDescription("t");
		elementConstitutif.setNbhCm((short) 1);
		elementConstitutif.setNbhTd((short) 1);
		elementConstitutif.setNbhTp((short) 1);
		elementConstitutif.setNoEnseignant(enseignant);
		try {
			final ElementConstitutif newElementConstitutif = elementConstitutifService.addElementConstitutif(elementConstitutif);
			Assert.assertNotNull(elementConstitutif.getElementConstitutifPK());
			Assert.assertEquals(elementConstitutif.getDescription(), newElementConstitutif.getDescription());
			Assert.assertEquals(elementConstitutif.getDesignation(), newElementConstitutif.getDesignation());
			Assert.assertEquals(elementConstitutif.getNbhCm(), newElementConstitutif.getNbhCm());
			Assert.assertEquals(elementConstitutif.getNbhTd(), newElementConstitutif.getNbhTd());
			Assert.assertEquals(elementConstitutif.getNbhTp(), newElementConstitutif.getNbhTp());
			Assert.assertEquals(elementConstitutif.getNoEnseignant(), newElementConstitutif.getNoEnseignant());
		} catch (final SPIException ex) {
			Assert.assertEquals("le qualificatif que vous souhaitez ajouter exsite déja ", ex.getMessage());
		}
	}
}
