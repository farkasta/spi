package fr.univbrest.dosi.spi.service;

import java.math.BigInteger;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * Classe de test pour le service unité d'enseignement
 * @author DOSI
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class UniteEnseignementServiceTests {
	/**
	 * Injection du service unité d'enseignement
	 */
	@Autowired
	private UniteEnseignementService uniteEnseignementService;
	
	/**
	 * Attribut utilisé pour le test
	 */
	private UniteEnseignement uniteEnseignement;
	private UniteEnseignementPK uniteEnseignementPK;
	private UniteEnseignementPK uniteEnseignementPKKO;
	private UniteEnseignement uniteEnseignementExiste;

	/**
	 * test de la methode de recherche des unités d'enseignements
	 */
	@Test
	public void findAllUniteEnseignementOK() {
		final List<UniteEnseignement> uniteEnseignements = uniteEnseignementService.findAllUniteEnseignement();
		Assert.assertNotNull(uniteEnseignements);
		Assert.assertEquals(8, Iterables.size(uniteEnseignements));
	}
	
	/**
	 * test de la methode de recherche d'une unité d'enseignement par son Id
	 */
	@Test
	public final void findUniteEnseignementTestOK() {
		final UniteEnseignement uniteEnseignement = uniteEnseignementService.findUniteEnseignement(this.uniteEnseignementPK);
		Assert.assertNotNull(uniteEnseignement);
		Assert.assertEquals(this.uniteEnseignementPK, uniteEnseignement.getUniteEnseignementPK());
	}

	/**
	 * test de la methode de recherche d'une unité d'enseignement par son Id
	 * Test du cas ou l'unité d'enseignement recherchée n'existe pas
	 */
	@Test
	public final void findUniteEnseignementTestKoNotExiste() {
		try {
			uniteEnseignementService.findUniteEnseignement(uniteEnseignementPKKO);
        } catch (SPIException e) {
        	Assert.assertEquals("L'unité d'enseignement que vous cherchez n'existe pas", e.getMessage());
        }
	}
	
	/**
	 * test de la methode d'ajout d'une unité d'enseignement
	 */
	@Test
	public final void addUniteEnseignementOK() {
		final UniteEnseignement newUniteEnseignement = uniteEnseignementService.addUniteEnseignement(uniteEnseignement);
		Assert.assertNotNull(newUniteEnseignement);
		Assert.assertEquals(uniteEnseignement.getUniteEnseignementPK(), newUniteEnseignement.getUniteEnseignementPK());
	}

	/**
	 *test de la methode d'ajout d'une unité d'enseignement
	 *Test du cas ou l'unité d'enseignement existe déjà
	 */
	@Test
	public final void addUniteEnseignementKoExiste() {
		try {
			uniteEnseignementService.addUniteEnseignement(uniteEnseignementExiste);
		} catch (final SPIException ex) {
			Assert.assertEquals("l'unité d'enseignement que vous souhaitez ajouter exsite déjà ", ex.getMessage());
		}
	}

	@Test
	public void updateUniteEnseignementOK() {
		uniteEnseignement.setDesignation("Systeme d'exploitation");
		final UniteEnseignement newUniteEnseignement = uniteEnseignementService.updateUniteEnseignement(uniteEnseignement);
		Assert.assertNotNull(newUniteEnseignement);
		Assert.assertEquals(newUniteEnseignement.getUniteEnseignementPK(), uniteEnseignement.getUniteEnseignementPK());
		Assert.assertEquals(newUniteEnseignement.getDesignation(), uniteEnseignement.getDesignation());
	}

	@Test
	public final void updateUniteEnsignementKoNotExist() {
		try {
			uniteEnseignementService.updateUniteEnseignement(uniteEnseignement);
			Assert.fail();
		} catch (final SPIException ex) {
			Assert.assertEquals("l'unité d'enseignement que vous souhaitez modifier n'exsite pas ", ex.getMessage());
		}
	}

	/**
	 * 
	 */
	@Test
	public final void deleteUniteEnseignementOK() {
		try {
			uniteEnseignementService.deleteUniteEnseignement(uniteEnseignementPKKO);
			Assert.fail();
		} catch (final SPIException ex) {
			Assert.assertEquals("l'unité d'enseignement que vous souhaitez supprimer n'exsite pas ", ex.getMessage());
		}
	}

	/**
	 *
	 */
	@Test
	public final void deleteEnseignantKoNotExist() {
		try {
			uniteEnseignementService.deleteUniteEnseignement(uniteEnseignementPKKO);
		} catch (final SPIException ex) {
			Assert.assertEquals("l'unité d'enseignement que vous souhaitez supprimer n'exsite pas ", ex.getMessage());
		}
	}

	@Before
	public final void init() {
		uniteEnseignementPK = new UniteEnseignementPK("M2DOSI", "ISI");
		uniteEnseignementPKKO = new UniteEnseignementPK("M2DOSI", "LIN");
		uniteEnseignement = new UniteEnseignement(uniteEnseignementPKKO, "Linux", "9");
		uniteEnseignement.setNoEnseignant(new Enseignant(1));
		uniteEnseignementExiste = new UniteEnseignement(uniteEnseignementPK);
		uniteEnseignementExiste.setDesignation("Ingénierie des Systèmes d'Information");
		uniteEnseignementExiste.setNbhCm(new BigInteger("20"));
		uniteEnseignementExiste.setNbhTd((short)20);
		uniteEnseignementExiste.setNbhTp((short)20);
		uniteEnseignementExiste.setSemestre("9");
		uniteEnseignementExiste.setNoEnseignant(new Enseignant(1));
	}
}