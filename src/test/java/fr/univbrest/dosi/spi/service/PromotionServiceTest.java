package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class PromotionServiceTest {

	@Autowired
	PromotionService promotionService;
	PromotionPK promotionPK;
	private final String codeFormation = "M2DOSI";
	private final String anneeUniversitaire = "2014-2015";
	private Promotion promotion;

	/**
	 * cette fonction permet d'afficher toutes les prmotions
	 */

	@Test
	public void findAllPromotion() {
		final List<Promotion> Promotions = promotionService.findAllPromotions();
		Assert.assertNotNull(Promotions);
		Assert.assertTrue(Promotions.size() >= 1);
	}

	/**
	 * cette fonction permet d'afficer les étudiants d'une permotion par ordre alphabétique
	 */
	@Test
	public void findEtudiantByPromotion() {
		PromotionPK promotionPK = new PromotionPK(codeFormation, anneeUniversitaire);
		List<Etudiant> etudiants = promotionService.findEtudiantByPromotion(promotionPK);
		Assert.assertNotNull(etudiants);
		Assert.assertTrue(etudiants.size() >= 1);
	}

	/**
	 * cette méthode permet d'afficher les information d'une promotion
	 */
	@Test
	public void findPromotion() {
		PromotionPK promotionPK = new PromotionPK(codeFormation, anneeUniversitaire);
		final Promotion promotion = promotionService.findPromotion(promotionPK);
		Assert.assertNotNull(promotion);

	}

}