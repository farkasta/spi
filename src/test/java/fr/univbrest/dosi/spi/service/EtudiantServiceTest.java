package fr.univbrest.dosi.spi.service;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.bean.utils.EtudiantPromotion;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class EtudiantServiceTest {

	@Autowired
	EtudiantService etudiantService;

	/**
	 * Test ajout d'un etudiant
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void addEtudiant() {
		Etudiant etudiant = new Etudiant();
		PromotionPK promotionPK = new PromotionPK();
		new EtudiantPromotion(etudiant, promotionPK);
		promotionPK.setCodeFormation("M2DOSI");
		promotionPK.setAnneeUniversitaire("2014-2015");
		etudiant.setNom("Achraf");
		etudiant.setPrenom("LACHGAR");
		etudiant.setAdresse("2 rue des archives");
		etudiant.setCodePostal("29200");
		etudiant.setEmail("elharchaouisamira@gmail.com");
		etudiant.setEmailUbo("elharchaouisamira@univ-brest.fr");
		etudiant.setNationalite("Marocaine");
		etudiant.setNoEtudiant("1234");
		etudiant.setDateNaissance(null);
		etudiant.setVille("Brest");
		etudiant.setMobile("06 64 85 76 53");
		etudiant.setLieuNaissance("Maroc");
		etudiant.setSexe("M");
		etudiant.setPaysOrigine("Maroc");
		etudiant.setUniversiteOrigine("IBN");
		etudiant.setTelephone("06 64 85 76 53");
		etudiant.setDateNaissance(new Date("14/04/90"));
		etudiant.setGroupeAnglais(new BigInteger("1"));
		etudiant.setGroupeTp(new BigInteger("1"));
		// etudiant.setPromotion(promotion);
		etudiant = etudiantService.addEtudiant(etudiant);
		Assert.assertNotNull(etudiant.getNoEtudiant());
	}

	/**
	 * Test suppression d'un etudiant
	 */
	@Test
	public void deleteEtudiant() {

		etudiantService.deletEtudiant("1234");
		Assert.assertNotNull(etudiantService.findEtudiant("934"));
	}

	/**
	 * Test return toute la liste
	 */
	@Test
	public void findAllEtudiant() {
		final List<Etudiant> Etudiants = (List<Etudiant>) etudiantService.listEtudiants();
		Assert.assertNotNull(Etudiants);
		Assert.assertTrue(Etudiants.size() >= 1);
	}
}
