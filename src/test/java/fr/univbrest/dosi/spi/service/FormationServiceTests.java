package fr.univbrest.dosi.spi.service;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.Formation;
import fr.univbrest.dosi.spi.exception.SPIException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class FormationServiceTests {

	/**
	 * Injection du service formation
	 */
	@Autowired
	private FormationService formationService;

	/**
	 *
	 */
	@Test
	public final void addformation() {
		final Formation formation = new Formation();
		formation.setCodeFormation("M2QL");
		formation.setDiplome("M");
		formation.setN0Annee((short) 2);
		formation.setNomFormation("Qualité Logiciel");
		formation.setDoubleDiplome('O');
		formation.setDebutAccreditation(new Date());
		formation.setFinAccreditation(new Date("10/03/16"));
		try {
			final Formation newFormation = formationService.addFormation(formation);
			Assert.assertNotNull(newFormation.getCodeFormation());
			Assert.assertEquals(formation.getCodeFormation(), newFormation.getCodeFormation());
			// Assert.fail();
		} catch (final SPIException ex) {
			Assert.assertEquals("l'formation que vous souhaitez ajouter exsite déja ", ex.getMessage());
		}
		/*
		 * formation newformation = formationService.addformation(formation); Assert.assertNotNull(newformation.getNoformation());
		 * Assert.assertEquals(formation.getNoformation(),newformation.getNoformation());
		 */
	}

	/**
	 * Test Unitaire de la fonction qui nous retourne la liste de toutes les formation
	 */
	@Test
	public void findAllFormationsOK() {
		final Iterable<Formation> formations = formationService.listFormations();
		Assert.assertNotNull(formations);
		Assert.assertEquals(4, Iterables.size(formations));
	}

	/**
	 * test unitaire de la methode de recherche d'une formation par son codeFormation Test du cas ou la formation recherchée n'existe pas
	 */
	@Test
	public final void findFormationTestKoNotExiste() {
		try {
			formationService.existeFormation("M2DOSI");
		} catch (SPIException e) {
			// OK
		}
	}

	/**
	 * test unitaire de la methode de recherche d'une formationn par son code formation
	 */
	@Test
	public final void findFormationTestOK() {
		boolean result = true;
		final boolean test = formationService.existeFormation("M2DOSI");
		Assert.assertEquals(test, result);
	}

	/**
	 * Test unitaire de la fonction qui nous returne la liste de tous les eleùent constitutifs appartennant à une unité d enseignement que son code est passé en paramètre
	 */

	@Test
	public void getElementConstitutifByUETest() {

		final List<ElementConstitutif> elementConstitutifs = formationService.getElementConstitutifByUE("J2EE");
		Assert.assertNotNull(elementConstitutifs);
		Assert.assertEquals(2, elementConstitutifs.size());
	}

	/**
	 * test unitaire de recherche des unite d enseignement par code formation
	 */
	@Test
	public void getUEsByFormationTest() {

		formationService.getUEsByFormation("M2DOSI");
		Assert.assertNotNull(formationService);
		// Assert.assertEquals(8, uniteEnseignements.size());

	}

	@Before
	public final void init() {

	}

	public void setFormationService(FormationService formationService) {
		this.formationService = formationService;
	}

}
