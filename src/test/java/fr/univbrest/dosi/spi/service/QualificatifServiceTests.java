package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * Classe de test pour le service qualificatif
 *
 * @author DOSI
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QualificatifServiceTests {
	/**
	 * Injection du service Qualificatif
	 */
	@Autowired
	private QualificatifService qualificatifService;

	/**
	 * Attribut utilisé pour le test
	 */
	private Long idQualificatif;

	@Test
	public void addQualificatif() {
		final Qualificatif qualificatif = new Qualificatif();
		qualificatif.setMaximal("Max");
		qualificatif.setMinimal("Min");
		try {
			final Qualificatif newQualificatif = qualificatifService.addQualificatif(qualificatif);
			Assert.assertNotNull(newQualificatif.getIdQualificatif());
			Assert.assertEquals(qualificatif.getIdQualificatif(), newQualificatif.getIdQualificatif());
		} catch (final SPIException ex) {
			Assert.assertEquals("le qualificatif que vous souhaitez ajouter exsite déja ", ex.getMessage());
		}
	}

	@Test
	public final void deleteQualificatif() {
		final Long id = (long) 14;
		qualificatifService.deleteQualificatif(id);
		Assert.assertNull(qualificatifService.findQualificatifById(id));
	}

	/**
	 * test de la methode de recherche des enseignants
	 */
	@Test
	public void findAllQualificatifOK() {
		final Iterable<Qualificatif> qualificatifs = qualificatifService.findAllQualificatif();
		Assert.assertNotNull(qualificatifs);
		Assert.assertEquals(13, Iterables.size(qualificatifs));
	}

	/**
	 * test de la methode de recherche d'un Qualificatif par son Id
	 */
	@Test
	public final void findQualificatifTestOK() {
		final Qualificatif qualificatif = qualificatifService.findQualificatifById(idQualificatif);
		Assert.assertNotNull(qualificatif);
		Assert.assertEquals(this.idQualificatif, qualificatif.getIdQualificatif());
	}

	@Before
	public final void init() {
		this.idQualificatif = (long) 1;

	}

	@Test
	public void updateQualificatif() {
		final Qualificatif qualificatif = new Qualificatif();
		qualificatif.setIdQualificatif(idQualificatif);
		qualificatif.setMaximal("Pauvre");
		qualificatif.setMinimal("Riche");
		try {
			final Qualificatif newQualificatif = qualificatifService.updateQualificatif(qualificatif);
			Assert.assertNotNull(newQualificatif.getIdQualificatif());
			Assert.assertEquals(qualificatif.getMaximal(), newQualificatif.getMaximal());
			Assert.assertEquals(qualificatif.getMinimal(), newQualificatif.getMinimal());
		} catch (final SPIException ex) {
			Assert.assertEquals("le qualificatif que vous souhaitez modifier n'exsite pas", ex.getMessage());
		}
	}
}