package fr.univbrest.dosi.spi.service;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class QuestionServiceTest {

	@Autowired
	QuestionService questionService;

	@Test
	// @Ignore
	public void addQuestion() {

		Question question = new Question();
		Qualificatif qualificatif = new Qualificatif();
		question.setIntitule("test Question");
		question.setType("QUS");
		qualificatif.setIdQualificatif((long) 1);
		question.setIdQualificatif(qualificatif);
		question = questionService.addQuestion(question);
		Assert.assertNotNull(question.getIdQuestion());
	}

	@Test
	public void deleteQuestion() {
		questionService.deleteQuestion(29L);
		Assert.assertNotNull(questionService.findQuestionById(29L));
	}

}
