package fr.univbrest.dosi.spi.service;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Iterables;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.exception.SPIException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RubriqueServiceTest {

	@Autowired
	private RubriqueService rubriqueService;

	// @Autowired
	// private String designation;
	//
	// @Autowired
	// private Long idRubrique;

	/**
	 * Test de la methode d'ajout d'une rubrique non existante
	 */
	@Test
	public void addRubrique() {
		Rubrique rubrique = new Rubrique();
		rubrique.setDesignation("chehf");
		rubrique.setType("RBS");
		rubrique.setIdRubrique(0L);

		Rubrique newRubrique = rubriqueService.addRubrique(rubrique);
		Assert.assertNotNull(newRubrique.getIdRubrique());

	}

	/**
	 * Test d'ajout d'une rubrique deja existe
	 */
	@Test
	public void addRubriqueExiste() {
		final Rubrique rubrique = new Rubrique();
		BigInteger order;
		order = new BigInteger("1");

		rubrique.setDesignation("COURS");
		rubrique.setIdRubrique((long) 1);
		rubrique.setOrdre(order);
		rubrique.setType("RBS");

		try {
			rubriqueService.addRubrique(rubrique);
			Assert.fail();
		} catch (final SPIException ex) {
			Assert.assertEquals("la rubrique que vous souhaitez ajouter exsite déja ", ex.getMessage());
		}
	}

	/**
	 * Test suppression d'une rubrique existante
	 */
	@Test
	public final void deleteRubrique() {
		final Long id = (long) 23;
		try {
			rubriqueService.deleteRubrique(id);
		} catch (final SPIException ex) {
			Assert.assertEquals("impossible de supprimer cet rubrique", ex.getMessage());
		}
	}

	/**
	 * Test suppression d'une rubrique non existante
	 */
	@Test
	public final void deleteRubriquetNotExist() {
		final Long id = (long) 9;
		try {
			rubriqueService.deleteRubrique(id);

			Assert.fail();
		} catch (final SPIException ex) {
			Assert.assertEquals("impossible de supprimer cet rubrique", ex.getMessage());
		}
	}

	/**
	 * Test de return toutes les rubriques OK
	 */
	@Test
	public void findAllRubriquesKo() {
		final Iterable<Rubrique> rubriques = rubriqueService.listRubriques();
		Assert.assertNotNull(rubriques);
		Assert.assertNotSame(7, Iterables.size(rubriques));
	}

	/**
	 * Test de return toutes les rubriques KO
	 */
	@Test
	public void findAllRubriquesOK() {
		final Iterable<Rubrique> rubriques = rubriqueService.listRubriques();
		Assert.assertNotNull(rubriques);
		Assert.assertEquals(8, Iterables.size(rubriques));
	}

	/**
	 * Test trouver rubrique existante
	 */
	@Test
	public void findRubriqueExsist() {
		Rubrique rubrique = rubriqueService.getRubrique((long) 1);
		Assert.assertNotNull(rubrique);
		Assert.assertEquals("COURS", rubrique.getDesignation());
	}

	/**
	 * Test trouver rubrique non existante
	 */
	@Test
	public void findRubriqueNoExsist() {
		Rubrique rubrique = rubriqueService.getRubrique((long) 90);
		Assert.assertNull(rubrique);

	}

	// @Before
	// public final void init() {
	//
	// this.idRubrique = (long) 1;
	// this.designation = "COURS";
	//
	// }
}
