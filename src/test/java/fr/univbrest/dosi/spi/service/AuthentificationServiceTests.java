package fr.univbrest.dosi.spi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.univbrest.dosi.spi.Application;
import fr.univbrest.dosi.spi.bean.Authentification;

/**
 * @author DOSI
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class AuthentificationServiceTests {

	@Autowired
	private AuthentificationService authentificationService;

	private String username;

	private String pwd;

	/**
	 * On initie nos variabes de tests
	 */
	@Before
	public final void init() {

		this.username = "adm";
		this.pwd = "dosi";
	}

	/**
	 * On Test le resultat de la fonction VerifySingIn de noter service authentification
	 */
	@Test
	public final void VerifySingIn() {
		final Authentification authentification = authentificationService.VerifySingIn(username, pwd);
		// Assert.assertNotNull(authentification);
		Assert.assertEquals(this.username, authentification.getPseudoConnection());
		Assert.assertEquals(this.pwd, authentification.getMotPasse());
	}
}
