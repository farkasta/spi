package fr.univbrest.dosi.spi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.service.PromotionService;

public class PromotionControllerTest {
	private static PromotionService promotionService = new PromotionService();
	private final Promotion promotion = new Promotion();

	/**
	 * c'est un Test de la methode qui va récuperer toutes les promotions
	 *
	 * @throws ClientProtocolException
	 * @throws IOException
	 */

	@Test
	public final void findAllPromotionTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/Promotion/findAllPromotion");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		final Iterable<Promotion> ens = mapper.readValue(rd, Iterable.class);

		Assert.assertNotNull(ens);

	}

	/**
	 * Test de la méthode qui va récuperer les étudiants d'une promotion
	 *
	 * @throws ClientProtocolException
	 * @throws IOException
	 */

	@Test
	public final void findEtudiantByPromotionTest() throws ClientProtocolException, IOException {
		String codeFormation = "M2DOSI";
		String anneeUniversitaire = "2014-2015";
		final PromotionPK promotionPK = new PromotionPK(codeFormation, anneeUniversitaire);
		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/Promotion/findEtudiantsByPromotion");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(promotionPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		final HttpResponse mockResponse = client.execute(mockRequestPost);
		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper map = new ObjectMapper();
		final Iterable<Etudiant> etudaint = map.readValue(rd, Iterable.class);

		Assert.assertNotNull(etudaint);

	}

	/**
	 * Test de la méthode qui va récuperer les informations d'une promotion
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */

	@Test
	public final void findPromotionTest() throws ClientProtocolException, IOException {
		String codeFormation = "M2DOSI";
		String anneeUniversitaire = "2014-2015";
		final PromotionPK promotionPK = new PromotionPK(codeFormation, anneeUniversitaire);
		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/Promotion/findPromotion");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(promotionPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		final HttpResponse mockResponse = client.execute(mockRequestPost);
		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
		new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		new ObjectMapper();

		// Assert.assertNotNull(ens);

	}

}
