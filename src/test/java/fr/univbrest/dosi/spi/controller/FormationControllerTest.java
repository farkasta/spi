package fr.univbrest.dosi.spi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Formation;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;

public class FormationControllerTest {

	UniteEnseignementPK uniteEnseignementPK = new UniteEnseignementPK("M2DOSI", "ISI");
	UniteEnseignementPK uniteEnseignementPKKO = new UniteEnseignementPK("M2DOSI", "LIN");
	UniteEnseignement uniteEnseignement;
	UniteEnseignement uniteEnseignementExiste;

	@Test
	/**
	 * Test de la methode qui recupere toutes les formations
	 *
	 */
	public final void findAllFormationTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/formations");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		final Iterable<Formation> formations = mapper.readValue(rd, Iterable.class);

		Assert.assertNotNull(formations);

	}

	@Test
	/**
	 * Test de la recherche d'une formation par son codeformation
	 *
	 */
	public void findFormationTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/formation/M2DOSI");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		Formation formation = mapper.readValue(rd, Formation.class);

		Assert.assertNotNull(formation);

	}

	/**
	 * Test de la methode qui recupere toutes les unités d enseignement appartenant à une formation passée en paramètres
	 */

	@Test
	public void findUEsByFormationTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/formation/finduesbyformation/M2DOSI");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		List<Formation> formations = mapper.readValue(rd, List.class);

		Assert.assertNotNull(formations);
	}

	/**
	 * Test de la methode qui recupere tous les elements constitutifs appartenant à une unite d enseignement passée en paramètres
	 */

	@Test
	public void finECByUniteEnseignementTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/formation/findecsbyuniteenseignement/J2EE");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		List<UniteEnseignement> uniteEnseignements = mapper.readValue(rd, List.class);

		Assert.assertNotNull(uniteEnseignements);
	}

	@Test
	/**
	 * Test de la methode qui recupere toutes les unités d'enseignements
	 *
	 */
	public final void findAllUniteEnseignementTest() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/formation/uniteenseignement/findalluniteenseignement");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		final List<UniteEnseignement> uniteEnseignements = mapper.readValue(rd, List.class);

		Assert.assertNotNull(uniteEnseignements);

	}

	@Test
	/**
	 * Test de la recherche d'une unité d'enseignement par son Id (uniteEnseignementPK)
	 *	Test réussi avec Advanced Rest Client mais pas avec JUnit
	 */
	public final void findUniteEnseignementTest() throws ClientProtocolException, IOException {
		UniteEnseignementPK uniteEnseignementPK = new UniteEnseignementPK("M2DOSI", "ISI");
		HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/uniteenseignement/finduniteenseignement");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(uniteEnseignementPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
	}

	/************************************* Tests de la partie EC *************************************************/

	/**
	 * Fonction de test d'ajout
	 *
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void ajouterECTest() throws ClientProtocolException, IOException {

		// l'objet enseignant pour le test
		Enseignant enseignant = new Enseignant();

		// l'objet ec pour le test
		ElementConstitutif elementConstitutif = new ElementConstitutif();

		// l'objet pk de ec de test
		ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("EEE");

		// initiation de l'objet ec de test
		elementConstitutif.setElementConstitutifPK(elementConstitutifPK);
		elementConstitutif.setDescription("description");
		elementConstitutif.setDesignation("designation");
		elementConstitutif.setNoEnseignant(enseignant);
		elementConstitutif.setNbhCm((short) 10);
		elementConstitutif.setNbhTd((short) 10);
		elementConstitutif.setNbhTp((short) 10);

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/ajouterec");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(elementConstitutif);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));

		final HttpResponse mockResponse = client.execute(mockRequestPost);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
		// Création du client et éxécution d'une requete GET

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		UniteEnseignement newUniteEnseignement = mapper.readValue(rd, UniteEnseignement.class);
		Assert.assertNotNull(newUniteEnseignement);
		Assert.assertEquals(newUniteEnseignement.getUniteEnseignementPK(), uniteEnseignementPK);
	}

	@Test
	/**
	 *
	 * Test de l'ajout d'une unité d'enseignement
	 *
	 */
	public void addUniteEnseignementTest() throws ClientProtocolException, IOException {
		uniteEnseignementPKKO = new UniteEnseignementPK("M2DOSI", "LIN");
		uniteEnseignement = new UniteEnseignement(uniteEnseignementPKKO, "Linux", "9");
		uniteEnseignement.setNoEnseignant(new Enseignant(1, "MCF", "H", "SALIOU", "Philippe", "adresse", "29200", "ville", "FR", "06666666", "mail@mail.com"));

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/uniteenseignement/adduniteenseignement");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(uniteEnseignement);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		final HttpResponse mockResponse = client.execute(mockRequestPost);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		UniteEnseignement newUniteEnseignement = mapper.readValue(rd, UniteEnseignement.class);
		Assert.assertNotNull(newUniteEnseignement);
		Assert.assertEquals(newUniteEnseignement.getUniteEnseignementPK(), uniteEnseignementPKKO);
	}

	@Test
	/**
	 *
	 * Test de la modification d'une unité d'enseignement
	 *
	 */
	public void updateUniteEnseignementTest() throws ClientProtocolException, IOException {
		uniteEnseignementPKKO = new UniteEnseignementPK("M2DOSI", "LIN");
		uniteEnseignement = new UniteEnseignement(uniteEnseignementPKKO, "Systeme d exploitation", "10");
		uniteEnseignement.setNoEnseignant(new Enseignant(1));
		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/uniteenseignement/updateuniteenseignement");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(uniteEnseignement);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		final HttpResponse mockResponse = client.execute(mockRequestPost);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
	}

	@Test
	/**
	 * Test de la methode de la suppression
	 *
	 */
	public final void deleteUniteEnseignementTest() throws ClientProtocolException, IOException {
		UniteEnseignementPK uniteEnseignementPK = new UniteEnseignementPK("M2DOSI", "LIN");
		HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/uniteenseignement/deleteuniteenseignement");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(uniteEnseignementPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));

	}

	/**
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public final void findECTest() throws ClientProtocolException, IOException {

		final ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("CPR");
		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/Promotion/findPromotion");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(elementConstitutifPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		final HttpResponse mockResponse = client.execute(mockRequestPost);
		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
		new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		new ObjectMapper();

	}

	@Test
	/**
	 * Test de la methode de la suppression
	 *
	 */
	public final void removeECTest() throws ClientProtocolException, IOException {

		ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeUe("CPR");

		// Création du client et éxécution d'une requete post
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/deleteec/");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(elementConstitutifPK);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));
		client.execute(mockRequestPost);

	}

	@Test
	/**
	 * Test de l'ajout d'un ec
	 */
	public void addecTest() throws ClientProtocolException, IOException {

		final ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		elementConstitutifPK.setCodeFormation("M2DOSI");
		elementConstitutifPK.setCodeUe("J2EE");
		elementConstitutifPK.setCodeEc("MMM");

		final Enseignant enseignant = new Enseignant();
		final int id = 7;
		enseignant.setNoEnseignant(id);
		enseignant.setNom("Uguen");
		enseignant.setPrenom("Denis");
		enseignant.setType("INT");
		enseignant.setSexe("H");
		enseignant.setAdresse("73 Avenue Le Dantec");
		enseignant.setCodePostal("29200");
		enseignant.setVille("Brest");
		enseignant.setPays("FR");
		enseignant.setMobile("06.42.20.30.13");
		// enseignant.setTelephone("06.67.58.23.00");
		enseignant.setEmailPerso("denis.uguen@univ-brest.fr");

		final ElementConstitutif elementConstitutif = new ElementConstitutif();
		elementConstitutif.setElementConstitutifPK(elementConstitutifPK);
		elementConstitutif.setDesignation("t");
		elementConstitutif.setDescription("t");
		elementConstitutif.setNbhCm((short) 1);
		elementConstitutif.setNbhTd((short) 1);
		elementConstitutif.setNbhTp((short) 1);
		elementConstitutif.setNoEnseignant(enseignant);

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/formation/ajouterec");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(elementConstitutif);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));

		final HttpResponse mockResponse = client.execute(mockRequestPost);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
	}

}
