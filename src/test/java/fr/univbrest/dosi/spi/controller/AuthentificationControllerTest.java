package fr.univbrest.dosi.spi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univbrest.dosi.spi.bean.Authentification;

/**
 * @author DOSI
 */
public class AuthentificationControllerTest {

	private final Authentification authentification = new Authentification();

	/**
	 * Test le droit a la connexion d'un utilisateur
	 *
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public final void singInTest() throws ClientProtocolException, IOException {
		// authentification.setIdConnection((long) 1);
		authentification.setPseudoConnection("adm");
		authentification.setMotPasse("dosi");

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/singin");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(authentification);
		mockRequestPost.addHeader("content-type", "application/json");
		mockRequestPost.setEntity(new StringEntity(jsonInString));

		final HttpResponse mockResponse = client.execute(mockRequestPost);

		System.out.println(mockResponse.getStatusLine().getStatusCode());

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
	}

	@Test
	public final void usersTest() throws ClientProtocolException, IOException {

		Authentification authentification_expected = new Authentification();
		authentification_expected.setPseudoConnection("adm");
		authentification_expected.setMotPasse("dosi");
		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090//user");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		new ObjectMapper();

		Assert.assertNotNull(authentification);
		Assert.assertEquals(authentification_expected, authentification);

	}
}
