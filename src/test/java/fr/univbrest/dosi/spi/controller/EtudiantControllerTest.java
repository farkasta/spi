package fr.univbrest.dosi.spi.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import junit.framework.Assert;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.bean.utils.EtudiantPromotion;
import fr.univbrest.dosi.spi.service.EtudiantService;

public class EtudiantControllerTest {

	private static EtudiantService etudiantService = new EtudiantService();

	@Test
	public void addEtudiant() throws ClientProtocolException, IOException, ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		final Etudiant etudiant = new Etudiant();
		Promotion promotion = new Promotion();
		promotion.setPromotionPK(new PromotionPK("M2DOSI", "2014-2015"));
		etudiant.setNom("Achraf");
		etudiant.setPrenom("LACHGAR");
		etudiant.setAdresse("2 rue des archives");
		etudiant.setCodePostal("29200");
		etudiant.setEmail("elharchaouisamira@gmail.com");
		etudiant.setEmailUbo("elharchaouisamira@univ-brest.fr");
		etudiant.setNationalite("Marocaine");
		etudiant.setNoEtudiant("1234");
		// etudiant.setDateNaissance(null);
		etudiant.setVille("Brest");
		etudiant.setMobile("06 64 85 76 53");
		etudiant.setLieuNaissance("Maroc");
		etudiant.setSexe("M");
		etudiant.setPaysOrigine("FR");
		etudiant.setUniversiteOrigine("IBN");
		etudiant.setTelephone("06 64 85 76 53");
		etudiant.setDateNaissance(sdf.parse("01/01/1990"));
		// etudiant.setGroupeAnglais(new BigInteger("1"));
		// etudiant.setGroupeTp(new BigInteger("1"));
		// etudiant.setPromotion(promotion);
		// etudiant.setAuthentificationCollection(new ArrayList<Authentification>());
		// etudiant.setCodePostal("44444");
		// etudiant.setLieuNaissance("ddd");
		// etudiant.setReponseEvaluationCollection(new ArrayList<ReponseEvaluation>());

		EtudiantPromotion etudiantPromotion = new EtudiantPromotion();
		etudiantPromotion.setEtudiant(etudiant);
		etudiantPromotion.setPromotionPK(new PromotionPK("M2DOSI", "2014-2015"));

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpPost mockRequestPost = new HttpPost("http://localhost:8090/etudiants/addetudiant");
		final ObjectMapper mapper = new ObjectMapper();
		final com.fasterxml.jackson.databind.ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		final String jsonInString = ow.writeValueAsString(etudiantPromotion);
		mockRequestPost.addHeader("content-type", "application/json;charset=UTF-8");
		mockRequestPost.setEntity(new StringEntity(jsonInString));

		final HttpResponse mockResponse = client.execute(mockRequestPost);

		// Le code retour HTTP doit être un succès (200)
		Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());
	}

	@Test
	public void deleteEtudiant() throws ClientProtocolException, IOException {

		// Création du client et éxécution d'une requete GET
		final HttpClient client = HttpClientBuilder.create().build();
		final HttpGet mockRequest = new HttpGet("http://localhost:8090/etudiant/delete/1234");
		final HttpResponse mockResponse = client.execute(mockRequest);

		// Le code retour HTTP doit être un succès (200)
		// Assert.assertEquals(200, mockResponse.getStatusLine().getStatusCode());

		final BufferedReader rd = new BufferedReader(new InputStreamReader(mockResponse.getEntity().getContent()));
		final ObjectMapper mapper = new ObjectMapper();
		final Iterable<Etudiant> etudiant = mapper.readValue(rd, Iterable.class);
		Assert.assertNotNull(etudiant);
	}
}
