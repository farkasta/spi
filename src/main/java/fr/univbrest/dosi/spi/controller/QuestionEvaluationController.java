package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.QuestionEvaluation;
import fr.univbrest.dosi.spi.service.QuestionEvaluationService;

@RestController
public class QuestionEvaluationController {

	@Autowired
	private QuestionEvaluationService questionEvaluationService;

	@RequestMapping(value = "/questionevaluation/findallquestionevaluation", produces = { "application/json;charset=UTF-8" })
	public List<QuestionEvaluation> findAllQuestionEvaluation() {
		return questionEvaluationService.findAllQuestionEvaluation();
	}

}
