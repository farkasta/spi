package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.service.PromotionService;

/**
 * @author DOSI
 */
@RestController
public class PromotionController {

	/**
	 * initialisation du promotion service
	 */
	@Autowired
	PromotionService promotionService;

	@RequestMapping(value = "/promotions/addpromotion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Promotion addPromotion(@RequestBody Promotion promotion) {
		return promotionService.addPromotion(promotion);
	}

	/**
	 * une fonction qui permet recuperer la liste de tous le promotions depuis une appel rest
	 *
	 * @return une liste qui contient les promotions
	 */
	@RequestMapping(value = "/Promotion/findAllPromotion")
	public List<Promotion> findAllPromotion() {
		return promotionService.findAllPromotions();
	}

	/**
	 * une fonction qui permet de recuperer la liste des etudiants d'une promotions donné
	 *
	 * @param promotionPK
	 * @return la liste des etudiants obtenue
	 */
	@RequestMapping(value = "/Promotion/findEtudiantsByPromotion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public List<Etudiant> findEtudiantByPromotion(@RequestBody PromotionPK promotionPK) {
		return promotionService.findEtudiantByPromotion(promotionPK);
	}

	/**
	 * une fonction permet de trouvé une promotion par son cle composé promotionPK
	 *
	 * @param promotionPK
	 *            clé composé
	 * @return
	 */
	@RequestMapping(value = "/Promotion/findPromotion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Promotion findPromotion(@RequestBody PromotionPK promotionPK) {
		return promotionService.findPromotion(promotionPK);
	}

	@RequestMapping(value = "/Promotion/findPromotion/{codeFormation}/{anneuniversitaire}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Promotion findPromotions(@PathVariable("codeFormation") String codeFormation, @PathVariable("anneuniversitaire") String anneuniversitaire) {
		PromotionPK promotionPK = new PromotionPK(codeFormation, anneuniversitaire);
		return promotionService.findPromotion(promotionPK);
	}

	@RequestMapping(value = "/promotions/updatepromotion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Promotion updatePromotion(@RequestBody Promotion promotion) {
		return promotionService.updatePromotion(promotion);
	}

	@RequestMapping(value = "/promotions/deletepromotion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" })
	public void deletePromotion(@RequestBody PromotionPK promotionPK) {
		promotionService.deletePromotion(promotionPK);
	}

}
