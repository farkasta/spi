package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.EnseignantService;
import fr.univbrest.dosi.spi.service.PromotionService;
import fr.univbrest.dosi.spi.service.UniteEnseignementService;

/**
 * Classe controleur de la partie gestion des enseignants
 *
 * @author DOSI
 */

@RestController
public class EnseignantController {
	/**
	 * Injection de enseignantService
	 */
	@Autowired
	private EnseignantService enseignantService;

	/**
	 * Injection de promotionService
	 */
	@Autowired
	private PromotionService promotionService;

	/**
	 * Injection de uniteEnseignementService
	 */
	@Autowired
	private UniteEnseignementService uniteEnseignementService;

	/**
	 * Methode d'ajout d'un enseignant
	 *
	 * @param enseignant
	 *            l'entité de l'enseignant à ajouter
	 * @return le message d'ajout
	 */
	@RequestMapping(value = "/enseignant/addenseignant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public String addEnseignant(@RequestBody Enseignant enseignant) {
		enseignantService.addEnseignant(enseignant);
		return "l'enseignant " + enseignant.getNom() + " " + enseignant.getPrenom() + " est ajouté";
	}

	/**
	 * Methode de suppression d'un enseignant
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant à supprimer
	 */
	@RequestMapping(value = "/enseignant/deleteenseignant/{noenseignant}")
	public void deleteEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		enseignantService.deleteEnseignant(noEnseignant);
	}

	/**
	 * Methode de test de l'existance d'un enseignant
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return resultat du test
	 */
	@RequestMapping(value = "/existenseignant/{noenseignant}", consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Boolean existEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		return enseignantService.existEnseignant(noEnseignant);
	}

	/**
	 * Methode de recuperation de tous les enseignants
	 *
	 * @return liste des enseignant
	 */
	@RequestMapping("/enseignant/findallenseignant")
	public Iterable<Enseignant> findAllEnseignant() {
		Iterable<Enseignant> enseignants = enseignantService.findAllEnseignant();
		if (enseignants != null)
			return enseignants;
		else
			throw new SPIException("Aucun enseignant n'est trouvé");
	}

	/**
	 * Methode de recherche d'un enseignant
	 *
	 * @param id
	 *            l'id de l'enseignant à rechercher
	 * @return l'enseignant
	 */
	@RequestMapping(value = "/enseignant/findenseignant/{noenseignant}", produces = { "application/json;charset=UTF-8" })
	public Enseignant findEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		Enseignant enseignant = enseignantService.findEnseignant(noEnseignant);
		if (enseignant != null)
			return enseignant;
		else
			throw new SPIException("L'enseignant que vous cherchez n'existe pas");
	}

	/**
	 * Methode de recuperation des promotion qui ont pour responsable l'enseignant en parametre
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return liste des promotions
	 */
	@RequestMapping(value = "/enseignant/findpromotionsbyenseignant/{noenseignant}")
	public List<Promotion> findPromotionsByEnseignant(@PathVariable(value = "noenseignant") Integer noEnseignant) {
		List<Promotion> promotions = promotionService.getPromotionByEnseignant(noEnseignant);
		if (promotions != null)
			return promotions;
		else
			throw new SPIException("Les details de cet enseignant ne se trouvent pas");
	}

	/**
	 * Methode de recuperation des Unités d'enseignement qui ont pour responsable l'enseignant en parametre
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return liste des unite enseignant
	 */
	@RequestMapping("/enseignant/finduesbyenseignant/{noenseignant}")
	public List<UniteEnseignement> findUEsByEnseignant(@PathVariable("noenseignant") Integer noEnseignant) {
		List<UniteEnseignement> uniteEnseignements = uniteEnseignementService.findUEsByEnseignant(noEnseignant);
		if (uniteEnseignements != null)
			return uniteEnseignements;
		else
			throw new SPIException("Les details de cet enseignant ne se trouvent pas");
	}

	public EnseignantService getEnseignantService() {
		return enseignantService;
	}

	public PromotionService getPromotionService() {
		return promotionService;
	}

	public UniteEnseignementService getUniteEnseignementService() {
		return uniteEnseignementService;
	}

	public void setEnseignantService(final EnseignantService enseignantService) {
		this.enseignantService = enseignantService;
	}

	public void setPromotionService(final PromotionService promotionService) {
		this.promotionService = promotionService;
	}

	public void setUniteEnseignementService(final UniteEnseignementService uniteEnseignementService) {
		this.uniteEnseignementService = uniteEnseignementService;
	}

	/**
	 * Methode de modicafication d'un enseignant
	 *
	 * @param enseignant
	 *            objet enseignant modifié
	 * @return message de modification
	 */
	@RequestMapping(value = "/enseignant/updateenseignant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public String updateEnseignant(@RequestBody Enseignant enseignant) {
		enseignantService.updateEnseignant(enseignant);
		return "l'enseignant " + enseignant.getNom() + " " + enseignant.getPrenom() + " est modifié";
	}
}