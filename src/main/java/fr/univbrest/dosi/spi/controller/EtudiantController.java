package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.utils.EtudiantPromotion;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.EtudiantService;
import fr.univbrest.dosi.spi.service.PromotionService;

@RestController
public class EtudiantController {

	@Autowired
	private EtudiantService etudiantService;

	@Autowired
	private PromotionService promotionService;

	/**
	 * Ajout d'un etudiant
	 *
	 * @param etudiant
	 * @return
	 */
	@RequestMapping(value = "/etudiants/addetudiant", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Etudiant ajouterEtudiant(@RequestBody EtudiantPromotion etudiantPromotion) {

		Etudiant etudiant = etudiantPromotion.getEtudiant();
		Promotion promotion = promotionService.findPromotion(etudiantPromotion.getPromotionPK());
		etudiant.setPromotion(promotion);

		return etudiantService.addEtudiant(etudiant);
	}

	/**
	 * modification d'un etudiant
	 *
	 * @param etudiant
	 * @return
	 */
	@RequestMapping(value = "/etudiants/update", method = RequestMethod.POST, headers = "Accept=application/json")
	public Etudiant editEtudiant(@RequestBody EtudiantPromotion etudiantPromotion) {

		Etudiant etudiant = etudiantPromotion.getEtudiant();
		Promotion promotion = promotionService.findPromotion(etudiantPromotion.getPromotionPK());
		etudiant.setPromotion(promotion);
		return etudiantService.updateEtudiant(etudiant);
	}

	/**
	 * Return la liste de tous les etudiants
	 *
	 * @return
	 */
	@RequestMapping("/etudiant/findalletudiant")
	public Iterable<Etudiant> findAllEtudiant() {
		Iterable<Etudiant> etudiants = etudiantService.listEtudiants();
		if (etudiants != null)
			return etudiants;
		else
			throw new SPIException("Aucun etudiant n'est trouvé");
	}

	/**
	 * Chercher un etudiant
	 *
	 * @param noEtudiant
	 * @return
	 */
	@RequestMapping(value = "/etudiant/findetudiant/{noEtudiant}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Etudiant findEtudiant(@PathVariable(value = "noEtudiant") String noEtudiant) {
		Etudiant etudiant = etudiantService.findEtudiant(noEtudiant);
		if (etudiant != null)
			return etudiant;
		else
			throw new SPIException("L'etudiant que vous cherchez n'existe pas");
	}

	@RequestMapping(value = "/etudiant/findpromotionbyetudiant/{noetudiant}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Promotion findPromotionbyEtudiant(@PathVariable("noetudiant") String noEtudiant) {
		Etudiant etudiant = etudiantService.findEtudiant(noEtudiant);
		if (etudiant != null)
			return etudiant.getPromotion();
		else
			throw new SPIException("L'etudiant que vous cherchez n'existe pas");
	}

	/**
	 * Suppression d'un etudiant
	 *
	 * @param noEtudiant
	 */
	@RequestMapping(value = "/etudiant/delete/{noEtudiant}", headers = "Accept=application/json")
	public void removeEtudiant(@PathVariable("noEtudiant") String noEtudiant) {
		etudiantService.deletEtudiant(noEtudiant);
	}
}
