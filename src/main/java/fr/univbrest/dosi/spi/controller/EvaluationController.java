package fr.univbrest.dosi.spi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.bean.QuestionEvaluation;
import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;
import fr.univbrest.dosi.spi.bean.utils.EtudiantEvaluation;
import fr.univbrest.dosi.spi.bean.utils.QuestionRubriqueEvaluation;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.EvaluationService;
import fr.univbrest.dosi.spi.service.QuestionEvaluationService;
import fr.univbrest.dosi.spi.service.RubriqueEvaluationService;
import fr.univbrest.dosi.spi.service.RubriqueService;

/**
 * @author DOSI
 */
@RestController
public class EvaluationController {

	/**
	 *
	 */
	@Autowired
	EvaluationService evaluationService;
	@Autowired
	RubriqueService rubriqueService;
	@Autowired
	RubriqueEvaluationService rubriqueEvaluationService;
	@Autowired
	QuestionEvaluationService questionEvaluationService;

	@RequestMapping(value = "/evaluation/addevaluation", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Evaluation addEvaluation(@RequestBody Evaluation evaluation) {
		System.out.println(evaluation.getCodeUe());
		return evaluationService.addEvaluation(evaluation);
	}

	@RequestMapping(value = "/evaluation/addQuestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public QuestionEvaluation addQuestionEvaluation(@RequestBody final QuestionEvaluation questionEvaluation) {
		return questionEvaluationService.addQuestionEvaluation(questionEvaluation);
	}

	@RequestMapping(value = "/evaluation/addQuestionRubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public QuestionEvaluation addQuestionEvaluation2(@RequestBody final QuestionRubriqueEvaluation questionEvaluationTemp) {
		QuestionEvaluation questionEvaluation = questionEvaluationTemp.getQuestionEvaluation();
		RubriqueEvaluation rubriqueEvaluation = rubriqueEvaluationService.findRubriqueEvaluation(questionEvaluationTemp.getIdRubriqueEvaluation());
		questionEvaluation.setIdRubriqueEvaluation(rubriqueEvaluation);
		return questionEvaluationService.addQuestionEvaluation(questionEvaluation);
	}

	@RequestMapping(value = "/evaluation/addRubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public RubriqueEvaluation addRubriqueEvaluation(@RequestBody final RubriqueEvaluation rubriqueEvaluation) {
		return rubriqueEvaluationService.addRubriqueEvaluation(rubriqueEvaluation);
	}

	@RequestMapping(value = "/evaluation/deleteevaluation/{idevaluation}", method = RequestMethod.GET)
	public void deleteEvaluation(@PathVariable("idevaluation") Long idEvaluation) {
		try {
			evaluationService.deleteEvaluation(idEvaluation);
		} catch (Exception e) {
			throw new SPIException("Veuillez dabors vider l'évaluation");
		}
	}

	@RequestMapping(value = "/evaluation/deletequestionevaluation/{idquestionevaluation}")
	public void deleteQuestionEvaluation(@PathVariable("idquestionevaluation") Long idQuestionEvaluation) {
		questionEvaluationService.deleteQuestionEvaluation(idQuestionEvaluation);
	}

	@RequestMapping(value = "/evaluation/deleterubrique/{idrubriqueevaluation}")
	public void deleteRubriqueEvaluation(@PathVariable("idrubriqueevaluation") Long idRubriqueEvaluation) {
		rubriqueEvaluationService.deleteRubriqueEvaluation(idRubriqueEvaluation);
	}

	/**
	 * @return
	 */
	@RequestMapping(value = "/evaluation/findallevaluation", method = RequestMethod.GET)
	public List<Evaluation> findAllEvaluation() {
		return evaluationService.findAllEvaluation();
	}

	@RequestMapping(value = "/evaluation/findallevaluationbynoenseignant/{noenseignant}", method = RequestMethod.GET)
	public List<Evaluation> findAllEvaluationByNoEnseignant(@PathVariable("noenseignant") Integer noEnseignant) {
		return evaluationService.findAllEvaluationByNoEnseignant(noEnseignant);
	}

	@RequestMapping(value = "/evaluation/findquestionevaluation")
	public List<QuestionEvaluation> findAllQuestionEvaluation() {
		return questionEvaluationService.findAllQuestionEvaluation();
	}

	@RequestMapping(value = "/evaluation/findrubriques/{idevaluation}", produces = { "application/json;charset=UTF-8" })
	public List<RubriqueEvaluation> findAllRubriqueEvaluation(@PathVariable("idevaluation") Long idEvaluation) {
		List<QuestionEvaluation> listeQuestion = new ArrayList<QuestionEvaluation>();
		List<RubriqueEvaluation> allRubriqueEvalution = rubriqueEvaluationService.findAllRubriqueEvaluation(idEvaluation);
		for (int i = 0; i < allRubriqueEvalution.size(); i++) {
			listeQuestion = questionEvaluationService.findQuestionEvaluationByIdRubriqueEvaluation(allRubriqueEvalution.get(i).getIdRubriqueEvaluation());
			allRubriqueEvalution.get(i).setQuestionEvaluationCollection(listeQuestion);
			System.out.println("taille des question dans chaque rubrique " + allRubriqueEvalution.get(i).getQuestionEvaluationCollection().size());
		}

		return allRubriqueEvalution;
	}

	@RequestMapping(value = "/evaluation/findallevaluationbyetudiant", produces = { "application/json;charset=UTF-8" })
	public List<EtudiantEvaluation> findAllRubriqueEvaluation(@RequestBody PromotionPK promotionPK) {
		return evaluationService.findEvaluationByPromotion(promotionPK);
	}

	@RequestMapping(value = "/evaluation/findrubriquesquestionbyidrubrique/{idRubriqueEvaluation}", produces = { "application/json;charset=UTF-8" })
	public List<QuestionEvaluation> findAllRubriquequestion(@PathVariable("idRubriqueEvaluation") Long idRubriqueEvaluation) {
		return questionEvaluationService.findQuestionEvaluationByIdRubriqueEvaluation(idRubriqueEvaluation);
	}

	/**
	 * @param idEvaluation
	 * @return
	 */
	@RequestMapping(value = "/evaluation/findevaluationbyid/{idevaluation}")
	public Evaluation findEvaluationById(@PathVariable("idevaluation") Long idEvaluation) {
		return evaluationService.findEvaluationById(idEvaluation);
	}

	@RequestMapping(value = "/evaluation/updateevaluation", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Evaluation updateEvaluation(@RequestBody Evaluation evaluation) {
		System.out.println(evaluation.getCodeUe());
		return evaluationService.updateEvaluation(evaluation);
	}

	@RequestMapping(value = "/evaluation/updateRubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public RubriqueEvaluation updateRubriqueEvaluation(@RequestBody final RubriqueEvaluation rubriqueEvaluation) {
		return rubriqueEvaluationService.updateRubriqueEvaluation(rubriqueEvaluation);
	}

}
