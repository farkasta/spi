package fr.univbrest.dosi.spi.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Authentification;
import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.AuthentificationService;
import fr.univbrest.dosi.spi.service.EnseignantService;

/**
 * @author DOSI Controlleur Rest d'authentification
 */
@RestController
public class AuthentificationController {

	/**
	 * Initiation de notre service d'authetification
	 */
	@Autowired
	private AuthentificationService userService;

	@Autowired
	private EnseignantService enseignantService;

	@RequestMapping(value = "/user/etudiant/{idConnection}")
	public Etudiant findEtudiant(@PathVariable("idConnection") Long idConnection) {

		return userService.findEnseignantByIdConnection(idConnection).getNoEtudiant();

	}

	@RequestMapping(value = "/user/enseignant/{idConnection}")
	public Enseignant findUser(@PathVariable("idConnection") Long idConnection) {

		return userService.findEnseignantByIdConnection(idConnection).getNoEnseignant();

	}

	/**
	 * Cette fonction nous Teste la validité des login et pwd fournis lors de l authentification si informations valides on ouvre une session de connexion sinon un message d erreur est retourné. Elle
	 * prend en paramètres l'utilisateur qui tente de se connecté
	 *
	 * @param request
	 * @param user
	 */
	@RequestMapping(value = "/singin", method = RequestMethod.POST, headers = "Accept=application/json")
	public void singIn(final HttpServletRequest request, @RequestBody final Authentification user) {
		final Authentification users = userService.VerifySingIn(user.getLoginConnection(), user.getMotPasse());
		if (users != null) {
			request.getSession().setAttribute("user", users);
		} else {
			request.getSession().removeAttribute("user");
			throw new SPIException("impossible de s'authentifier");
		}
	}

	/**
	 * Cette fonction ferme la session de connexion actuellement ouverte.
	 *
	 * @param request
	 */
	@RequestMapping(value = "/singout", method = RequestMethod.GET)
	public void singOut(final HttpServletRequest request) {
		request.getSession().removeAttribute("user");
	}

	/**
	 * Cette fonction nous permet de retourner l'utilisateur connecté dans la session courantes
	 *
	 * @param request
	 * @param response
	 * @return on recupere l'utilisateur connecté
	 */
	@RequestMapping(value = "/user")
	public Authentification users(final HttpServletRequest request, final HttpServletResponse response) {
		Authentification user = (Authentification) request.getSession().getAttribute("user");
		return user;
	}
}
