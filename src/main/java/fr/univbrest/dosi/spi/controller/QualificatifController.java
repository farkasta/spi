package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.QualificatifService;

/**
 * Classe controleur de la partie gestion des qualificatifs
 *
 * @author DOSI
 */

@RestController
public class QualificatifController {
	/**
	 * Injection de qualificatifService
	 */
	@Autowired
	private QualificatifService qualificatifService;

	/**
	 * Methode d'ajout d'un qualificatif
	 *
	 * @param qualificatif
	 *            l'entité qualificatif à ajouter
	 * @return le message d'ajout
	 */
	@RequestMapping(value = "/qualificatif/addqualificatif", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Qualificatif addQualificatif(@RequestBody Qualificatif qualificatif) {
		return qualificatifService.addQualificatif(qualificatif);
	}

	/**
	 * Methode de suppression d'un qualificatif
	 *
	 * @param idQualificatif
	 *            l'id du qualificatif à supprimer
	 */
	@RequestMapping(value = "/qualificatif/deletequalificatif/{idqualificatif}")
	public void deleteQualificatif(@PathVariable(value = "idqualificatif") Long idQualificatif) {
		try {
			qualificatifService.deleteQualificatif(idQualificatif);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les questions associées");
		}

	}

	/**
	 * Methode de recuperation de tous les qualificatifs
	 *
	 * @return liste des qualificatifs
	 */
	@RequestMapping("/qualificatif/findallqualificatif")
	public Iterable<Qualificatif> findAllQualificatif() {
		Iterable<Qualificatif> qualificatifs = qualificatifService.findAllQualificatif();
		if (qualificatifs != null)
			return qualificatifs;
		else
			throw new SPIException("Aucun qualificatif n'est trouvé");
	}

	/**
	 * Methode de recherche d'un qualificatif
	 *
	 * @param id
	 *            l'id du qualificatif à rechercher
	 * @return qualificatif
	 */
	@RequestMapping(value = "/qualificatif/findqualificatifbyid/{idqualificatif}", produces = { "application/json;charset=UTF-8" })
	public Qualificatif findQualificatif(@PathVariable(value = "idqualificatif") Long idQualificatif) {
		Qualificatif qualificatif = qualificatifService.findQualificatifById(idQualificatif);
		if (qualificatif != null)
			return qualificatif;
		else
			throw new SPIException("Le qualificatif que vous cherchez n'existe pas");
	}

	public void setQualificatifService(final QualificatifService qualificatifService) {
		this.qualificatifService = qualificatifService;
	}

	/**
	 * Methode de modification d'un qualificatif
	 *
	 * @param qualificatif
	 *            objet qualificatif modifié
	 * @return message de modification
	 */
	@RequestMapping(value = "/qualificatif/updatequalificatif", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Qualificatif updateQualificatif(@RequestBody Qualificatif qualificatif) {
		return qualificatifService.updateQualificatif(qualificatif);
	}

}