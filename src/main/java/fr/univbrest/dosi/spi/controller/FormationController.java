package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.bean.Formation;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.ElementConstitutifService;
import fr.univbrest.dosi.spi.service.FormationService;
import fr.univbrest.dosi.spi.service.UniteEnseignementService;

/**
 * @author DOSI
 */
@RestController
public class FormationController {
	/**
	 * Injection de notre service formation
	 */
	@Autowired
	private FormationService formationService;
	/**
	 * Injection du service unite d'enseignement
	 */
	@Autowired
	UniteEnseignementService uniteEnseignementService;

	@Autowired
	ElementConstitutifService elementConstitutifService;

	/**
	 * @param formation
	 *            l'entité de formation
	 * @return une formation
	 */
	@RequestMapping(value = "/formation/uniteenseignement/adduniteenseignement", method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public UniteEnseignement addUniteEnseignement(@RequestBody UniteEnseignement uniteEnseignement) {
		UniteEnseignement newUniteEnseignement = uniteEnseignementService.addUniteEnseignement(uniteEnseignement);
		if (newUniteEnseignement != null)
			return newUniteEnseignement;
		else
			throw new SPIException("L'unité d'enseignement que vous voulez ajouter existe déjà");
	}

	@RequestMapping(value = "/formation/ajouterec", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public ElementConstitutif ajouterEC(@RequestBody ElementConstitutif elementConstitutif) {
		return elementConstitutifService.addElementConstitutif(elementConstitutif);
	}

	@RequestMapping(value = "/formation/ajouterformation", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public final Formation ajouterFormation(@RequestBody final Formation formation) {
		return formationService.addFormation(formation);

	}

	@RequestMapping(value = "/formation/uniteenseignement/deleteuniteenseignement", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public void deleteUniteEnseignement(@RequestBody UniteEnseignementPK uniteEnseignementPK) {
		try {
			uniteEnseignementService.deleteUniteEnseignement(uniteEnseignementPK);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les éléments constitutifs et/ou les formations associées");
		}
	}

	/**
	 * Cette méthode nous retourne la liste des elements constitutifs appartenant à une unité d'enseignements
	 *
	 * @param codeUe
	 * @return
	 */
	/**
	 * Fonction de mise a jour d'un ec
	 *
	 * @param elementConstitutif
	 * @return
	 */
	@RequestMapping(value = "/formation/updateec", method = RequestMethod.POST, headers = "Accept=application/json")
	public final ElementConstitutif editEC(@RequestBody ElementConstitutif elementConstitutif) {
		return elementConstitutifService.updateEC(elementConstitutif);
	}

	/**
	 * @param codeFormation
	 *            l'id de formation
	 * @return une formation
	 */
	/**
	 * @param formation
	 *            l'entité de formation
	 * @return une formation
	 */
	@RequestMapping(value = "/formation/update", method = RequestMethod.POST, headers = "Accept=application/json")
	public final Formation editFormation(@RequestBody final Formation formation) {
		return formationService.updateFormation(formation);
	}

	/**
	 * Methode de recuperation de tous les unités d'enseignements
	 *
	 * @return liste des unités d'enseignements
	 */
	@RequestMapping(value = "/formation/uniteenseignement/findalluniteenseignement")
	public List<UniteEnseignement> findAllUniteEnseignement() {
		List<UniteEnseignement> uniteEnseignements = uniteEnseignementService.findAllUniteEnseignement();
		if (uniteEnseignements != null)
			return uniteEnseignements;
		else
			throw new SPIException("Aucune unité d'enseignement n'est trouvée");
	}

	@RequestMapping(value = "/formation/findec", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ElementConstitutif findEC(@RequestBody ElementConstitutifPK elementConstitutifPK) {

		try {

			return elementConstitutifService.getElementConstitutif(elementConstitutifPK);
		} catch (Exception e) {

			throw new SPIException("cette unite de formation n'existe pas");
		}

	}

	@RequestMapping(value = "/formation/findecbyecpk", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public ElementConstitutif findEcByEcPK(@RequestBody ElementConstitutifPK elementConstitutifPK) {
		return elementConstitutifService.getElementConstitutif(elementConstitutifPK);
	}

	/**
	 * Methode de recherche d'une unité d'enseignement
	 *
	 * @param uniteEnseignementPK
	 *            l'id de l'unité d'enseignement à rechercher
	 * @return l'enseignant
	 */
	@RequestMapping(value = "formation/finduebyensformation/{noEnseignant}/{codeFormation}")
	public List<UniteEnseignement> findUebyEnsFormation(@PathVariable("noEnseignant") Integer noEnseignant, @PathVariable("codeFormation") String codeFormation) {
		return uniteEnseignementService.findUebyEnsFormation(noEnseignant, codeFormation);
	}

	/**
	 * Methode de Test de l'ajout d'une unité d'enseignement
	 *
	 * @param uniteEnseignement
	 *            l'unité d'enseignement à ajouter
	 * @return l'enseignant
	 */
	@RequestMapping(value = "/formation/finduebyuepk", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public UniteEnseignement findUeByUePK(@RequestBody UniteEnseignementPK uniteEnseignementPK) {
		return uniteEnseignementService.findUniteEnseignement(uniteEnseignementPK);
	}

	/**
	 * Methode de modification d'une unité d'enseignement
	 *
	 * @param uniteEnseignement
	 *            l'unité d'enseignement à modifier
	 * @return l'unité d'enseignement modifiée
	 */
	/**
	 * @param codeFormation
	 *            l'id de la formation
	 * @return liste des unite enseignement
	 */
	@RequestMapping("/formation/finduesbyformation/{codeFormation}")
	public List<UniteEnseignement> findUEsByFormation(@PathVariable("codeFormation") String codeFormation) {
		List<UniteEnseignement> uniteEnseignements = formationService.getUEsByFormation(codeFormation);
		if (uniteEnseignements != null)
			return uniteEnseignements;
		else
			throw new SPIException("Cette formation ne contient pas d unités de formation");
	}

	@RequestMapping(value = "/formation/uniteenseignement/finduniteenseignement", method = RequestMethod.POST, produces = { "application/json;charset=UTF-8" })
	public UniteEnseignement findUniteEnseignement(@RequestBody UniteEnseignementPK uniteEnseignementPK) {
		UniteEnseignement uniteEnseignement = uniteEnseignementService.findUniteEnseignement(uniteEnseignementPK);
		if (uniteEnseignement != null)
			return uniteEnseignement;
		else
			throw new SPIException("L'unité d'enseignement que vous cherchez n'existe pas");
	}

	@RequestMapping(value = "/formation/findecsbyuniteenseignement", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<ElementConstitutif> finECByUnite(@RequestBody UniteEnseignementPK uniteEnseignementPK) {
		return formationService.getElementConstitutifByUePK(uniteEnseignementPK);
		// if (elementConstitutifs != null)
		// return elementConstitutifs;
		// else
		// throw new SPIException("Cette unite d enseignement ne contient pas d elements constitutif");
	}

	@RequestMapping(value = "/formation/findecsbycodeue/{codeUe}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<ElementConstitutif> finECByUniteEnseignement(@PathVariable("codeUe") String codeUe) {
		List<ElementConstitutif> elementConstitutifs = formationService.getElementConstitutifByUE(codeUe);
		if (elementConstitutifs != null)
			return elementConstitutifs;
		else
			throw new SPIException("Cette unite d'enseignement ne contient pas d'elements constitutif");
	}

	/**
	 * Methode de suppression d'une unité d'enseignement
	 *
	 * @param uniteEnseignementPK
	 *            lid de l'unité d'enseignement à supprimer
	 * @return l'unité d'enseignement modifiée
	 */
	@RequestMapping(value = "/formation/{codeFormation}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public final Formation formation(@PathVariable(value = "codeFormation") final String codeFormation) {
		return formationService.getFormation(codeFormation);

	}

	/*********************************** Partie EC ***********************************************/

	/**
	 * @param elementConstitutif
	 * @return l'objet element constitutif ajouté
	 */
	/**
	 * @return list de formation
	 */
	@RequestMapping(produces = "application/json", value = "/formations")
	public final Iterable<Formation> formations() {

		// enseignantService.traitement();
		final Iterable<Formation> formations = formationService.listFormations();
		for (final Formation frm : formations) {
			System.out.println("OK traitement " + frm.getNomFormation());
		}
		return formationService.listFormations();
	}

	@RequestMapping(value = "/formation/deleteec", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" })
	public void removeEC(@RequestBody ElementConstitutifPK elementConstitutifPK) {
		try {
			elementConstitutifService.deleteElementConstitutif(elementConstitutifPK);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les évaluations associées");
		}

	}

	/**
	 * @param codeFormation
	 *            l'id de formation
	 */
	@RequestMapping(value = "/formation/delete/{codeformation}")
	public void removeFormation(@PathVariable("codeformation") String codeFormation) {
		try {
			formationService.deleteFormation(codeFormation);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les promotions associées");
		}

	}

	/**
	 * Fonction de supression d un ec d'une formation
	 *
	 * @param elementConstitutifPK
	 */
	@RequestMapping(value = "/formation/uniteenseignement/updateuniteenseignement", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public UniteEnseignement updateUniteEnseignement(@RequestBody UniteEnseignement uniteEnseignement) {
		// uniteEnseignement.setNoEnseignant(new EnseignantService().findEnseignant(uniteEnseignement.getNoEnseignant().getNoEnseignant()));
		// uniteEnseignement.setFormation(formationService.getFormation(uniteEnseignement.getUniteEnseignementPK().getCodeFormation()));
		UniteEnseignement newUniteEnseignement = uniteEnseignementService.updateUniteEnseignement(uniteEnseignement);
		if (newUniteEnseignement != null)
			return newUniteEnseignement;
		else
			throw new SPIException("L'unité d'enseignement que vous voulez modifier existe déjà");
	}

	@RequestMapping(value = "/formation/findue/{codeformation}/{codeue}", produces = { "application/json;charset=UTF-8" })
	public UniteEnseignement findUeByUePK(@PathVariable("codeformation") String codeFormation, @PathVariable("codeue") String codeUe) {
		UniteEnseignementPK uepk = new UniteEnseignementPK();
		uepk.setCodeFormation(codeFormation);
		uepk.setCodeUe(codeUe);
		return uniteEnseignementService.findUniteEnseignement(uepk);
	}

}
