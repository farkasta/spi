package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.bean.utils.QuestionQualificatif;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.QualificatifService;
import fr.univbrest.dosi.spi.service.QuestionService;

/**
 * @author DOSI : Contolleur permet de gerer les questions
 */
@RestController
public class QuestionController {

	/**
	 * l'injection de service : QuestionService
	 */
	@Autowired
	private QuestionService questionService;

	@Autowired
	QualificatifService qualificatifService;

	/**
	 * fonction permet de ajouter un nouveau question
	 *
	 * @param question
	 *            recupéré auprés de l'appel Rest
	 * @return
	 */
	@RequestMapping(value = "/question/addquestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Question addQuestion(@RequestBody QuestionQualificatif questionQualificatif) {
		Question question = new Question();
		question.setIdQualificatif(questionQualificatif.getQualificatif());
		question.setIntitule(questionQualificatif.getQuestion().getIntitule());
		question.setType(questionQualificatif.getQuestion().getType());
		question = questionService.addQuestion(question);
		question.setIdQualificatif(qualificatifService.findQualificatifById(questionQualificatif.getQualificatif().getIdQualificatif()));
		return question;
	}

	/**
	 * Methode de suppression d'une question
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant à supprimer
	 */
	@RequestMapping(value = "/question/deletequestion/{idQuestion}")
	public void deleteQuestion(@PathVariable(value = "idQuestion") Long idQuestion) {
		try {
			questionService.deleteQuestion(idQuestion);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les rubriques associées");
		}
	}

	/**
	 * fonction permet de recuperer la liste de tous les questions
	 *
	 * @return une liste de tous les questions
	 */
	@RequestMapping(value = "/question/findallquestion", produces = { "application/json;charset=UTF-8" })
	public List<Question> findAllQuestion() {
		return questionService.findAllQuestion();
	}

	/**
	 * fonction permet de retourner un question par son id
	 *
	 * @param idQuestion
	 *            : id de question desiré
	 * @return
	 */
	@RequestMapping(value = "/question/findquestionbyid/{idquestion}", produces = { "application/json;charset=UTF-8" })
	public Question findQuestionById(@PathVariable("idquestion") Long idQuestion) {
		return questionService.findQuestionById(idQuestion);
	}

	/**
	 * setter de service injecté
	 *
	 * @param questionService
	 */
	public void setQuestionService(QuestionService questionService) {
		this.questionService = questionService;
	}

	/**
	 * fonction permet de modifier un question donné
	 *
	 * @param question
	 *            à modifié
	 * @return
	 */
	@RequestMapping(value = "/question/updatequestion", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Question updateQuestion(@RequestBody QuestionQualificatif questionQualificatif) {
		Question question = new Question();
		question.setIdQuestion(questionQualificatif.getQuestion().getIdQuestion());
		question.setIdQualificatif(questionQualificatif.getQualificatif());
		question.setIntitule(questionQualificatif.getQuestion().getIntitule());
		question.setType(questionQualificatif.getQuestion().getType());
		return questionService.updateQuestion(question);
	}

}
