package fr.univbrest.dosi.spi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.CgRefCodes;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.CgRefCodesService;

@RestController
public class CgRefCodesController {

	@Autowired
	private CgRefCodesService cgRefCodesService;

	@RequestMapping(value = "/cgrefcodes/findcgrefcodes/{rvDomain}")
	public List<CgRefCodes> findCgRefCodeByRvDomain(@PathVariable(value = "rvDomain") String rvDomain) {
		List<CgRefCodes> cgRefCodes = cgRefCodesService.findCgRefCodeByRvDomain(rvDomain);
		if (cgRefCodes != null)
			return cgRefCodes;
		else
			throw new SPIException("aucun domain trouvé!");
	}

	@RequestMapping(value = "/cgrefcodes/findmeaning/{rvLowValue}")
	public String findMeaning(@PathVariable(value = "rvLowValue") String rvLowValue) {
		String Meaning = cgRefCodesService.findMeaning(rvLowValue);
		if (Meaning != null)
			return Meaning;
		else
			throw new SPIException("aucun domain trouvé!");
	}
}
