package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.exception.SPIException;
import fr.univbrest.dosi.spi.service.RubriqueService;

@RestController
public class RubriqueController {

	@Autowired
	RubriqueService rubriqueService;

	/**
	 * Methode ajouter une rubrique
	 *
	 * @param rubrique
	 * @return
	 */
	@RequestMapping(value = "/rubrique/addrubrique", method = RequestMethod.POST, consumes = { "application/json;charset=UTF-8" }, produces = { "application/json;charset=UTF-8" })
	public Rubrique addRubrique(@RequestBody Rubrique rubrique) {
		System.out.println(rubrique.getDesignation());
		return rubriqueService.addRubrique(rubrique);

	}

	/**
	 * Methode pour supprimer une rubrique
	 *
	 * @param idRubrique
	 */
	@RequestMapping(value = "/rubrique/deleterubrique/{idRubrique}")
	public void deleteRubrique(@PathVariable("idRubrique") Long idRubrique) {
		try {
			rubriqueService.deleteRubrique(idRubrique);
		} catch (Exception e) {
			throw new SPIException("Veuillez d'abord supprimer les évaluations associées");
		}
	}

	/**
	 * Methode retrun la list de toutes les rubriques
	 *
	 * @return
	 */
	@RequestMapping(value = "/rubrique/findallrubrique")
	public final Iterable<Rubrique> findAllRubriques() {

		final Iterable<Rubrique> rubriques = rubriqueService.listRubriques();
		if (rubriques != null)
			return rubriques;
		else
			throw new SPIException("Aucune rubrique n'est trouvé");

	}

	/**
	 * Chercher une rubrique par son ID
	 *
	 * @param idRubrique
	 * @return
	 */
	@RequestMapping(value = "/rubrique/findrubrique/{idRubrique}", produces = { "application/json;charset=UTF-8" })
	public final Rubrique findRubrique(@PathVariable(value = "idRubrique") final Long idRubrique) {
		Rubrique rubrique = rubriqueService.getRubrique(idRubrique);
		if (rubrique != null)
			return rubrique;
		else
			throw new SPIException("la rubrique demander n'existe pas");
	}

	/**
	 * methode pour modifier une rubrique
	 *
	 * @param rubrique
	 * @return
	 */
	@RequestMapping(value = "/rubrique/updaterubrique", method = RequestMethod.POST, headers = "Accept=application/json")
	public final Rubrique updateRubrique(@RequestBody Rubrique rubrique) {

		return rubriqueService.updateRubrique(rubrique);
	}
}
