package fr.univbrest.dosi.spi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.univbrest.dosi.spi.service.StatistiqueService;

@RestController
public class StatistiqueController {

	@Autowired
	StatistiqueService statistiqueService;

	@RequestMapping(value = "/Statistique/countEtudiant")
	public Long countEtudiants() {
		return statistiqueService.StatEtudiants();
	}

	@RequestMapping(value = "/Statistique/countEnseignant")
	public Long countEnseignants() {
		return statistiqueService.StatEnseignants();
	}

	@RequestMapping(value = "/Statistique/countFormation")
	public Long countFormations() {
		return statistiqueService.StatFormations();
	}
}
