package fr.univbrest.dosi.spi.bean.converter;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;

@Configuration
public class CleesComposeesConfiguration {

    @Resource(name = "defaultConversionService")
    private GenericConversionService genericConversionService;

    @Bean
    public PromotionPKConverter string2PromotionPKConverter(){
    	PromotionPKConverter promotionPkConverter = new PromotionPKConverter();
        genericConversionService.addConverter(promotionPkConverter);
        return promotionPkConverter;
    }
    
    @Bean
    public ElementConstitutifPKConverter string2ElementConstitutifPKConverter(){
    	ElementConstitutifPKConverter elementConstitutifPKConverter = new ElementConstitutifPKConverter();
        genericConversionService.addConverter(elementConstitutifPKConverter);
        return elementConstitutifPKConverter;
    }
    
    @Bean
    public ReponseQuestionPKConverter string2ReponseQuestionPKConverter(){
    	ReponseQuestionPKConverter ReponseQuestionPKConverter = new ReponseQuestionPKConverter();
        genericConversionService.addConverter(ReponseQuestionPKConverter);
        return ReponseQuestionPKConverter;
    }
    
    @Bean
    public UniteEnseignementPKConverter string2UniteEnseignementPKConverter(){
    	UniteEnseignementPKConverter uniteEnseignementPKConverter = new UniteEnseignementPKConverter();
        genericConversionService.addConverter(uniteEnseignementPKConverter);
        return uniteEnseignementPKConverter;
    }


}