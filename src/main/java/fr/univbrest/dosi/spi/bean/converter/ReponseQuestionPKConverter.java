package fr.univbrest.dosi.spi.bean.converter;

import fr.univbrest.dosi.spi.bean.ReponseQuestionPK;

public class ReponseQuestionPKConverter implements org.springframework.core.convert.converter.Converter<String, ReponseQuestionPK> {

	@Override
	public ReponseQuestionPK convert(String source) {
		ReponseQuestionPK reponseQuestionPK = new ReponseQuestionPK();
		Integer cle0;
		Integer cle1;
		String[] cle = source.split("_");
		cle0=Integer.parseInt(cle[0]);
		cle1=Integer.parseInt(cle[1]);
		reponseQuestionPK.setIdReponseEvaluation(cle0);
		reponseQuestionPK.setIdQuestionEvaluation(cle1);
		return reponseQuestionPK;
	}

}
