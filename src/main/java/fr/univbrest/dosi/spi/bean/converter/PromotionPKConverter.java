package fr.univbrest.dosi.spi.bean.converter;

import fr.univbrest.dosi.spi.bean.PromotionPK;

public class PromotionPKConverter implements org.springframework.core.convert.converter.Converter<String, PromotionPK> {

	@Override
	public PromotionPK convert(String source) {
		PromotionPK promotionPK = new PromotionPK();
		String[] cle = source.split("_");
		promotionPK.setCodeFormation(cle[0]);
		promotionPK.setAnneeUniversitaire(cle[1]);
		return promotionPK;
	}

}
