package fr.univbrest.dosi.spi.bean.utils;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.PromotionPK;

public class EtudiantPromotion {

	private Etudiant etudiant;

	private PromotionPK promotionPK;

	public EtudiantPromotion() {

	}

	public EtudiantPromotion(Etudiant etudiant, PromotionPK promotionPK) {
		super();
		this.etudiant = etudiant;
		this.promotionPK = promotionPK;

	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public PromotionPK getPromotionPK() {
		return promotionPK;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public void setPromotionPK(PromotionPK promotionPK) {
		this.promotionPK = promotionPK;
	}

}
