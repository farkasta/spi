package fr.univbrest.dosi.spi.bean.converter;

import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;

public class UniteEnseignementPKConverter implements org.springframework.core.convert.converter.Converter<String, UniteEnseignementPK> {

	@Override
	public UniteEnseignementPK convert(String source) {
		UniteEnseignementPK uniteEnseignementPK = new UniteEnseignementPK();
		// codeFormation + "_" + codeUe
		String[] cle = source.split("_");
		uniteEnseignementPK.setCodeFormation(cle[0]);
		uniteEnseignementPK.setCodeUe(cle[1]);
		return uniteEnseignementPK;
	}

}
