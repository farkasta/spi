package fr.univbrest.dosi.spi.bean.converter;

import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;

public class ElementConstitutifPKConverter implements org.springframework.core.convert.converter.Converter<String, ElementConstitutifPK> {

	@Override
	public ElementConstitutifPK convert(String source) {
		ElementConstitutifPK elementConstitutifPK = new ElementConstitutifPK();
		String[] cle = source.split("_");
		//codeFormation + "_" + codeUe + "_" + codeEc
		elementConstitutifPK.setCodeFormation(cle[0]);
		elementConstitutifPK.setCodeUe(cle[1]);
		elementConstitutifPK.setCodeEc(cle[2]);
		return elementConstitutifPK;
	}

}
