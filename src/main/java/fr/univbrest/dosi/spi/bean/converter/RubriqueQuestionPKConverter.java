package fr.univbrest.dosi.spi.bean.converter;

import fr.univbrest.dosi.spi.bean.RubriqueQuestionPK;


public class RubriqueQuestionPKConverter implements org.springframework.core.convert.converter.Converter<String, RubriqueQuestionPK> {

	@Override
	public RubriqueQuestionPK convert(String source) {
		RubriqueQuestionPK rubriqueQuestionPK = new RubriqueQuestionPK();
		String[] cle = source.split("_");
		rubriqueQuestionPK.setIdRubrique(Integer.valueOf(cle[0]));
		rubriqueQuestionPK.setIdQuestion(Integer.valueOf(cle[1]));
		return rubriqueQuestionPK;
	}

}
