package fr.univbrest.dosi.spi.bean.utils;

import org.springframework.stereotype.Component;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.bean.Formation;
import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;

@Component
public class UniteEnseignementUtil {

	private Enseignant enseignant;
	private UniteEnseignement uniteEnseignement;

	public UniteEnseignementUtil() {

	}

	public UniteEnseignementUtil(Enseignant enseignant, UniteEnseignement uniteEnseignement, Formation formation) {
	    super();
	    this.enseignant = enseignant;
	    this.uniteEnseignement = uniteEnseignement;
    }

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	public UniteEnseignement getUniteEnseignement() {
		return uniteEnseignement;
	}

	public void setUniteEnseignement(UniteEnseignement uniteEnseignement) {
		this.uniteEnseignement = uniteEnseignement;
	}	
}
