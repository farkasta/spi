package fr.univbrest.dosi.spi.bean.utils;

import fr.univbrest.dosi.spi.bean.Evaluation;

public class EtudiantEvaluation {

	private String nomFormation;
	private String UE;
	private String EC;
	private Evaluation evaluation;

	public EtudiantEvaluation() {

	}

	public EtudiantEvaluation(String nomFormation, Evaluation evaluation) {
		super();
		this.nomFormation = nomFormation;
		this.evaluation = evaluation;
	}

	public String getEC() {
		return EC;
	}

	public Evaluation getEvaluation() {
		return evaluation;
	}

	public String getNomFormation() {
		return nomFormation;
	}

	public String getUE() {
		return UE;
	}

	public void setEC(String eC) {
		EC = eC;
	}

	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}

	public void setNomFormation(String nomFormation) {
		this.nomFormation = nomFormation;
	}

	public void setUE(String uE) {
		UE = uE;
	}

}
