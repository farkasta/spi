/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univbrest.dosi.spi.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author DOSI
 */
@Entity
@Table(name = "EVALUATION", catalog = "", schema = "DOSI", uniqueConstraints = { @UniqueConstraint(columnNames = { "ANNEE_UNIVERSITAIRE", "NO_ENSEIGNANT", "NO_EVALUATION", "CODE_FORMATION", "CODE_UE" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Evaluation.findAll", query = "SELECT e FROM Evaluation e"),
	@NamedQuery(name = "Evaluation.findByIdEvaluation", query = "SELECT e FROM Evaluation e WHERE e.idEvaluation = :idEvaluation"),
	@NamedQuery(name = "Evaluation.findByNoEvaluation", query = "SELECT e FROM Evaluation e WHERE e.noEvaluation = :noEvaluation"),
	@NamedQuery(name = "Evaluation.findByDesignation", query = "SELECT e FROM Evaluation e WHERE e.designation = :designation"),
	@NamedQuery(name = "Evaluation.findByEtat", query = "SELECT e FROM Evaluation e WHERE e.etat = :etat"),
	@NamedQuery(name = "Evaluation.findByPeriode", query = "SELECT e FROM Evaluation e WHERE e.periode = :periode"),
	@NamedQuery(name = "Evaluation.findByDebutReponse", query = "SELECT e FROM Evaluation e WHERE e.debutReponse = :debutReponse"),
	@NamedQuery(name = "Evaluation.findByFinReponse", query = "SELECT e FROM Evaluation e WHERE e.finReponse = :finReponse"),
	@NamedQuery(name = "Evaluation.findByNoEnseignant", query = "SELECT e FROM Evaluation e WHERE e.noEnseignant.noEnseignant = :noEnseignant"),
	@NamedQuery(name = "Evaluation.findByPromotion", query = "SELECT e FROM Evaluation e WHERE e.codeFormation = :codeFormation AND e.annee =:anneeUniversitaire AND e.etat NOT LIKE 'ELA'") })
public class Evaluation implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID_EVALUATION")
	@SequenceGenerator(name = "EVE_SEQ", sequenceName = "EVE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVE_SEQ")
	private Long idEvaluation;
	@Basic(optional = false)
	@NotNull
	@Column(name = "NO_EVALUATION")
	private short noEvaluation;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 16)
	@Column(name = "DESIGNATION")
	private String designation;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 3)
	@Column(name = "ETAT")
	private String etat;
	@Size(max = 64)
	@Column(name = "PERIODE")
	private String periode;
	@Basic(optional = false)
	@NotNull
	@Column(name = "DEBUT_REPONSE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date debutReponse;
	@Basic(optional = false)
	@NotNull
	@Column(name = "FIN_REPONSE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finReponse;

	@Column(name = "CODE_FORMATION", nullable = false)
	private String codeFormation;
	@Column(name = "CODE_UE", nullable = false)
	private String codeUe;
	@Column(name = "CODE_EC")
	private String codeEc;
	@Column(name = "ANNEE_UNIVERSITAIRE", nullable = false)
	private String annee;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluation")
	private Collection<Droit> droitCollection;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idEvaluation")
	private Collection<ReponseEvaluation> reponseEvaluationCollection;

	@JoinColumn(name = "NO_ENSEIGNANT", referencedColumnName = "NO_ENSEIGNANT")
	@ManyToOne(optional = false)
	private Enseignant noEnseignant;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idEvaluation")
	private Collection<RubriqueEvaluation> rubriqueEvaluationCollection;

	public Evaluation() {
	}

	public Evaluation(Long idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public Evaluation(Long idEvaluation, short noEvaluation, String designation, String etat, Date debutReponse, Date finReponse) {
		this.idEvaluation = idEvaluation;
		this.noEvaluation = noEvaluation;
		this.designation = designation;
		this.etat = etat;
		this.debutReponse = debutReponse;
		this.finReponse = finReponse;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Evaluation))
			return false;
		Evaluation other = (Evaluation) object;
		if ((this.idEvaluation == null && other.idEvaluation != null) || (this.idEvaluation != null && !this.idEvaluation.equals(other.idEvaluation)))
			return false;
		return true;
	}

	public String getAnnee() {
		return annee;
	}

	public String getCodeEc() {
		return codeEc;
	}

	public String getCodeFormation() {
		return codeFormation;
	}

	public String getCodeUe() {
		return codeUe;
	}

	public Date getDebutReponse() {
		return debutReponse;
	}

	public String getDesignation() {
		return designation;
	}

	@XmlTransient
	public Collection<Droit> getDroitCollection() {
		return droitCollection;
	}

	public String getEtat() {
		return etat;
	}

	public Date getFinReponse() {
		return finReponse;
	}

	public Long getIdEvaluation() {
		return idEvaluation;
	}

	public Enseignant getNoEnseignant() {
		return noEnseignant;
	}

	public short getNoEvaluation() {
		return noEvaluation;
	}

	public String getPeriode() {
		return periode;
	}

	@XmlTransient
	public Collection<ReponseEvaluation> getReponseEvaluationCollection() {
		return reponseEvaluationCollection;
	}

	@XmlTransient
	public Collection<RubriqueEvaluation> getRubriqueEvaluationCollection() {
		return rubriqueEvaluationCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idEvaluation != null ? idEvaluation.hashCode() : 0);
		return hash;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public void setCodeEc(String codeEc) {
		this.codeEc = codeEc;
	}

	public void setCodeFormation(String codeFormation) {
		this.codeFormation = codeFormation;
	}

	public void setCodeUe(String codeUe) {
		this.codeUe = codeUe;
	}

	public void setDebutReponse(Date debutReponse) {
		this.debutReponse = debutReponse;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setDroitCollection(Collection<Droit> droitCollection) {
		this.droitCollection = droitCollection;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public void setFinReponse(Date finReponse) {
		this.finReponse = finReponse;
	}

	public void setIdEvaluation(Long idEvaluation) {
		this.idEvaluation = idEvaluation;
	}

	public void setNoEnseignant(Enseignant noEnseignant) {
		this.noEnseignant = noEnseignant;
	}

	public void setNoEvaluation(short noEvaluation) {
		this.noEvaluation = noEvaluation;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public void setReponseEvaluationCollection(Collection<ReponseEvaluation> reponseEvaluationCollection) {
		this.reponseEvaluationCollection = reponseEvaluationCollection;
	}

	public void setRubriqueEvaluationCollection(Collection<RubriqueEvaluation> rubriqueEvaluationCollection) {
		this.rubriqueEvaluationCollection = rubriqueEvaluationCollection;
	}

	@Override
	public String toString() {
		return "jpa.Evaluation[ idEvaluation=" + idEvaluation + " ]";
	}

}
