package fr.univbrest.dosi.spi.bean.utils;

import org.springframework.stereotype.Component;

import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.bean.Question;

@Component
public class QuestionQualificatif {

	private Qualificatif qualificatif;
	private Question question;

	public QuestionQualificatif() {

	}

	public QuestionQualificatif(Qualificatif qualificatif, Question question) {
		super();
		this.qualificatif = qualificatif;
		this.question = question;
	}

	public Qualificatif getQualificatif() {
		return qualificatif;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQualificatif(Qualificatif qualificatif) {
		this.qualificatif = qualificatif;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

}
