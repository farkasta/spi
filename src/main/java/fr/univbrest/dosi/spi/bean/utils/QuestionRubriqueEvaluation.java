package fr.univbrest.dosi.spi.bean.utils;

import fr.univbrest.dosi.spi.bean.QuestionEvaluation;

public class QuestionRubriqueEvaluation {

	private QuestionEvaluation questionEvaluation;
	private Long idRubriqueEvaluation;

	public QuestionRubriqueEvaluation() {

	}

	public Long getIdRubriqueEvaluation() {
		return idRubriqueEvaluation;
	}

	public QuestionEvaluation getQuestionEvaluation() {
		return questionEvaluation;
	}

	public void setIdRubriqueEvaluation(Long idRubriqueEvaluation) {
		this.idRubriqueEvaluation = idRubriqueEvaluation;
	}

	public void setQuestionEvaluation(QuestionEvaluation questionEvaluation) {
		this.questionEvaluation = questionEvaluation;
	}
}
