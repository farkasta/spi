package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.CgRefCodes;
import fr.univbrest.dosi.spi.dao.CgRefCodesRepository;

@Service
public class CgRefCodesService {

	@Autowired
	private CgRefCodesRepository cgrefcodesrepository;

	public List<CgRefCodes> findCgRefCodeByRvDomain(final String rvDomain) {
		return cgrefcodesrepository.findByRvDomain(rvDomain);
	}

	public String findMeaning(String rvLowValue) {
		CgRefCodes cgRefCodes = cgrefcodesrepository.findByRvLowValue(rvLowValue);
		if (cgRefCodes != null)
			return cgRefCodes.getRvMeaning();
		return null;
	}

}
