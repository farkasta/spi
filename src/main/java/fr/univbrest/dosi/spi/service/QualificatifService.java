package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Qualificatif;
import fr.univbrest.dosi.spi.dao.QualificatifRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 */
@Service
public class QualificatifService {

	@Autowired
	private QualificatifRepository qualificatifRepository;

	public Qualificatif addQualificatif(final Qualificatif qualificatif) {
		return qualificatifRepository.save(qualificatif);
	}

	public void deleteQualificatif(final Long idQualificatif) {
		if (qualificatifRepository.exists(idQualificatif)) {
			try {
				qualificatifRepository.delete(idQualificatif);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les questions associées");
			}
		} else
			throw new SPIException("Le qualitificatif que vous souhaitez supprimer n'exsite pas");

	}

	/**
	 * Methode de recuperation de tous les qualificatifs
	 *
	 * @return liste des qualificatifs
	 */
	public Iterable<Qualificatif> findAllQualificatif() {
		Iterable<Qualificatif> qualificatifs = qualificatifRepository.findAll();
		if (qualificatifs != null)
			return qualificatifs;
		else
			throw new SPIException("Aucun qualificatif n'a est trouvé");
	}

	public Qualificatif findQualificatifById(final Long idQualificatif) {
		return qualificatifRepository.findOne(idQualificatif);
	}

	/**
	 * Methode de modification d'un qualificatif
	 *
	 * @param qualificatif
	 *            l'entité qualificatif est modifié
	 * @return qualificatif modifié
	 */
	public Qualificatif updateQualificatif(Qualificatif qualificatif) {
		if (qualificatifRepository.exists(qualificatif.getIdQualificatif()))
			return qualificatifRepository.save(qualificatif);
		else
			throw new SPIException("Le qualificatif que vous souhaitez modifier n'exsite pas ");
	}

}
