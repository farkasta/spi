package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.dao.ElementConstitutifRepository;

/**
 * @author DOSI
 */
@Service
public class ElementConstitutifService {

	@Autowired
	private ElementConstitutifRepository elementConstitutifRepository;

	public ElementConstitutif addElementConstitutif(ElementConstitutif elementConstitutif) {
		return elementConstitutifRepository.save(elementConstitutif);
	}

	/**
	 * Methode de modification pour element constitutif
	 *
	 * @param element
	 *            constitutif
	 * @return l element constitutif modifier
	 */
	public ElementConstitutif updateEC(ElementConstitutif elementConstitutif) {
		return elementConstitutifRepository.save(elementConstitutif);
	}

	public void deleteElementConstitutif(final ElementConstitutifPK elementConstitutifPK) {
		elementConstitutifRepository.delete(elementConstitutifPK);
	}

	public Boolean existElementCostitutif(final ElementConstitutifPK elementConstitutifPK) {
		return elementConstitutifRepository.exists(elementConstitutifPK);
	}

	public final ElementConstitutif getElementConstitutif(final ElementConstitutifPK elementConstitutifPK) {
		return elementConstitutifRepository.findOne(elementConstitutifPK);
	}

}
