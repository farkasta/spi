package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.bean.Promotion;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.dao.PromotionRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class PromotionService {

	@Autowired
	private PromotionRepository promotionRepository;

	public Promotion addPromotion(final Promotion promotion) {
		return promotionRepository.save(promotion);
	}

	public void deletePromotion(final PromotionPK promotionPK) {
		if (promotionRepository.exists(promotionPK)) {
			try {
				promotionRepository.delete(promotionPK);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les étudiants associés");
			}
		} else
			throw new SPIException("La promotion que vous souhaitez supprimer n'exsite pas");

	}

	public Boolean existPromotion(final PromotionPK promotionPK) {
		return promotionRepository.exists(promotionPK);
	}

	/**
	 * @return cette méthode retourne toutes les promotions
	 */
	public List<Promotion> findAllPromotions() {
		List<Promotion> promotions = (List<Promotion>) promotionRepository.findAll();
		Collections.reverse(promotions);
		return promotions;

	}

	/**
	 * @param codeFormation
	 *            ce code indique la formation associé a une promotion
	 * @param anneeUniversitaire
	 *            l'année de promotion
	 * @return cette méthode va retourner une liste ordonnées des étudiants
	 */
	public List<Etudiant> findEtudiantByPromotion(PromotionPK promotionPK) {
		if (promotionRepository.exists(promotionPK)) {
			Promotion promotion = promotionRepository.findOne(promotionPK);
			List<Etudiant> etudiants = (List<Etudiant>) promotion.getEtudiantCollection();
			Collections.sort(etudiants, new Comparator<Etudiant>() {
				@Override
				public int compare(Etudiant e1, Etudiant e2) {
					int order = e1.getNom().compareToIgnoreCase(e2.getNom());
					return order == 0 ? e1.getPrenom().compareToIgnoreCase(e2.getPrenom()) : order;
				}
			});
			return etudiants;
		} else
			throw new SPIException("promotion n'est pas trouvée");
	}

	/**
	 * @param promotionPK
	 *            c'est un objet de type PromotionPK (classe qui contient lq clé primaire de la classe promotion)
	 * @return cette méthode va retourner les information d'une promotion
	 */
	public Promotion findPromotion(PromotionPK promotionPK) {
		return promotionRepository.findOne(promotionPK);
	}

	/**
	 * une fonction qui permet de retourner la liste des promotion d'un enseignant
	 *
	 * @param noEnseignant
	 * @return liste des promotions
	 */
	public List<Promotion> getPromotionByEnseignant(final Integer noEnseignant) {
		List promotions = promotionRepository.findByNoEnseignant(noEnseignant);
		Collections.sort(promotions, new Comparator<Promotion>() {
			@Override
			public int compare(Promotion p1, Promotion p2) {
				return p1.getPromotionPK().getAnneeUniversitaire().compareTo(p2.getPromotionPK().getAnneeUniversitaire());
			}
		});
		return promotions;
	}

	public Promotion updatePromotion(Promotion promotion) {
		return promotionRepository.save(promotion);
	}
}
