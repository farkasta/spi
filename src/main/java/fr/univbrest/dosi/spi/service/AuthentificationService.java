package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Authentification;
import fr.univbrest.dosi.spi.dao.AuthentificationRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI Le service d'authentification: Nous permet de verifier l authentification de chaque utlisateur pour ensuite recupérer son rôle
 */
@Service
public class AuthentificationService {

	@Autowired
	private AuthentificationRepository authentificationRepository;

	public Authentification findEnseignantByIdConnection(Long idConnection) {
		return authentificationRepository.findEnseignantByIdConnection(idConnection);
	}

	/**
	 * Liste des utilisateurs sur la quelle on se basera pour les tests avant cnx avec la base de données.
	 */

	public final void setauthentificationRepository(final AuthentificationRepository authentificationRepository) {
		this.authentificationRepository = authentificationRepository;
	}

	/**
	 * Cette fonction verifie l existance de l'utilisateur qui tente de se connecté
	 *
	 * @param login
	 * @param pwd
	 * @return doit nous retourner l'utilisateur (son role ainsi que son login, pwd et user name)
	 */
	public Authentification VerifySingIn(final String login, final String pwd) {
		final Authentification authentificationLogin = authentificationRepository.findByLoginConnection(login);
		final Authentification authentificationPseudo = authentificationRepository.findByPseudoConnection(login);
		if (authentificationLogin != null && authentificationLogin.getMotPasse().equalsIgnoreCase(pwd))
			return authentificationLogin;
		else if (authentificationPseudo != null && authentificationPseudo.getMotPasse().equalsIgnoreCase(pwd))
			return authentificationPseudo;
		else
			throw new SPIException("Le nom d'utilisateur ou le mot de passe est incorrect");
	}

}
