package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Rubrique;
import fr.univbrest.dosi.spi.dao.RubriqueRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class RubriqueService {

	@Autowired
	RubriqueRepository rubriqueRepository;

	/**
	 * Methode ajouter une rubrique
	 *
	 * @param rubrique
	 * @return
	 */
	public Rubrique addRubrique(Rubrique rubrique) {
		return rubriqueRepository.save(rubrique);
	}

	/**
	 * Supprimer une rubrique
	 *
	 * @param idRubrique
	 */
	public void deleteRubrique(Long idRubrique) {
		if (rubriqueRepository.exists(idRubrique)) {
			try {
				rubriqueRepository.delete(idRubrique);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abord supprimer les rubriques des évaluations associées");
			}

		} else
			throw new SPIException("La rubrique que vous souhaitez supprimer n'exsite pas");
	}

	/**
	 * Verifier l'existance une rubrique
	 *
	 * @param idRubrique
	 * @return
	 */
	public final Boolean existeRubrique(Long idRubrique) {

		final Boolean exist = rubriqueRepository.exists(idRubrique);
		if (exist)
			return exist;
		else
			throw new SPIException("Il n'y a aucune rubrique avec ce numero");

	}

	public Rubrique findRubriqueById(Long idRubrique) {
		return rubriqueRepository.findOne(idRubrique);
	}

	/**
	 * @param idRubrique
	 * @return
	 */
	public final Rubrique getRubrique(final Long idRubrique) {
		Rubrique rubrique = rubriqueRepository.findOne(idRubrique);
		if (rubrique != null)
			return rubrique;
		else
			throw new SPIException("La rubrique que vous cherchez n'existe pas");

	}

	/**
	 * Afficher toutes les ribrques
	 *
	 * @return
	 */
	public final List<Rubrique> listRubriques() {

		List<Rubrique> rubriques = (List<Rubrique>) rubriqueRepository.findAll();
		if (rubriques != null) {
			Collections.sort(rubriques, new Comparator<Rubrique>() {
				@Override
				public int compare(Rubrique r1, Rubrique r2) {
					return r1.getOrdre().compareTo(r2.getOrdre());
				}
			});
			return rubriques;
		}

		else
			throw new SPIException("Aucune rubrique n'est trouvé");

	}

	/**
	 * Modifier une rubrique
	 *
	 * @param rubrique
	 * @return
	 */
	public Rubrique updateRubrique(Rubrique rubrique) {

		if (rubriqueRepository.exists(rubrique.getIdRubrique()))
			return rubriqueRepository.save(rubrique);
		else
			throw new SPIException("l'enseignant que vous souhaitez modifier n'exsite pas ");

	}

}
