package fr.univbrest.dosi.spi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.QuestionEvaluation;
import fr.univbrest.dosi.spi.dao.QuestionEvaluationRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class QuestionEvaluationService {

	/**
	 * injection de repository
	 */
	@Autowired
	private QuestionEvaluationRepository questionEvaluationRepository;

	/**
	 * fonction permet d'ajouter une nouvelle questionEvaluation
	 *
	 * @param questionEvaluation
	 *            : enregistrement à ajouté
	 * @return la questionEvaluation ajoutée
	 */
	public QuestionEvaluation addQuestionEvaluation(QuestionEvaluation questionEvaluation) {
		return questionEvaluationRepository.save(questionEvaluation);
	}

	/**
	 * fonction permet de supprimer une questionEvaluation
	 *
	 * @param idQuestionEvaluation
	 *            : id de la questionEvaluation à supprimer
	 */
	public void deleteQuestionEvaluation(Long idQuestionEvaluation) {
		if (questionEvaluationRepository.exists(idQuestionEvaluation)) {
			try {
				questionEvaluationRepository.delete(idQuestionEvaluation);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les réponses associées");
			}
		} else
			throw new SPIException("La question évaluation que vous souhaitez supprimer n'exsite pas");
	}

	/**
	 * fonction permet de retourner la liste de toutes les questionEvaluations
	 *
	 * @return une liste contient toutes questionEvaluations
	 */
	public List<QuestionEvaluation> findAllQuestionEvaluation() {
		return (List<QuestionEvaluation>) questionEvaluationRepository.findAll();
	}

	/**
	 * fonction permet de retourner une questionEvaluation par son id
	 *
	 * @param idQuestionEvaluation
	 *            : id de la questionEvaluation desirée
	 * @return
	 */
	public QuestionEvaluation findQuestionEvaluationById(Long idQuestionEvaluation) {
		return questionEvaluationRepository.findByIdQuestionEvaluation(idQuestionEvaluation);
	}

	public List<QuestionEvaluation> findQuestionEvaluationByIdRubriqueEvaluation(Long idRubriqueEvaluation) {

		return questionEvaluationRepository.findByIdRubriqueEvaluation(idRubriqueEvaluation);
	}

	public void setQuestionEvaluationRepository(QuestionEvaluationRepository questionEvaluationRepository) {
		this.questionEvaluationRepository = questionEvaluationRepository;
	}

	/**
	 * fontion permet de modifier une questionEvaluation
	 *
	 * @param questionEvaluation
	 *            : la ques tion à modifiée
	 * @return la nouvelle questionEvaluation
	 */
	public QuestionEvaluation updateQuestionEvaluation(QuestionEvaluation questionEvaluation) {
		return questionEvaluationRepository.save(questionEvaluation);
	}

}
