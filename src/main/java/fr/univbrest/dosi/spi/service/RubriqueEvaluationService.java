package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;
import fr.univbrest.dosi.spi.dao.RubriqueEvaluationRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class RubriqueEvaluationService {

	@Autowired
	RubriqueEvaluationRepository rubriqueEvaluationRepository;

	public RubriqueEvaluation addRubriqueEvaluation(RubriqueEvaluation rubriqueEvaluation) {
		return rubriqueEvaluationRepository.save(rubriqueEvaluation);
	}

	public void deleteRubriqueEvaluation(Long idRubriqueEvaluation) {
		if (rubriqueEvaluationRepository.exists(idRubriqueEvaluation)) {
			try {
				rubriqueEvaluationRepository.delete(idRubriqueEvaluation);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les réponses associées");
			}
		} else
			throw new SPIException("La rubrique évaluation que vous souhaitez supprimer n'exsite pas");
	}

	public List<RubriqueEvaluation> findAllRubriqueEvaluation(Long idEvaluation) {
		List<RubriqueEvaluation> rubriqueEvaluation = rubriqueEvaluationRepository.findByIdEvaluation(idEvaluation);
		Collections.sort(rubriqueEvaluation, new Comparator<RubriqueEvaluation>() {
			@Override
			public int compare(RubriqueEvaluation rub1, RubriqueEvaluation rub2) {
				return rub1.getIdRubrique().getDesignation().compareToIgnoreCase(rub2.getIdRubrique().getDesignation());
			}
		});
		// for (int i = 0; i < rubriqueEvaluation.size(); i++) {
		// Collections.sort((List<QuestionEvaluation>) rubriqueEvaluation.get(i).getQuestionEvaluationCollection(), new Comparator<QuestionEvaluation>() {
		// @Override
		// public int compare(QuestionEvaluation que1, QuestionEvaluation que2) {
		// return que1.getIdQuestion().getIntitule().compareToIgnoreCase(que2.getIdQuestion().getIntitule());
		// }
		// });
		// }

		return rubriqueEvaluation;
	}

	public RubriqueEvaluation findRubriqueEvaluation(Long idRubriqueEvaluation) {
		return rubriqueEvaluationRepository.findOne(idRubriqueEvaluation);
	}

	public RubriqueEvaluation updateRubriqueEvaluation(RubriqueEvaluation rubriqueEvaluation) {
		return rubriqueEvaluationRepository.save(rubriqueEvaluation);
	}
}
