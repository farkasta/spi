package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Etudiant;
import fr.univbrest.dosi.spi.dao.EtudiantRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 */
@Service
public class EtudiantService {

	@Autowired
	private EtudiantRepository etudiantRepository;

	/**
	 * methode permet d'ajouter un etudiant
	 *
	 * @param etudiant
	 */
	public Etudiant addEtudiant(Etudiant etudiant) {
		return etudiantRepository.save(etudiant);
	}

	/**
	 * suppression d'un etudiant a partir du noEtudiant
	 *
	 * @param noEtudiant
	 */
	public void deletEtudiant(String noEtudiant) {
		if (etudiantRepository.exists(noEtudiant)) {
			try {
				etudiantRepository.delete(noEtudiant);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abord supprimer les réponses d'évaluations associées ");
			}
		} else
			throw new SPIException("l'étudiant que vous souhaitez supprimer n'existe pas ");
	}

	public final Boolean existEtudiant(final String noEtudiant) {
		return etudiantRepository.exists(noEtudiant);
	}

	/**
	 * afficher les details d'un etudiant donnee
	 *
	 * @param noEtudiant
	 * @return
	 */
	public Etudiant findEtudiant(String noEtudiant) {
		return etudiantRepository.findOne(noEtudiant);
	}

	/**
	 * cette methode return la list de tout les etudiants dans la base de donnee
	 *
	 * @return
	 */
	public final Iterable<Etudiant> listEtudiants() {
		final Iterable<Etudiant> etudiants = etudiantRepository.findAll();
		return etudiants;
	}

	/**
	 * permet de modifier un etudiant
	 *
	 * @param etudiant
	 */
	public Etudiant updateEtudiant(Etudiant etudiant) {
		return etudiantRepository.save(etudiant);
	}
}
