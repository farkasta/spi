package fr.univbrest.dosi.spi.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.ElementConstitutifPK;
import fr.univbrest.dosi.spi.bean.Evaluation;
import fr.univbrest.dosi.spi.bean.PromotionPK;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.bean.utils.EtudiantEvaluation;
import fr.univbrest.dosi.spi.dao.ElementConstitutifRepository;
import fr.univbrest.dosi.spi.dao.EvaluationRepository;
import fr.univbrest.dosi.spi.dao.FormationRepository;
import fr.univbrest.dosi.spi.dao.UniteEnseignementRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

@Service
public class EvaluationService {

	/**
	 *
	 */
	@Autowired
	EvaluationRepository evaluationRepository;

	@Autowired
	FormationRepository formationRepository;

	@Autowired
	UniteEnseignementRepository uniteEnseignementRepository;

	@Autowired
	ElementConstitutifRepository elemnConstitutifRepository;

	/**
	 * @param evaluation
	 * @return
	 */
	public Evaluation addEvaluation(Evaluation evaluation) {
		return evaluationRepository.save(evaluation);
	}

	public void deleteEvaluation(Long idEvaluation) {
		if (evaluationRepository.exists(idEvaluation)) {
			try {
				evaluationRepository.delete(idEvaluation);
			} catch (Exception e) {
				throw new SPIException("Veuillez supprimer les réponses associées ");
			}
		} else
			throw new SPIException("l'évaluation que vous souhaitez supprimer n'exsite pas ");

	}

	/**
	 * @return
	 */
	public List<Evaluation> findAllEvaluation() {
		return (List<Evaluation>) evaluationRepository.findAll();
	}

	public List<Evaluation> findAllEvaluationByNoEnseignant(Integer noEnseignant) {
		return evaluationRepository.findByNoEnseignant(noEnseignant);
	}

	public Evaluation findEvaluationById(Long idEvaluation) {
		return evaluationRepository.findOne(idEvaluation);
	}

	public List<EtudiantEvaluation> findEvaluationByPromotion(PromotionPK promotionPK) {
		EtudiantEvaluation etudiantEvaluation = new EtudiantEvaluation();
		List<EtudiantEvaluation> etudiantEvaluations = new ArrayList<EtudiantEvaluation>();
		String nomFormation;
		String uniteEns;
		UniteEnseignementPK uniteEnseignementPK = new UniteEnseignementPK();
		new ElementConstitutifPK();
		new ElementConstitutifPK();
		List<Evaluation> evaluation = evaluationRepository.findByPromotion(promotionPK.getCodeFormation(), promotionPK.getAnneeUniversitaire());
		for (int i = 0; i < evaluation.size(); i++) {
			etudiantEvaluation = new EtudiantEvaluation();
			nomFormation = formationRepository.findOne(evaluation.get(i).getCodeFormation()).getNomFormation();
			uniteEnseignementPK.setCodeFormation(evaluation.get(i).getCodeFormation());
			uniteEnseignementPK.setCodeUe(evaluation.get(i).getCodeUe());
			uniteEns = uniteEnseignementRepository.findOne(uniteEnseignementPK).getDesignation();
			etudiantEvaluation.setNomFormation(nomFormation);
			etudiantEvaluation.setUE(uniteEns);
			etudiantEvaluation.setEvaluation(evaluation.get(i));
			etudiantEvaluations.add(etudiantEvaluation);

		}
		Collections.sort(etudiantEvaluations, new Comparator<EtudiantEvaluation>() {
			@Override
			public int compare(EtudiantEvaluation ev1, EtudiantEvaluation ev2) {
				int ordre = ev1.getEvaluation().getAnnee().compareTo(ev2.getEvaluation().getAnnee());
				int ordrefinal = ordre == 0 ? ev1.getEvaluation().getCodeFormation().compareTo(ev2.getEvaluation().getCodeFormation()) : ordre;
				return ordrefinal == 0 ? String.valueOf(ev1.getEvaluation().getNoEvaluation()).compareToIgnoreCase(String.valueOf(ev2.getEvaluation().getNoEvaluation())) : ordrefinal;
			}
		});
		return etudiantEvaluations;
	}

	public Evaluation updateEvaluation(Evaluation evaluation) {
		return evaluationRepository.save(evaluation);
	}
}
