package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Question;
import fr.univbrest.dosi.spi.dao.QuestionRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 */
@Service
public class QuestionService {

	/**
	 * injection de repository
	 */
	@Autowired
	private QuestionRepository questionRepository;

	/**
	 * fonction permet de ajouter un nouveau question
	 *
	 * @param question
	 *            : enregistrement à ajouté
	 * @return le question ajouté
	 */
	public Question addQuestion(Question question) {
		return questionRepository.save(question);

	}

	/**
	 * fonction permet de supprimer un question
	 *
	 * @param idQuestion
	 *            : id de question à supprimer
	 */
	public void deleteQuestion(Long idQuestion) {
		if (questionRepository.exists(idQuestion)) {
			try {
				questionRepository.delete(idQuestion);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les questions évaluation associées");
			}
		} else
			throw new SPIException("La question que vous souhaitez supprimer n'exsite pas");

	}

	/**
	 * fonction permet de retourner la liste de tous les questions
	 *
	 * @return une liste contient tous questions
	 */
	public List<Question> findAllQuestion() {
		List<Question> questions = (List<Question>) questionRepository.findAll();
		Collections.sort(questions, new Comparator<Question>() {
			@Override
			public int compare(Question q1, Question q2) {
				return q1.getIntitule().compareToIgnoreCase(q2.getIntitule());
			}
		});
		return questions;
	}

	/**
	 * fonction permet de retourner un question par son id
	 *
	 * @param idQuestion
	 *            : id de question desiré
	 * @return
	 */
	public Question findQuestionById(Long idQuestion) {
		return questionRepository.findByIdQuestion(idQuestion);
	}

	public void setQuestionRepository(QuestionRepository questionRepository) {
		this.questionRepository = questionRepository;
	}

	/**
	 * fontion permet de modifier un question
	 *
	 * @param question
	 *            : le question à modifié
	 * @return le nouveau question
	 */
	public Question updateQuestion(Question question) {
		return questionRepository.save(question);
	}

}
