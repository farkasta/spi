package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.Enseignant;
import fr.univbrest.dosi.spi.dao.EnseignantRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * Classe Service de l'enseignant
 *
 * @author DOSI
 */
@Service
public class EnseignantService {
	/**
	 * Injection du repository enseignant
	 */
	@Autowired
	private EnseignantRepository enseignantRepository;

	/**
	 * Methode de recuperation de tous les enseignants
	 *
	 * @return liste des enseignant
	 */
	public List<Enseignant> findAllEnseignant() {
		List<Enseignant> enseignants = (List) enseignantRepository.findAll();
		if (enseignants != null) {
			Collections.sort(enseignants, new Comparator<Enseignant>() {
				@Override
				public int compare(Enseignant e1, Enseignant e2) {
					return e1.getNom().compareTo(e2.getNom());
				}
			});
			return enseignants;
		} else
			throw new SPIException("Aucun enseignant n'est trouvé");
	}

	/**
	 * Methode de recherche d'un enseignant
	 *
	 * @param id
	 *            l'id de l'enseignant à rechercher
	 * @return l'enseignant
	 */
	public Enseignant findEnseignant(Integer noEnseignant) {
		Enseignant enseignant = enseignantRepository.findOne(noEnseignant);
		if (enseignant != null)
			return enseignant;
		else
			throw new SPIException("L'enseignant que vous cherchez n'existe pas");
	}

	/**
	 * Methode de recherche d'un/des enseignant(s) par son nom
	 *
	 * @param nom
	 *            de l'enseignant
	 * @return la liste des enseignants ayant le nom recherché
	 */
	public List<Enseignant> findEnseignantByNom(String nom) {
		return enseignantRepository.findByNom(nom);
	}

	/**
	 * Methode de test de l'existance d'un enseignant
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 * @return resultat du test
	 */
	public Boolean existEnseignant(Integer noEnseignant) {
		final Boolean exist = enseignantRepository.exists(noEnseignant);
		if (exist)
			return exist;
		else
			throw new SPIException("Il n'y a aucun enseignant avec ce numero");
	}

	/**
	 * Methode d'ajout d'un enseignant
	 *
	 * @param enseignant
	 *            l'entité à ajouter
	 * @return l'enseignant ajouté
	 */
	public Enseignant addEnseignant(Enseignant enseignant) {
		if (enseignantRepository.exists(enseignant.getNoEnseignant()))
			throw new SPIException("l'enseignant que vous souhaitez ajouter exsite déjà ");
		return enseignantRepository.save(enseignant);
	}

	/**
	 * Methode de modification d'un enseignant
	 *
	 * @param enseignant
	 *            l'entité enseignant à modifié
	 * @return l'enseignant modifié
	 */
	public Enseignant updateEnseignant(Enseignant enseignant) {
		if (enseignantRepository.exists(enseignant.getNoEnseignant()))
			return enseignantRepository.save(enseignant);
		else
			throw new SPIException("l'enseignant que vous souhaitez supprimer n'exsite pas ");
	}

	/**
	 * Methode de suppression d'un enseignant
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 */
	public void deleteEnseignant(Integer noEnseignant) {
		if (enseignantRepository.exists(noEnseignant)) {
			try {
				enseignantRepository.delete(noEnseignant);
			} catch (Exception e) {
				throw new SPIException("Veuillez supprimer les unités d'enseignements et les éléments constitutifs associés");
				// throw new SPIException("Veuillez d'abord supprimer les unités d'enseignements et/ou éléments constitutifs associées");
			}
		} else
			throw new SPIException("l'enseignant que vous souhaitez supprimer n'exsite pas");
	}

	/**
	 * Getter du repository
	 *
	 * @return getter
	 */
	public final EnseignantRepository getEnseignantRepository() {
		return enseignantRepository;
	}

	/**
	 * Setter du repository
	 *
	 * @param enseignantRepository
	 *            setter
	 */
	public final void setEnseignantRepository(final EnseignantRepository enseignantRepository) {
		this.enseignantRepository = enseignantRepository;
	}
}