package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.dao.UniteEnseignementRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 */
@Service
public class UniteEnseignementService {

	@Autowired
	private UniteEnseignementRepository uniteEnseignementRepository;

	/**
	 * Methode d'ajout d'une unité d'enseignement
	 *
	 * @param uniteEnseignement
	 *            l'entité à ajouter
	 * @return l'unité d'enseignement ajoutée
	 */
	public UniteEnseignement addUniteEnseignement(final UniteEnseignement uniteEnseignement) {
		if (uniteEnseignementRepository.exists(uniteEnseignement.getUniteEnseignementPK()))
			throw new SPIException("l'unité d'enseignement que vous souhaitez ajouter exsite déjà ");
		return uniteEnseignementRepository.save(uniteEnseignement);
	}

	/**
	 * Methode de suppression d'une unité d'enseignement
	 *
	 * @param uniteEnseignementPK
	 *            l'id de l'unité d'enseignement
	 */
	public void deleteUniteEnseignement(UniteEnseignementPK uniteEnseignementPK) {
		if (uniteEnseignementRepository.exists(uniteEnseignementPK)) {
			try {
				uniteEnseignementRepository.delete(uniteEnseignementPK);
			} catch (SPIException e) {
				throw new SPIException("Veuillez d'abors supprimer les éléments constitutifs et/ou les formations associées");
			}
		} else
			throw new SPIException("l'unité d'enseignement que vous souhaitez supprimer n'exsite pas ");
	}

	/**
	 * Methode de test de l'existance d'une unite d'enseignement
	 *
	 * @param uniteEnseignementPK
	 *            l'id de l'unité d'enseignement
	 * @return resultat du test
	 */
	public Boolean existUniteEnseignement(final UniteEnseignementPK uniteEnseignementPK) {
		final Boolean exist = uniteEnseignementRepository.exists(uniteEnseignementPK);
		if (exist)
			return exist;
		else
			throw new SPIException("Il n'y a aucun enseignant avec cet Id");
	}

	/**
	 * Methode de recuperation de toutes les unités d'enseignements
	 *
	 * @return liste des unités d'enseignements
	 */
	public List<UniteEnseignement> findAllUniteEnseignement() {
		List<UniteEnseignement> uniteEnseignements = (List) uniteEnseignementRepository.findAll();
		if (uniteEnseignements != null) {
			Collections.sort(uniteEnseignements, new Comparator<UniteEnseignement>() {
				@Override
				public int compare(UniteEnseignement ue1, UniteEnseignement ue2) {
					return ue1.getUniteEnseignementPK().getCodeUe().compareToIgnoreCase(ue2.getUniteEnseignementPK().getCodeUe());
				}
			});
			return uniteEnseignements;
		} else
			throw new SPIException("Aucune unité d'enseignement n'est trouvée");
	}

	public List<UniteEnseignement> findUebyEnsFormation(Integer noEnseignant, String codeFormation) {
		return uniteEnseignementRepository.findByUebyEnsFormation(noEnseignant, codeFormation);

	}

	/**
	 * Methode de recuperation des unités d'enseignements qui ont pour responsable l'enseigneant qui a le noEnseignant
	 *
	 * @param noEnseignant
	 *            l'id de l'enseignant
	 */
	public final List<UniteEnseignement> findUEsByEnseignant(final Integer noEnseignant) {
		List<UniteEnseignement> uniteEnseignements = uniteEnseignementRepository.findByNoEnseignant(noEnseignant);
		Collections.sort(uniteEnseignements, new Comparator<UniteEnseignement>() {
			@Override
			public int compare(UniteEnseignement u1, UniteEnseignement u2) {
				return u1.getUniteEnseignementPK().getCodeFormation().compareTo(u2.getUniteEnseignementPK().getCodeFormation());
			}
		});
		return uniteEnseignements;
	}

	/**
	 * Methode de recherche d'une unité d'enseignement
	 *
	 * @param uniteEnseignementPK
	 *            l'id de l'unite d'enseignement à rechercher
	 * @return l'unité d'enseignement
	 */
	public UniteEnseignement findUniteEnseignement(UniteEnseignementPK uniteEnseignementPK) {
		UniteEnseignement uniteEnseignement = uniteEnseignementRepository.findOne(uniteEnseignementPK);
		if (uniteEnseignement != null)
			return uniteEnseignement;
		else
			throw new SPIException("L'unité d'enseignement que vous cherchez n'existe pas");
	}

	public final List<UniteEnseignement> getUEsByEnseignant(final Integer noEnseignant) {
		List<UniteEnseignement> uniteEnseignements = uniteEnseignementRepository.findByNoEnseignant(noEnseignant);
		Collections.sort(uniteEnseignements, new Comparator<UniteEnseignement>() {
			@Override
			public int compare(UniteEnseignement u1, UniteEnseignement u2) {
				return u1.getUniteEnseignementPK().getCodeFormation().compareTo(u2.getUniteEnseignementPK().getCodeFormation());
			}
		});
		return uniteEnseignements;

	}

	/**
	 * Methode de modification d'une unité d'enseignement
	 *
	 * @param uniteEnseignement
	 *            l'entité à modifié
	 * @return l'unité d'enseignement modifiée
	 */
	public UniteEnseignement updateUniteEnseignement(UniteEnseignement uniteEnseignement) {
		if (uniteEnseignementRepository.exists(uniteEnseignement.getUniteEnseignementPK()))
			return uniteEnseignementRepository.save(uniteEnseignement);
		else
			throw new SPIException("l'unité d'enseignement que vous souhaitez modifier n'exsite pas ");

	}
}
