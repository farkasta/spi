package fr.univbrest.dosi.spi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.dao.EnseignantRepository;
import fr.univbrest.dosi.spi.dao.EtudiantRepository;
import fr.univbrest.dosi.spi.dao.FormationRepository;

@Service
public class StatistiqueService {
	@Autowired
	EtudiantRepository etudiantRepository;

	@Autowired
	EnseignantRepository enseignantRepository;

	@Autowired
	FormationRepository formationRepository;

	public Long StatEtudiants() {

		Long nombre = etudiantRepository.count();
		return nombre;
	}

	public Long StatEnseignants() {

		Long nombre = enseignantRepository.count();
		return nombre;
	}

	public Long StatFormations() {

		Long nombre = formationRepository.count();
		return nombre;
	}
}
