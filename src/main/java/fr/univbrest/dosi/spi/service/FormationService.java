package fr.univbrest.dosi.spi.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univbrest.dosi.spi.bean.ElementConstitutif;
import fr.univbrest.dosi.spi.bean.Formation;
import fr.univbrest.dosi.spi.bean.UniteEnseignement;
import fr.univbrest.dosi.spi.bean.UniteEnseignementPK;
import fr.univbrest.dosi.spi.dao.ElementConstitutifRepository;
import fr.univbrest.dosi.spi.dao.FormationRepository;
import fr.univbrest.dosi.spi.dao.UniteEnseignementRepository;
import fr.univbrest.dosi.spi.exception.SPIException;

/**
 * @author DOSI
 */
@Service
public class FormationService {

	/**
	 * Injection des trois Repository de (formation, elementConstitutif et uniteEnseignement) dont on aura besoin
	 */

	@Autowired
	private FormationRepository formationRepository;

	private ElementConstitutifRepository elementConstitutifRepository;

	@Autowired
	private UniteEnseignementRepository uniteEnseignementRepository;

	/**
	 * Methode d'ajout d'une formation
	 *
	 * @param formation
	 *            que nous voulons ajouter
	 * @return la formation ajoutée
	 */
	public final Formation addFormation(final Formation formation) {
		if (formationRepository.exists(formation.getCodeFormation()))
			throw new SPIException("Cette formation existe déjà");
		else
			return formationRepository.save(formation);

	}

	/**
	 * Methode de suppression d'une formation
	 *
	 * @param codeFormation
	 *            l'id de formation
	 */
	public final void deleteFormation(final String codeFormation) {
		if (formationRepository.exists(codeFormation)) {
			try {
				formationRepository.delete(codeFormation);
			} catch (Exception e) {
				throw new SPIException("Veuillez d'abors supprimer les éléments constitutif associées");
			}
		} else
			throw new SPIException("La formation que vous souhaitez supprimer n'exsite pas");

	}

	/**
	 * Methode de test de l'existance d'une formation
	 *
	 * @param code
	 *            l'id de formation
	 * @return resultat du test
	 */
	public final Boolean existeFormation(final String code) {
		return formationRepository.exists(code);
	}

	public List<ElementConstitutif> getElementConstitutif(String codeUe) {

		List<ElementConstitutif> elementConstitutifs = elementConstitutifRepository.findByCodeUe(codeUe);
		return elementConstitutifs;
	}

	/**
	 * @param codeUe
	 *            ( code formation + code_ue) l id de l unite d enseignement ( le code formation on l a deja)
	 * @return la liste des element Constitutifs qui appartiennet a l uniet d enseignement passée en paramtre
	 */
	public List<ElementConstitutif> getElementConstitutifByUE(String codeUe) {

		return elementConstitutifRepository.findByCodeUe(codeUe);

	}

	/**
	 * @param uniteEnseignementPK
	 *            l'id de l'elementconstitutif
	 * @return list d'element constitutif
	 */
	public List<ElementConstitutif> getElementConstitutifByUePK(UniteEnseignementPK uniteEnseignementPK) {
		UniteEnseignement uniteEnseignement = uniteEnseignementRepository.findOne(uniteEnseignementPK);
		List<ElementConstitutif> elementConstitutifs = (List<ElementConstitutif>) uniteEnseignement.getElementConstitutifCollection();
		Collections.sort(elementConstitutifs, new Comparator<ElementConstitutif>() {
			@Override
			public int compare(ElementConstitutif ec1, ElementConstitutif ec2) {

				return ec1.getElementConstitutifPK().getCodeEc().compareTo(ec2.getElementConstitutifPK().getCodeEc());
			}
		});
		return elementConstitutifs;
	}

	/**
	 * Cette fonction nous retourne une formation qu'on cherche par son code
	 *
	 * @param code
	 *            de la formation qu'on veux verifier son existance dans notre base de données
	 * @return la formation recherchée
	 */
	public final Formation getFormation(final String code) {
		return formationRepository.findOne(code);
	}

	/**
	 * @param codeFormation
	 *            l id de la formation
	 * @return la liste des unite d enseignement qui appartiennet a la formation passée en parametres
	 */
	public List<UniteEnseignement> getUEsByFormation(final String codeFormation) {

		List<UniteEnseignement> uniteEnseignements = uniteEnseignementRepository.findByCodeFormation(codeFormation);

		Collections.sort(uniteEnseignements, new Comparator<UniteEnseignement>() {
			@Override
			public int compare(UniteEnseignement ue1, UniteEnseignement ue2) {
				return ue1.getUniteEnseignementPK().getCodeUe().compareToIgnoreCase(ue2.getUniteEnseignementPK().getCodeUe());
			}
		});
		return uniteEnseignements;

	}

	/**
	 * Cette fonction nous liste toutes nos formations existantes dans notre base de données
	 *
	 * @return Liste de formations
	 */

	public final Iterable<Formation> listFormations() {
		List<Formation> formations = (List<Formation>) formationRepository.findAll();
		Collections.sort(formations, new Comparator<Formation>() {
			@Override
			public int compare(Formation f1, Formation f2) {
				return f1.getCodeFormation().compareToIgnoreCase(f2.getCodeFormation());
			}

		});
		return formations;
	}

	/**
	 * Methode de modification pour formation
	 *
	 * @param formation
	 *            de l'entité formation
	 * @return la formation modifier
	 */
	public final Formation updateFormation(final Formation formation) {

		return formationRepository.save(formation);
	}

}
