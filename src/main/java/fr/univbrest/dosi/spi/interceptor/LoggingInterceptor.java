package fr.univbrest.dosi.spi.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import fr.univbrest.dosi.spi.bean.Authentification;
import fr.univbrest.dosi.spi.exception.SPIException;

@Component
public class LoggingInterceptor extends HandlerInterceptorAdapter {

	// @Override
	// public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
	// final String authToken = request.getHeader("AuthToken");
	// }

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
		// Set Request Attribute(TODO)
		final Authentification user = (Authentification) request.getSession().getAttribute("user");
		if (user.getRole().equalsIgnoreCase("adm"))
			return true;
		else
			throw new SPIException("Vous devez vous connecter en tant qu'administrateur");

	}
}