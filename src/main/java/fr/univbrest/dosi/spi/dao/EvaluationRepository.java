package fr.univbrest.dosi.spi.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.Evaluation;

/**
 * @author DOSI
 */
@RepositoryRestResource(collectionResourceRel = "evaluation", path = "evalution")
public interface EvaluationRepository extends PagingAndSortingRepository<Evaluation, Long> {

	List<Evaluation> findByNoEnseignant(@Param("noEnseignant") Integer noEnseignant);

	List<Evaluation> findByPromotion(@Param("codeFormation") String codeFormation, @Param("anneeUniversitaire") String anneeUniversitaire);
}
