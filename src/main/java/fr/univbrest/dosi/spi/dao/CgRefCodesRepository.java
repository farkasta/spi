package fr.univbrest.dosi.spi.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.CgRefCodes;

@RepositoryRestResource(collectionResourceRel = "cgrefcodes", path = "cgrefcodes")
public interface CgRefCodesRepository extends PagingAndSortingRepository<CgRefCodes, BigDecimal> {

	List<CgRefCodes> findByRvDomain(@Param("rvDomain") String rvDomain);

	// List<CgRefCodes> findByRvDomain(@Param("rvDomain") String rvDomain);
	CgRefCodes findByRvLowValue(@Param("rvLowValue") String rvLowValue);

}
