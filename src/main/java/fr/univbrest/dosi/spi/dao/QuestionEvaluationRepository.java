package fr.univbrest.dosi.spi.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.QuestionEvaluation;

@RepositoryRestResource(collectionResourceRel = "questionevaluation", path = "questionevaluation")
public interface QuestionEvaluationRepository extends PagingAndSortingRepository<QuestionEvaluation, Long> {

	QuestionEvaluation findByIdQuestionEvaluation(@Param("idQuestionEvaluation") Long idQuestionEvaluation);

	List<QuestionEvaluation> findByIdRubriqueEvaluation(@Param("idRubriqueEvaluation") Long idRubriqueEvaluation);

}
