package fr.univbrest.dosi.spi.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.RubriqueEvaluation;

@RepositoryRestResource(collectionResourceRel = "rubriqueevaluation", path = "rubriqueevaluation")
public interface RubriqueEvaluationRepository extends PagingAndSortingRepository<RubriqueEvaluation, Long> {

	List<RubriqueEvaluation> findByIdEvaluation(@Param("idEvaluation") Long idEvaluation);

}
