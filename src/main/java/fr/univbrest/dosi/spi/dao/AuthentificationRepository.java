package fr.univbrest.dosi.spi.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.Authentification;

/**
 * @author DOSI
 */
@RepositoryRestResource(collectionResourceRel = "authentification", path = "authentification")
public interface AuthentificationRepository extends PagingAndSortingRepository<Authentification, String> {
	/**
	 * @param idAuthentififcation
	 *            l'id de l'authentification
	 * @return liste des utilisateurs
	 */
	Authentification findByLoginConnection(@Param("loginConnection") String loginConnection);

	Authentification findByPseudoConnection(@Param("pseudoConnection") String pseudoConnection);

	Authentification findEnseignantByIdConnection(@Param("idConnection") Long idConnection);

	// Authentification findEtudiantByIdConnection(@Param("idConnection") Long idConnection);

}
