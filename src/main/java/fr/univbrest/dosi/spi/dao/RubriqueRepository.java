package fr.univbrest.dosi.spi.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.RequestBody;

import fr.univbrest.dosi.spi.bean.Rubrique;

@RepositoryRestResource(collectionResourceRel = "rubrique", path = "rubrique")
public interface RubriqueRepository extends PagingAndSortingRepository<Rubrique, Long> {

	@Override
	Rubrique save(@RequestBody Rubrique rubrique);

}
