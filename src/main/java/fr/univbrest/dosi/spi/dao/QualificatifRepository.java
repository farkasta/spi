package fr.univbrest.dosi.spi.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import fr.univbrest.dosi.spi.bean.Qualificatif;

/**
 * Classe DAO pour interagir avec la base de données concarnant la partie qualificatif
 *
 * @author DOSI
 */
@RepositoryRestResource(collectionResourceRel = "qualificatif", path = "qualificatif")
public interface QualificatifRepository extends PagingAndSortingRepository<Qualificatif, Long> {

}
