(function() {
  'use strict';
  angular.module('app.controllers', ['app.auth']).controller('AppCtrl', [
    '$scope', '$location', 'AuthService', function($scope, $location, AuthService) {
      $scope.isSpecificPage = function() {
        var path;
        path = $location.path();
        return _.contains(['/404', '/pages/500', '/pages/login', '/pages/signin', '/pages/signin1', '/pages/signin2', '/pages/signup', '/pages/signup1', '/pages/signup2', '/pages/forgot', '/pages/lock-screen'], path);
      };
      $scope.deconnexion = function() {
    	  
    	  AuthService.deconnexion().success(function() {
    		  $location.path('/pages/signin');
    	  });
    	  
      };
      
      $scope.login = function() {
    	  $location.path('/pages/signin');
      };
      
      return $scope.main = {
        brand: 'Square',
        name: 'Lisa Doe'
      };
    }
  ]).factory('DashboardFactory', ['$http',function($http){ 
		
		return {
		
		 // return le nombre des etudiants dans la Table etudiant
		getNbrEtudiant : function(){
			return $http.get('http://localhost:8090/Statistique/countEtudiant');
		},
		
		 // return le nombre des enseignants dans la Table enseignant
		getNbrEnseignant : function(){
			return $http.get('http://localhost:8090/Statistique/countEnseignant');
		},
		
		 // return le nombre des formations dans la Table formation
		getNbrFormation : function(){
			return $http.get('http://localhost:8090/Statistique/countFormation');
		}
		} ;
	}]).controller('NavCtrl', [
    '$scope', 'taskStorage', 'filterFilter', function($scope, taskStorage, filterFilter) {
      var tasks;
      tasks = $scope.tasks = taskStorage.get();
      $scope.taskRemainingCount = filterFilter(tasks, {
        completed: false
      }).length;
      return $scope.$on('taskRemaining:changed', function(event, count) {
        return $scope.taskRemainingCount = count;
      });
    }
  ]).controller('DashboardCtrl', ['$scope','DashboardFactory', function($scope, DashboardFactory) {
		var promiseEtudiant = DashboardFactory.getNbrEtudiant();
		promiseEtudiant.success(function(data) {
    		    $scope.statistiquesEtudiant = data;
    		    console.log($scope.statistiquesEtudiant);
    		  }
    		)
    		.error(function(data) {
    			 $scope.error = 'aucun personne ';
    		  }
    		);
    	
    	var promiseEnseignant = DashboardFactory.getNbrEnseignant();
    	promiseEnseignant.success(function(data) {
    		    $scope.statistiquesEnseignant = data;
    		    console.log($scope.statistiquesEnseignant);
    		  }
    		)
    		.error(function(data) {
    			 $scope.error = 'aucun personne ';
    		  }
    		);
    	
    	var promiseFormation = DashboardFactory.getNbrFormation();
    	promiseFormation.success(function(data) {
    		    $scope.statistiquesFormation = data;
    		    console.log($scope.statistiquesFormation);
    		  }
    		)
    		.error(function(data) {
    			 $scope.error = 'aucun personne ';
    		  }
    		);
	  
  }]);

}).call(this);

