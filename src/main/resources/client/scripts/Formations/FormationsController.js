(function() {
  'use strict';

  var app = angular.module('app.formations', []);
  
  Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? null : v;
	   });
	   this.length = 0; //clear original array
	   this.push.apply(this, array); //push all elements except the one we want to delete
	}
 
  Array.prototype.retourValue = function(name, value){
	   var array = $.map(this, function(v,i){
	      return v[name] === value ? v : null;
	   });
	   return array[0];
	}
  
   

  app.factory('formationsFactory',['$http', function($http){  
	  
	  var UE =   {"uniteEnseignementPK":{"codeFormation":"M2DOSI","codeUe":"ISI"},
			  "designation":"Ingénierie des Systèmes d'Information",
			  "semestre":  9  ,
			  "description":null,
			  "nbhCm":20,
			  "nbhTd":20,
			  "nbhTp":20}  
	  
    return {

      // renvoi la liste de tous les formations
      all: function() { // TODO retourner la liste 
    	  return  $http.get('http://localhost:8090/formations') //list,//;
    	  //console.log("TODO : retourner la liste des formations", formation);
    	  },
    	  
      // renvoi la formation avec le code demandé
      get: function(codeFormation) { 
        // TODO Retourner un enregistrement
    	  console.log("TODO : get formation",codeFormation); 
    	  return  $http.get('http://localhost:8090/formation/'+codeFormation);
    	  
      },
      listdiplome: function(){
		    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/DIPLOME')
		},
		listOuiNon: function(){
		    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/OUI_NON')
		},
         // renvoi la liste des UE pour une formation donnée
      listeUE: function(codeFormation) {
			console.log(codeFormation);
			return $http.get('http://localhost:8090/formation/finduesbyformation/'+codeFormation);
		},
		
		   // renvoi la liste des ECs pour un UE 
		 listeEC: function(codeFormation,codeUe) {

			 var uniteEnseignementpk = {
						"codeFormation" : codeFormation,
						"codeUe" : codeUe,
					};
			     console.log("TODO :  uniteEnseignementpk",uniteEnseignementpk);
				return $http.post('http://localhost:8090/formation/findecsbyuniteenseignement',uniteEnseignementpk);
			},
			
			// cette méthode pour ajoute une formation
      set: function(formation,edit) {
        var idf = formation.codeFormation;
        
        if(idf){
        	var newFormation = {
					"codeFormation" : formation.codeFormation,
					"diplome" : formation.diplome,
					"n0Annee" : formation.n0Annee,
					"nomFormation" : formation.nomFormation,
					"doubleDiplome" : formation.doubleDiplome,
					"debutAccreditation" : formation.debutAccreditation,
					"finAccreditation" : formation.finAccreditation
					
				};      	
        	if(edit)
        		return $http.post('http://localhost:8090/formation/update',newFormation);
        	else
        		return $http.post('http://localhost:8090/formation/ajouterformation',newFormation);
        }

      },

      delete: function(codeFormation) { 
        console.log("TODO : supprimer formation", codeFormation);
        return $http.get('http://localhost:8090/formation/delete/'+codeFormation);

      },
      
      deleteUE : function(uniteEnseignementPK){
    	  return $http.post('http://localhost:8090/formation/uniteenseignement/deleteuniteenseignement',uniteEnseignementPK);
      },
      // fonction de modification d'une unite enseignement
      updateUE : function(uniteEnseignement){
    	  	var enseignant = "http://localhost:8090/enseignant/findenseignant/"+uniteEnseignement.noEnseignant.noEnseignant;
  			var formation = "http://localhost:8090/formation/"+uniteEnseignement.uniteEnseignementPK.codeFormation;
          	var newUniteEnseignement = {
          			"uniteEnseignementPK":{"codeFormation":uniteEnseignement.uniteEnseignementPK.codeFormation,"codeUe":uniteEnseignement.uniteEnseignementPK.codeUe},
      			    "designation":   uniteEnseignement.designation,
      			    "semestre":  uniteEnseignement.semestre,
      			    "description":uniteEnseignement.description,
        			"nbhCm":uniteEnseignement.nbhCm,
        			"nbhTd":uniteEnseignement.nbhTd,
        			"nbhTp":uniteEnseignement.nbhTp,
        			"formation":formation,
          			"noEnseignant":enseignant
  			          	              };
          	console.log(newUniteEnseignement);
  			return	$http.post('http://localhost:8090/formation/uniteenseignement/updateuniteenseignement',newUniteEnseignement); 
      },
      
      //fonction d'ajout d'une unite enseignement
      addUE : function (uniteEnseignement) { 
        		console.log(uniteEnseignement);
        		var enseignant = "http://localhost:8090/enseignant/findenseignant/"+uniteEnseignement.noEnseignant.noEnseignant;
        		var formation = "http://localhost:8090/formation/"+uniteEnseignement.uniteEnseignementPK.codeFormation;
           	    var newUniteEnseignement = {
           			"uniteEnseignementPK":{"codeFormation":uniteEnseignement.uniteEnseignementPK.codeFormation,"codeUe":uniteEnseignement.uniteEnseignementPK.codeUe},
       			    "designation":   uniteEnseignement.designation,
       			    "semestre":      uniteEnseignement.semestre  ,
       			    "description":uniteEnseignement.description,
         			"nbhCm":uniteEnseignement.nbhCm,
         			"nbhTd":uniteEnseignement.nbhTd,
         			"nbhTp":uniteEnseignement.nbhTp,
         			"formation":formation,
         			"noEnseignant":enseignant
   			          	              };
           	console.log(newUniteEnseignement);
          return	$http.post('http://localhost:8090/formation/uniteenseignement/adduniteenseignement',newUniteEnseignement);
          	
      },
      
      getUE : function (codeFormation,codeUe){
    	      var uniteEnseignementpk = {"codeFormation":codeFormation,"codeUe":codeUe}
    	  return $http.post('http://localhost:8090/formation/uniteenseignement/finduniteenseignement',uniteEnseignementpk); 
      },

      
      
      ListEnseignants: function(){
		    return $http.get('http://localhost:8090/enseignant/findallenseignant')
		}, 
		
		
		
      addEC : function (elementconstitutif){
       var enseignant = "http://localhost:8090/enseignant/findenseignant/"+elementconstitutif.noEnseignant.noEnseignant;
  		var formation = "http://localhost:8090/formation/"+elementconstitutif.elementConstitutifPK.codeFormation;
  		var ue = "http://localhost:8090/formation/findue/"+elementconstitutif.elementConstitutifPK.codeFormation+"/"+elementconstitutif.elementConstitutifPK.codeUe;
    	  var newElementConstitutif = {
    			"elementConstitutifPK":{"codeFormation":elementconstitutif.elementConstitutifPK.codeFormation,
    				                    "codeUe":elementconstitutif.elementConstitutifPK.codeUe,
    				                     "codeEc":elementconstitutif.elementConstitutifPK.codeEc}, 
    			"description" :elementconstitutif.description ,
    			"designation" :elementconstitutif.designation , 
    		    "formation":formation,
       			"noEnseignant":enseignant,
       			"UniteEnseignement" : ue,
       			"nbhCm" : elementconstitutif.nbhCm,
       			"nbhTd" : elementconstitutif.nbhTd,
       			"nbhTp" : elementconstitutif.nbhTp
    	  }; 
    	  
    	  return $http.post('http://localhost:8090/formation/ajouterec',newElementConstitutif);
        }, 
		
      
      
      
	  updateEC : function(elementconstitutif){
		  var enseignant = "http://localhost:8090/enseignant/findenseignant/"+elementconstitutif.noEnseignant.noEnseignant;
	  		var formation = "http://localhost:8090/formation/"+elementconstitutif.elementConstitutifPK.codeFormation;
	  		var ue = "http://localhost:8090/formation/findue/"+elementconstitutif.elementConstitutifPK.codeFormation+"/"+elementconstitutif.elementConstitutifPK.codeUe;
	    	  var newElementConstitutif = {
	    			"elementConstitutifPK":{"codeFormation":elementconstitutif.elementConstitutifPK.codeFormation,
	    				                    "codeUe":elementconstitutif.elementConstitutifPK.codeUe,
	    				                     "codeEc":elementconstitutif.elementConstitutifPK.codeEc}, 
	    			"description" :elementconstitutif.description ,
	    			"designation" :elementconstitutif.designation , 
	    		    "formation":formation,
	       			"noEnseignant":enseignant,
	       			"UniteEnseignement" : ue,
	       			"nbhCm" : elementconstitutif.nbhCm,
	       			"nbhTd" : elementconstitutif.nbhTd,
	       			"nbhTp" : elementconstitutif.nbhTp
	    	  };
		  return $http.post('http://localhost:8090/formation/updateec',newElementConstitutif);
	  }, 
	  
	  deleteEC : function(elementConstitutifPK){
		  return $http.post('http://localhost:8090/formation/deleteec',elementConstitutifPK);
	  }, 
	  
	  getEC : function(codeFormation,codeUe,codeEc){
		      var elementConstitutifPK = {"codeFormation":codeFormation,"codeUe":codeUe, "codeEc":codeEc};
		      console.log(elementConstitutifPK);
		     return $http.post('http://localhost:8090/formation/findec',elementConstitutifPK);
		     
	  }
        
    };
  }]);

// le controller de liste formation
  app.controller('FormationsController', 
    ['$scope', '$filter' ,'$location', 'formationsFactory', '$http','logger',
    function($scope, $filter ,$location, formationsFactory, $http, logger){
    	var init;
    	var promise = formationsFactory.all();
    	promise.success(function(data) {
    		    $scope.formations = data;
    		    $scope.formationsSize=$scope.formations.length;
				$scope.searchKeywords = '';
				$scope.filteredFormation = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageFormation = $scope.filteredFormation.slice(start, end);
				};
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredFormation = $filter('filter')($scope.formations, $scope.searchKeywords);
					$scope.filteredFormationsSize = $scope.filteredFormation.length;
					return $scope.onFilterChange();
				};
				$scope.order = function(rowName) {
					if ($scope.row === rowName) {
						return;
					}
					$scope.row = rowName;
					$scope.filteredFormation = $filter('orderBy')($scope.formations, rowName);
					$scope.filteredFormationsSize = $scope.filteredFormation.length;
					return $scope.onOrderChange();
				};
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageFormation = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();

			})
    		.error(function(data) {
    			 $scope.error = 'unable to get the poneys';
    		  }
    		);
    
    	
      // Crée la page permettant d'ajouter une formation
      $scope.ajoutFormation = function(){
	  $location.path('/admin/formation/nouveau'); 
      }
      
      // Créé la page permettant de modifier une formation
      // TODO Lien vers la page permettant de modifier une formation 
      $scope.updateFormation = function(codeFormation){
          $location.path("/admin/formation/"+ codeFormation); 
       }
      

      //creer la page permettant d'afficher les details d 'une formation
      $scope.showDetails = function(codeFormation){
        $location.path("/admin/formation/"+ codeFormation+"/details");
      }

      // supprime une formation
      $scope.supprime = function(codeFormation){ 
    	// TODO Suppression d'une rubrique de la liste
		    swal({
				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
				   imageSize: '50x50',
		   		   title: "Etes vous sûr de vouloir le supprimer?",
		   		   text: "La suppression est irréversible !",
		   		   type: "warning",
		   		   showCancelButton: true,
		   		   confirmButtonText: "Oui, confirmer!",
		   		   closeOnConfirm: true}, 
		   		   function(){ 
					    	  var promise= formationsFactory.delete(codeFormation);
					    	  	promise.success(function(data,statut){
								$scope.currentPageFormation.removeValue("codeFormation",codeFormation);
									
									//Pour actualiser le numéro d elements affichés à côté de la bare de recherche
									
									$scope.formationsSize = $scope.formations.length-1;
						        	$scope.filteredFormationsSize = $scope.filteredFormation.length-1;
						        	
						        	//message
						        	logger.logSuccess("Formation supprimée avec succès");
								})
								.error(function(data,statut){
									logger.logError("Suppression impossible : " + data.message);
								});
      				});
    	  	
      }
 }]);
  
  // le controller pour les détails d'une formation

  app.controller('FormationDetailsController', 
    ['$scope','$filter', '$routeParams', '$location', 'formationsFactory','logger',
    function($scope, $filter, $routeParams, $location, formationsFactory,logger){         

    	$scope.updateFormation = function(codeFormation){
            $location.path("/admin/formation/"+ codeFormation); 
         }
    	
      $scope.edit2 = false;
      $scope.updateFormation = function(codeFormation){
          $location.path("/admin/formation/"+ codeFormation); 
       }
      // si creation d'une nouvelle formation
      if($routeParams.id == "nouveau"){
        $scope.formation= { };
        $scope.frm= null;
        
      //Charger le contenu du combobox de diplome
        var promiseDiplome = formationsFactory.listdiplome();
        promiseDiplome.success(function(data,status){
			$scope.formation.diplome = "D";
    		$scope.listdiplome = data ; 
    	})
          .error(function(data,status){
                	console.log("impossible de recuperer la liste des diplomes") ;
          }); 
        
      //Charger le contenu du combobox de double diplome
        var promiseOuiNon = formationsFactory.listOuiNon();
        promiseOuiNon.success(function(data,status){
			$scope.formation.doubleDiplome = "N";
    		$scope.listouinon = data ; 
    	})
          .error(function(data,status){
                	console.log("impossible de recuperer la liste des oui non") ;
          }); 
        
        $scope.edit= true; 
        
      } else if($routeParams.details == "details"){ 
    	  
//    	sinon on modifie une formation existante
        var promise = formationsFactory.get($routeParams.id);
            promise.success(function(data,status) {
			$scope.formation = data ;
			$scope.formation.debutAccreditation = new Date($scope.formation.debutAccreditation);
			$scope.formation.finAccreditation = new Date($scope.formation.finAccreditation);
	        $scope.frm= "bla bla";
			$scope.formation.diplome = data.diplome;
			$scope.formation.doubleDiplome = data.doubleDiplome;
			var promiseDiplome = formationsFactory.listdiplome();
	        promiseDiplome.success(function(data,status){
	    		$scope.listdiplome = data ; 
	    	})
	          .error(function(data,status){
	                	console.log("impossible de recuperer la liste des diplomes") ;
	          }); 
	        var promiseOuiNon = formationsFactory.listOuiNon();
	        promiseOuiNon.success(function(data,status){
	    		$scope.listouinon = data ; 
	    	})
	          .error(function(data,status){
	                	console.log("impossible de recuperer la liste des oui non") ;
	          }); 
			$scope.edit = true;
//        	console.log("Dans FormationDetailsController : success : "+$scope.formation.nomFormation);
			//$scope.formation = JSON.parse(JSON.stringify(data));
		  }
		)
		 .error(function(data,status){
	        	console.log("erreur de recupere la formation choisi");
	      });
        
        // TODO coder une fonction cancel permettant de modifier une formation et rediriger vers /admin/formations
        // $scope.afficher = false;
    	//recuperer la liste des UEs apour une formation donnée
        $scope.afficherUE = false;
      	var Uepromise = formationsFactory.listeUE($routeParams.id);
        	Uepromise.success(function(data,status){
        		 var init;
        		console.log(data);
        		if(data!=0){
        		$scope.formation.ue = data ; 
        		$scope.afficherUE = true;
        		$scope.searchKeywords = '';
				$scope.filteredUE = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageUE = $scope.filteredUE.slice(start, end);
				};
				
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredUE = $filter('filter')($scope.formation.ue, $scope.searchKeywords);
					return $scope.onFilterChange();
				};
				
//				$scope.order = function(rowName) {
//					if ($scope.row === rowName) {
//						return;
//					}
//					$scope.row = rowName;
//					$scope.filteredPromotion = $filter('orderBy')($scope.promotion.etudiants, rowName);
//					return $scope.onOrderChange();
//				};
        		
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageUE = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();
				
        		
        		}else{
      			$scope.afficherUE = false;
      			logger.logWarning("Cette formation ne contient aucune unité d'enseignement");
      		//	$scope.elements =" Pas des élements constitutif pour cet unité d'enseignement"
      		}
        	})
                     .error(function(data,status){
                    	console.log("impossible de recuperer la liste des UE") ;
                     })	; 
  	
//        	$scope.showec = function(uniteEns){
//        		// promise pour récuperer la liste des EC pour un UE selon son CodeFormation et CodeUE
//        		$scope.afficher = false;
//        		var promiseElement = formationsFactory.listeEC(uniteEns.uniteEnseignementPK.codeFormation,uniteEns.uniteEnseignementPK.codeUe);
//         	    promiseElement.success(function(data,status){
//      		console.log(data);
//      		console.log(data.designation);
//      		//console.log(data.elementConstitutifPK.codeEc);
//      		if(data!=0){
//      		$scope.elements = data ; 
//      		$scope.afficher = true;
//      		}else{
//      			$scope.afficher = false;
//      			logger.logWarning("Cette unite d'enseignement ne contient aucun èlèment constitutif");
//      		//	$scope.elements =" Pas des élements constitutif pour cet unité d'enseignement"
//      		}
//      			 
//      	})
//                   .error(function(data,status){
//                  	 //logger.logError("Supression impossible! : " + data.message);
//                  	console.log("impossible de recuperer la liste des EC") ;
//                   })	;  
//                     
//        	}
      }else{
    	  $scope.edit2 = true ;
      	// sinon on modifie une formation existante
          var promise = formationsFactory.get($routeParams.id);
          promise.success(function(data,status) {
     
  			$scope.formation = data ;
  	        $scope.formation.debutAccreditation= new Date(data.debutAccreditation);
 			$scope.formation.finAccreditation= new Date(data.finAccreditation);
	        $scope.frm= "bla bla";
  			$scope.formation.diplome = data.diplome;
  			$scope.formation.doubleDiplome = data.doubleDiplome;
  			var promiseDiplome = formationsFactory.listdiplome();
  	        promiseDiplome.success(function(data,status){
  	    		$scope.listdiplome = data ; 
  	    	})
  	          .error(function(data,status){
  	                	console.log("impossible de recuperer la liste des diplomes") ;
  	          }); 
  	        var promiseOuiNon = formationsFactory.listOuiNon();
  	        promiseOuiNon.success(function(data,status){
  	    		$scope.listouinon = data ; 
  	    	})
  	          .error(function(data,status){
  	                	console.log("impossible de recuperer la liste des oui non") ;
  	          }); 
  			$scope.edit = true;
//          	console.log("Dans FormationDetailsController : success : "+$scope.formation.nomFormation);
  			//$scope.formation = JSON.parse(JSON.stringify(data));
  		  }
  		)
  		 .error(function(data,status){
  	        	console.log("erreur de recupere la formation choisi");
  	      });
      }      


      // valide le formulaire d'édition d'une formation
      $scope.submit = function(){
			$scope.formation.debutAccreditation = new Date($scope.formation.debutAccreditation).getTime();
			$scope.formation.finAccreditation = new Date($scope.formation.finAccreditation).getTime();
    	  var promiseAjout = formationsFactory.set($scope.formation,$scope.edit2);
    	  promiseAjout.success(function(data,status){
    		  logger.logSuccess("Succès : Insertion de la formation réussie!");
    		  $location.path('/admin/formations');
    	  }).error(function(data,status){
    		  logger.logError("Erreur d'insertion : "+data.message);
    	  });
    	  
           
        }
      
      // annule l'édition
      $scope.cancel = function(){ 
       // si ajout d'une nouvelle formation => retour à la liste des rubriques
			if(!$scope.formation.codeFormation){
				$location.path('/admin/formations');
			}
			else {
		          var r = formationsFactory.get($routeParams.id);
					r.success(function(data) {
						$scope.formation = JSON.parse(JSON.stringify(data));
						$location.path('/admin/formations');
					})
					.error(function(data){
						console.log("impossible de re-recuperer la formation choisie");
					});
			}
    }
      
      //Suppression un UE
      // supprime une formation
      $scope.supprimeUE = function(uniteEnseignement){  
    	swal({
    	   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
		   imageSize: '50x50',
   		   title: "Etes vous sûr de vouloir le supprimer?",
   		   text: "La supression est irréversible !",
   		   type: "warning",
   		   showCancelButton: true,
   		   confirmButtonText: "Oui, confirmer!",
   		   closeOnConfirm: true}, 
   		   function(){
			    	  var promise= formationsFactory.deleteUE(uniteEnseignement.uniteEnseignementPK);
						console.log(uniteEnseignement.uniteEnseignementPK);

			    	           	promise.success(function(data,statut){
			    	           		
						    	$scope.currentPageUE.removeValue("uniteEnseignementPK",uniteEnseignement.uniteEnseignementPK);
						    	$location.path("/admin/formation/"+$scope.formation.codeFormation+"/details");
			//				//Pour actualiser le numéro d elements affichés à côté de la bare de recherche
							
								$scope.formation.ueSize = $scope.formation.ue.length-1;
					        	$scope.filteredUESize = $scope.filteredUE.length-1;					        	
					        	//message
					        	logger.logSuccess("Unité d'enseignement supprimée avec succès");				        	
//					            $location.path("/admin/formation/"+$scope.formation.codeFormation+"/details");	

						})
						.error(function(data,statut){
			//				console.log("impossible de supprimer l'unite d'enseignement choisie");
							logger.logError("Suppression impossible : " + data.message);
						}); 
	   		   });
      }
      
      $scope.ajoutUniteEnseignement = function(codeFormation){
          $location.path('/admin/ues/nouveau/'+codeFormation); 
        }
      $scope.updateUniteEnseignement = function(uniteEns){
          $location.path("/admin/uesmodif/"+uniteEns.uniteEnseignementPK.codeFormation+"/"+uniteEns.uniteEnseignementPK.codeUe); 
       }
      
      $scope.detailsUe = function(uniteEns){
    	  $location.path("/admin/uesdetail/"+uniteEns.uniteEnseignementPK.codeFormation+"/"+uniteEns.uniteEnseignementPK.codeUe+"/detailsue"); 
      }
            	
    }]);
  
//UE controller
  app.controller('FormationsUEController', 
		    ['$scope', '$filter', '$routeParams', '$location', 'formationsFactory', '$http','logger',
		    function($scope, $filter, $routeParams ,$location, formationsFactory, $http, logger){
		    // si creation d'une nouvelle formation
		    	
		    	
		    	$scope.updateUniteEnseignement = function(uniteEns){
		            $location.path("/admin/uesmodif/"+uniteEns.uniteEnseignementPK.codeFormation+"/"+uniteEns.uniteEnseignementPK.codeUe); 
		         }
		        if($routeParams.id == "nouveau"){
		          $scope.UE= {"uniteEnseignementPK": {}};
		          $scope.unitens = null;
		            //Recuperer la liste des enseignants.
			    	var promiseenseignant = formationsFactory.ListEnseignants();
			    	promiseenseignant.success(function(data,status){
		 	    		$scope.enseignantsFormation = data ; 
		 	    		 
		 	    		
		 	    	}).error(function(data,status){
		                     	console.log("impossible de recuperer la liset des enseignants") ;
		            });
			    	
		          $scope.UE.uniteEnseignementPK.codeFormation = $routeParams.codeFormation ; 
		        }		        
		        
		        else if($routeParams.detailsue == "detailsue"){
		        	var promise = formationsFactory.getUE($routeParams.codeFormation,$routeParams.codeUe);
		  			   promise.success(function(data){
		  				 $scope.detailsUE = data ;
		  				console.log(data);
		  				
			      		
			  	    	var promiseElement = formationsFactory.listeEC(data.uniteEnseignementPK.codeFormation,data.uniteEnseignementPK.codeUe);
			         	 promiseElement.success(function(data,status){
			      		
			      		
						      		if(data!=0){
						      		$scope.elements = data ; 
						      		$scope.afficher = true;
						      		}else{
						      			$scope.afficher = false;
						      			logger.logWarning("Cette unité d'enseignement ne contient aucun élément constitutif");
						      		
						      		}
			      			 
			         	 })
			              .error(function(data,status){
			                  	 //logger.logError("Supression impossible! : " + data.message);
			                  	console.log("impossible de recuperer la liste des EC") ;
			              })	;
		  				
		  				
		  			})
		  			.error(function(data){
		  				console.log("impossible de recuperer les details de l'ue choisi");
		  			}); 
		        }
		          else {
		        	  
		        	  var promise = formationsFactory.getUE($routeParams.codeFormation,$routeParams.codeUe);
		  				promise.success(function(data){
		  					//$scope.detailsUE = data ;
		  					$scope.UE = data ;
		  					$scope.unitens = $routeParams.codeUe;
		  					console.log("semestre ue: =="+$scope.UE.semestre+"==");
		  					//Recuperer la liste des enseignants.
					    	var promiseenseignant = formationsFactory.ListEnseignants();
					    	promiseenseignant.success(function(data,status){
				 	    		$scope.enseignantsFormation = data ; 
				 	    	}).error(function(data,status){
				                console.log("impossible de recuperer la liste des enseignants") ;
				            });
		  				
		  				})
		          }


//		          else { 
//		        	  console.log("111");
//		        	  console.log($routeParams.codeFormation);
//		        	  console.log($routeParams.codeUe);
//			        	var promise = formationsFactory.getUE($routeParams.codeFormation,$routeParams.codeUe);
//			  			promise.success(function(data){
//			  				console.log(data);
//			  				$scope.UE = data ;		  						  				
//			  			})
//			  			.error(function(data){
//			  				console.log("impossible de recuperer les details de l'ue choisi");
//			  			});   
//		          }
		                  
		        $scope.retourDetailFormarion = function(){ 
                    history.back();
		         }
		        
		        $scope.retourDetailUe = function(){ 
                   // history.back();
		        	console.log("retourDetailsUe : "+$routeParams.codeFormation);
		        	$location.path('/admin/formation/'+$routeParams.codeFormation+'/details');
		         }
		        
		        $scope.submit = function(){
		        	if($routeParams.id == "nouveau"){
		            var promiseAjoutUE = formationsFactory.addUE($scope.UE);
		            	promiseAjoutUE.success(function(data){
		            		
		            		logger.logSuccess("Succès : Insertion de l'unité d'enseignement réussie!");
				            $location.path("/admin/formation/"+$scope.UE.uniteEnseignementPK.codeFormation+"/details");	
		            	})
		            	.error(function(data){
			  				console.log("impossible d'ajouter ");
				        	  logger.logError("Insertion impossible : " + data.message);
		            	});
		            	
		            }
		            else {
		            	
		            	var promiseMidifUE = formationsFactory.updateUE($scope.UE);
		            	promiseMidifUE.success(function(data){
		            		logger.logSuccess("Succès : Modification de l'unité d'enseignement réussie!");
				            $location.path("/admin/formation/"+ $scope.UE.uniteEnseignementPK.codeFormation+"/details");
		            	})
		            	.error(function(data){
				        	  logger.logError("Modification impossible! : " + data.message);
		            	}); 
		            }
		        }
		        
		        $scope.ajoutElementConstitutif = function(detailsUEPK){
		            $location.path("/admin/ec/nouveau/"+detailsUEPK.codeFormation+"/"+detailsUEPK.codeUe); 
		          };
		        $scope.updateElementConstitutif = function(EelementConstutitif){
                    $location.path("/admin/ecmodif/"+EelementConstutitif.elementConstitutifPK.codeFormation+"/"+EelementConstutitif.elementConstitutifPK.codeUe+"/"+EelementConstutitif.elementConstitutifPK.codeEc); 
		         };
		         
			        $scope.supprimeEC = function(ecpk){
			        	console.log("entre supprimer Ec");
			        	swal({
							   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
							   imageSize: '50x50',
				    		   title: "Etes vous sûr de vouloir le supprimer?",
				    		   text: "La supression est irréversible !",
				    		   type: "warning",
				    		   showCancelButton: true,
				    		   confirmButtonText: "Oui, confirmer!",
				    		   closeOnConfirm: true}, 
				    		   function(){
						    	  var promise= formationsFactory.deleteEC(ecpk);
						    	           	promise.success(function(data,statut){
						    	           		
									    	$scope.elements.removeValue("elementConstitutifPK",ecpk);
									    	logger.logSuccess("Elément constitutif supprimé avec succès");
//											$location.path("/admin/uesdetail/"+ecpk.codeFormation+"/"+ecpk.codeUe+"/detailsue");

									})
									.error(function(data,statut){
						//				console.log("impossible de supprimer l'unite d'enseignement choisie");
										logger.logError("Suppression impossible : " + data.message);
									}); 
				   		   });
			        	
			        };
  
		    }]);
  
//UE controller
  app.controller('FormationsEcController', 
		    ['$scope', '$filter', '$routeParams', '$location', 'formationsFactory', '$http','logger',
		    function($scope, $filter, $routeParams ,$location, formationsFactory, $http, logger){ 
 
		        if($routeParams.id == "nouveau"){
			       $scope.EC= {"elementConstitutifPK": {}};
			         $scope.ec = true ;
			            //Recuperer la liste des enseignants.
				    	var promiseenseignant = formationsFactory.ListEnseignants();
				        	promiseenseignant.success(function(data,status){
			 	    		$scope.enseignantsFormation = data ; 
			 	    	}).error(function(data,status){
			                     	console.log("impossible de recuperer la liset des enseignants") ;
			            });
				        	
		              $scope.EC.elementConstitutifPK.codeFormation = $routeParams.codeFormation; 
		              $scope.EC.elementConstitutifPK.codeUe = $routeParams.codeUe;				    	
			        }		        
			          else {
			        	  
			        	  var promise = formationsFactory.getEC($routeParams.id,$routeParams.codeUe,$routeParams.codeEc);
			        	  
			  				promise.success(function(data){

			  					$scope.EC = data ;
			  					$scope.ec = false;
			  					//Recuperer la liste des enseignants.
						    	var promiseenseignant = formationsFactory.ListEnseignants();
						    	promiseenseignant.success(function(data,status){
					 	    	$scope.enseignantsFormation = data ; 
					 	    	}).error(function(data,status){
					                     	console.log("impossible de recuperer la liste des enseignants") ;
					            });
			  				
			  				})
			          }
		        
		        $scope.retourDetailUe = function(){
		        	history.back();
		        	
		        }
		        
		        $scope.submit = function(){
		        	if($routeParams.id == "nouveau"){
		            var promiseAjoutEC = formationsFactory.addEC($scope.EC);
		            	promiseAjoutEC.success(function(data){
		            		logger.logSuccess("Succès : Insertion de l'élément constutif réussie!");
				            $location.path("/admin/uesdetail/"+$scope.EC.elementConstitutifPK.codeFormation+"/"+$scope.EC.elementConstitutifPK.codeUe+"/detailsue");	
		            	})
		            	.error(function(data){
			  				console.log("impossible d'ajouter ");
				        	  logger.logError("Insertion impossible : " + data.message);
		            	});
		            	
		            }
		            else {
		            	
		            	var promiseMidifEC = formationsFactory.updateEC($scope.EC);
		            	promiseMidifEC.success(function(data){
		            		logger.logSuccess("Succès : Modification de l'élément constutif réussie!");
		            		$location.path("/admin/uesdetail/"+$scope.EC.elementConstitutifPK.codeFormation+"/"+$scope.EC.elementConstitutifPK.codeUe+"/detailsue");
		            	})
		            	.error(function(data){
				        	  logger.logError("Modification impossible! : " + data.message);
		            	}); 
		            }
		        } 
		        		        
	
		    }]);

}).call(this);
