(function() {	
	'use strict';
	var app = angular.module('app.evaluations', []);
	
	// Methode utilisée en local pour supprimer un element d'une liste d'objets
	// Json
	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; // clear original array
		this.push.apply(this, array); // push all elements except the one we
										// want to delete
	};

	// Methode utilisée en local pour recuperer un element d'une liste d'objets
	// Json
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	};

	
	app.factory('evaluationsFactory', ['$http',function($http){
		
		return {
			// renvoi la liste de tous les evaluations
			all:function(){
				return $http.get('http://localhost:8090/evaluation/findallevaluation');
			},
			
			//renvoi la liste de tous les evaluations qui concerne un enseignant spécifique 
			getEvaBynoEnseignant(noEnseignant){
				return $http.get('http://localhost:8090/evaluation/findallevaluationbynoenseignant/'+noEnseignant);
			},
			
			// renvoi l'evaluation avec l'id demandé
			get: function(idEvaluation) {
				// revoi les evaluations
				return $http.get('http://localhost:8090/evaluation/findevaluationbyid/'+idEvaluation);
			},
			
			// supprimer une evaluation
			delete: function(idEvaluation) { 
				return $http.get('http://localhost:8090/evaluation/deleteevaluation/'+idEvaluation);
			},
			
			ListeUE : function(noEnseignant,codeFormation) {				
				return $http.get('http://localhost:8090/formation/finduebyensformation/'+noEnseignant+"/"+codeFormation);
			},
				
			listeEC: function(uePK){
				return $http.post('http://localhost:8090/formation/findecsbyuniteenseignement',uePK);
			},
			listePromotion: function(){
				return $http.get('http://localhost:8090/Promotion/findAllPromotion');
			},
			ListeEtat: function(){
			    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/ETAT-EVALUATION')
			},
			getConnectedUser: function(idUser){
			    return $http.get('http://localhost:8090/user/enseignant/'+idUser)
			},
			getPromotionInEvaluation: function(codeFormation,annee){
				var promotionPK = {
						"codeFormation" : codeFormation,
						"anneeUniversitaire" : annee
				};
				return $http.post("http://localhost:8090/Promotion/findPromotion",promotionPK);
			},
			getUEInEvaluation : function (codeFormation,codeUe){
				var uniteEnseignementPK = {
						"codeFormation" : codeFormation,
						"codeUe" : codeUe
				}
				return $http.post("http://localhost:8090/formation/finduebyuepk",uniteEnseignementPK);
			},
			getECInEvaluation : function(codeFormation,codeUe,codeEc){
				var elementConstitutifPK =  {
						"codeFormation" : codeFormation,
						"codeUe" : codeUe,
						"codeEc" : codeEc
				}
				return $http.post("http://localhost:8090/formation/findecbyecpk",elementConstitutifPK);
			},
			
			getMeaning : function (rvDomaine){
				return $http.get("http://localhost:8090/cgrefcodes/findmeaning/"+rvDomaine);
			},
			//lister les rubrique contient des question evaluation
			getRubriqueEvaluation : function (idEvaluation){
				return $http.get("http://localhost:8090/evaluation/findrubriques/"+idEvaluation);
			},
			//lister les rubriques
			ListeRubrique : function() {
				return $http.get("http://localhost:8090/rubrique/findallrubrique");
			 },
			 
			 findPromotionbyEtudiant : function(noEtudiant){
				return $http.get("http://localhost:8090/etudiant/findpromotionbyetudiant/"+noEtudiant); 
			 },
			 
			 findEvaluationPromotion : function(promotionPK){
				 return $http.post("http://localhost:8090/evaluation/findallevaluationbyetudiant",promotionPK);
			 },
			 
			 //ajouter rubrique evaluation
			 ajouterRubriqueEvalation : function(idEvaluation,idRubrique,ordre,designation){
				 	var evaluationRub = "http://localhost:8090/evaluation/findevaluationbyid/"+idEvaluation;
				 	var rubriquestand = "http://localhost:8090/evaluation/findrubriques/"+idRubrique;
					var newRubrique = {
							"ordre" : ordre,"designation" : designation,
						    "idEvaluation" : evaluationRub , "idRubrique" :rubriquestand
							};
				return $http.post('http://localhost:8090/evaluation/addRubrique',newRubrique);
				 
			 },
			 
			//supprimer une rubrique evaluation 
			 deleteRubriqueEval: function(idRubriqueEvaluation) { 
					return $http.get('http://localhost:8090/evaluation/deleterubrique/'+idRubriqueEvaluation);
			},
			 //lister les question aprtient à une rubrique evaluation
			getQuestionRubriqueEvaluation: function(idRubriqueEvaluation){
			return $http.get("http://localhost:8090/evaluation/findrubriquesquestionbyidrubrique/"+idRubriqueEvaluation);
			},
			//lister les question 
			ListeQuestion : function() {
				return $http.get("http://localhost:8090/question/findallquestion");
			 },
			 
			 //ajouter question evaluation
			 ajouterQuestionEvaluation : function(idRubriqueEvaluation,idQuestion,ordre,intitule,qualificatif){
				 	var rubriqueEval = "http://localhost:8090/evaluation/findrubriques/"+idRubriqueEvaluation;
				 	var questionstand = "http://localhost:8090/question/findquestionbyid/"+idQuestion;
				 	var qualificatif = "http://localhost:8090/qualificatif/findqualificatifbyid/"+qualificatif;
					var newQuestionEvaluation = {
							"ordre" : ordre,"intitule" : intitule,
							"idQuestion" :questionstand,
						    "idQualificatif" : null
							};
					
					var newQuestionRubriqueEvaluation = {
							"questionEvaluation" : newQuestionEvaluation,
							"idRubriqueEvaluation" : idRubriqueEvaluation
					}
				 return $http.post('http://localhost:8090/evaluation/addQuestionRubrique',newQuestionRubriqueEvaluation);
				 
			 },
			 
			//supprimer une rubrique evaluation 
			 deleteQuestionEval : function(idQuestionEvaluation) { 
					return $http.get('http://localhost:8090/evaluation/deletequestionevaluation/'+idQuestionEvaluation);
			},
			 
			// ajouter une évaluation
			set: function(evaluation,ec,ue,promotionPK,enseignant,dd,df) {
			
			if ( evaluation.idEvaluation) {
				var codeEns = "http://localhost:8090/enseignant/"+enseignant.noEnseignant;
				if (ec == undefined){
					var newEvaluation = {
							"idEvaluation" : evaluation.idEvaluation,
							"noEvaluation": evaluation.noEvaluation,
							"designation": evaluation.designation,
							"etat" : evaluation.etat,
							"periode": evaluation.periode,
							"debutReponse": dd,
							"finReponse": df,
							"codeFormation": promotionPK.codeFormation,
							"codeUe": ue,
							"codeEc": "",
							"annee": promotionPK.anneeUniversitaire,
							"noEnseignant" : codeEns
							}; 
				}
				else {
					var newEvaluation = {
							"idEvaluation" : evaluation.idEvaluation,
							"noEvaluation": evaluation.noEvaluation,
							"designation": evaluation.designation,
							"etat" : evaluation.etat,
							"periode": evaluation.periode,
							"debutReponse": dd,
							"finReponse": df,
							"codeFormation": promotionPK.codeFormation,
							"codeUe": ue,
							"codeEc": ec,
							"annee": promotionPK.anneeUniversitaire,
							"noEnseignant" : codeEns
							}; 
				}
				$http.post('http://localhost:8090/evaluation/updateevaluation',newEvaluation);
			}
			else {
				var codeEns = "http://localhost:8090/enseignant/"+enseignant.noEnseignant;
				if (ec == undefined){
					var newEvaluation = {
							"noEvaluation": evaluation.noEvaluation,
							"designation": evaluation.designation,
							"etat" : evaluation.etat,
							"periode": evaluation.periode,
							"debutReponse": dd,
							"finReponse": df,
							"codeFormation": promotionPK.codeFormation,
							"codeUe": ue,
							"codeEc": "",
							"annee": promotionPK.anneeUniversitaire,
							"noEnseignant" : codeEns
							}; 
				}
				else {
					var newEvaluation = {
							"noEvaluation": evaluation.noEvaluation,
							"designation": evaluation.designation,
							"etat" : evaluation.etat,
							"periode": evaluation.periode,
							"debutReponse": dd,
							"finReponse": df,
							"codeFormation": promotionPK.codeFormation,
							"codeUe": ue,
							"codeEc": ec,
							"annee": promotionPK.anneeUniversitaire,
							"noEnseignant" : codeEns
							}; 
				}
				$http.post('http://localhost:8090/evaluation/addevaluation',newEvaluation);
			}	    	  
			
		
		},
		
				
		};
	}]);
	
	//service rubrique
	app.service('rubriqueService', ['$http', function($http){
		this.rubriqueevaluation = null;
	}]);
	
	//service question
	app.service('questionService', ['$http', function($http){
		this.rubriqueevaluation = null;
	}]);
	// Controleur de la partie saisie d'une evaluation
	app.controller('EvaluationsController', 
    ['$rootScope','$scope', '$routeParams','$filter','$location', 'evaluationsFactory','$http','logger',
    function($rootScope,$scope, $routeParams, $filter, $location, evaluationsFactory,$http, logger){
    	
    	var init;
    	//var promiseEvaluation = evaluationsFactory.all();
    	var promiseEvaluation = evaluationsFactory.getEvaBynoEnseignant($rootScope.noEnseignant);
    	// dans le cas ou la requette arrive à s'executer
    	promiseEvaluation.success(function(data) {
		$scope.evaluations = data;
				
		$scope.searchKeywords = '';
		$scope.filteredEvaluation = [];
		$scope.row = '';
		$scope.select = function(page) {
			var end, start;
			start = (page - 1) * $scope.numPerPage;
			end = start + $scope.numPerPage;
			return $scope.currentPageEvaluation = $scope.filteredEvaluation.slice(start, end);
		};
		$scope.onFilterChange = function() {
			$scope.select(1);
			$scope.currentPage = 1;
			return $scope.row = '';
		};
		$scope.onNumPerPageChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};
		$scope.onOrderChange = function() {
			$scope.select(1);
			return $scope.currentPage = 1;
		};
		$scope.search = function() {
			$scope.filteredEvaluation = $filter('filter')($scope.evaluations, $scope.searchKeywords);
			return $scope.onFilterChange();
		};
		$scope.order = function(rowName) {
			if ($scope.row === rowName) {
				return;
			}
			$scope.row = rowName;
			$scope.filteredEvaluation = $filter('orderBy')($scope.evaluations, rowName);
			return $scope.onOrderChange();
		};
		$scope.numPerPageOpt = [3, 5, 10, 20];
		$scope.numPerPage = $scope.numPerPageOpt[2];
		$scope.currentPage = 1;
		$scope.currentPageEvaluation = [];
		
		
		init = function() {
			$scope.search();
			return $scope.select($scope.currentPage);
		};
		return init();
	})
	// dans le cas ou la requette n'arrive pas à s'executer
	.error(function(data) {
		$scope.error = 'La recuperation de la liste des question est impossible';
	});
    	
    	
    // fonction de redirection vers le formulaire d'ajout
	$scope.ajoutEvaluation = function(){
		$location.path('/ens/evaluations/nouveau/'); 
	}
	
	$scope.edit = function(idEvaluaion) {
		$location.path('/ens/evaluations/'+idEvaluaion);
	}
	
	$scope.showDetails = function(idEvaluation) {
		$location.path('/ens/evaluations/'+idEvaluation+"/details");
	}
	
	$scope.supprime = function(idEvaluation){

				swal({
					   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
					   imageSize: '50x50',
					   title: "Etes vous sûr de vouloir la supprimée?",
					   text: "La suppression est irréversible !",
					   type: "warning",
					   showCancelButton: true,
					   confirmButtonText: "Oui, confirmer!",
					   closeOnConfirm: true}, 
					   function(){ 
								var promise= evaluationsFactory.delete(idEvaluation);
								promise.success(function(data,statut){
									// $scope.question.promotions = data ;
									$scope.currentPageEvaluation.removeValue("idEvaluation",idEvaluation);
									logger.logSuccess("Evaluation supprimée avec succès");
								})
								.error(function(data,statut){
									console.log("impossible de supprimer l'evaluation choisie");
									logger.logError("Suppression impossible : " + data.message);
								});
					   });
	};
	
			
    }]);
	
	
	// Controleur de la partie saisie d'une evaluation
	app.controller('EvaluationsControllerDetails', 
    ['$rootScope','$scope', '$routeParams','$filter','$location', 'evaluationsFactory','$http', 'logger','$modal', '$log','rubriqueService','questionService',
    function($rootScope,$scope, $routeParams, $filter, $location, evaluationsFactory,$http, logger,$modal, $log,rubriqueService,questionService){
    	
    	     $scope.date={
    	    	    value: new Date(2016 ,3 , 7)
    	     };
    	     
 	    	// recuperer la liste des etats d'une evaluation
 	    	var Etatpromise = evaluationsFactory.ListeEtat();
 	    	Etatpromise.success(function(data,status){
 	    		$scope.etatevaluation = data ; 
 	    	}).error(function(data,status){
                     	console.log("impossible de recuperer l'etat de l'evaluations") ;
            }); 
 	    	
	    	// recuperer la liste des des promotions
	    	var Promotionpromise = evaluationsFactory.listePromotion();
	    	Promotionpromise.success(function(data,status){
	    		$scope.promotionevaluation = data ; 
	    	}).error(function(data,status){
                    	console.log("impossible de recuperer la listes promotions") ;
	    	}); 
    	     
    	    if ($routeParams.id=="nouveau")
    	    	{    	    	
	    	    
	    	    	
	    	    	// recuperer la liste des UE appartir de l'enseignant
	    	    	var ensPromise = evaluationsFactory.getConnectedUser($rootScope.idConnection);
	    	    	ensPromise.success(function(data,status){
	    	    		$scope.enseignantConnecte = data ; 	
	    	    	}).error(function(data,status){
	   	    		 console.log("impossible de recuperer l'enseignant connecté");
	    	    	});   	       	 	    	
    	    	}
    	    else {
    	    	// si c'est pas nouveau : la modification
    	    	$scope.edit = true ;
    	    	$scope.selectedPromotion = {};
    	    	$scope.selectedPromotion.promotionPK = {};
    	    	
    	    	// recupération de evaluation à modifier
    	    	var promoiseDetails = evaluationsFactory.get($routeParams.id);
    	    	promoiseDetails.success(function(data,status){
    	    		
    	    		$scope.evaluation = data; 
    	    	    $scope.evaluation.debutReponse= new Date(data.debutReponse);
    		  		$scope.evaluation.finReponse= new Date(data.finReponse);
    	    		
		    		// recuperer la liste des etats
		    		var promiseEtatMeaning = evaluationsFactory.getMeaning($scope.evaluation.etat);
					promiseEtatMeaning.success(function(data,status){
						$scope.etatMeaning  = data;
						console.log("etatMeaning retourné : "+$scope.etatMeaning);
					});
					    	    		
		    		// recuperer la promotion qui est concernée par l'evaluation
		    		var promisepromotion = evaluationsFactory.getPromotionInEvaluation($scope.evaluation.codeFormation,$scope.evaluation.annee);
		    		promisepromotion.success(function(data,status){
		    			$scope.selectedPromotion = data;
		    			$scope.onchangePro();
		    			console.log("promotion retourné : "+$scope.selectedPromotion);
		    		}).error(function(data,status){
		    			console.log("impossible de recuperer la promotion indiqué dans une evaluation");
		    		});
	    	    		
	    		
		    		// recuperer l'ue d'une evaluation
		    		var promiseue = evaluationsFactory.getUEInEvaluation($scope.evaluation.codeFormation,$scope.evaluation.codeUe);
		    		promiseue.success(function(data,status){
		    			$scope.selectedUE = data;
		    			$scope.onchange();
		    			console.log("ue retourné : "+$scope.selectedUE);
		    		}).error(function(data,status){
		    			console.log("impossible de recuperer la ue indiqué dans une evaluation");
		    		});
	    	    		
		    		// recuperer les rubriques
	    	    	var rubpromise = evaluationsFactory.getRubriqueEvaluation($scope.evaluation.idEvaluation);
	    	    	rubpromise.success(function(data){
	    	    		$scope.rubevaluation = data;
	    	    		console.log("rub Evaluation"+$scope.rubevaluation);
	    	    	}).error(function(data,status){
	    	    		console.log("impossible de recuperer les rubriqueEvaluatios de cette evaluation");
	    	    	});
	    	    	
	    	    	$scope.questionEvaluation = {};
	    	    	$scope.serviceRE = rubriqueService;
	    	    	$scope.serviceQE = questionService;
	    	    	// recuperer les rubriques de l'evaluation
        	    	var rubpromise = evaluationsFactory.getRubriqueEvaluation($scope.evaluation.idEvaluation);
        	    	rubpromise.success(function(data){
        	    		$scope.serviceRE.rubriqueevaluation = data;
        	    		console.log("qestion rubrique Evaluation "+$scope.questionEvaluation.intitule);
        	    	});
        	    	
        	    	
        	    	$scope.listeQuestionRubrique = function(idRubriqueEvaluation){
        	    		var questionRubEvaluationpromise = evaluationsFactory.getQuestionRubriqueEvaluation(idRubriqueEvaluation);
        		        questionRubEvaluationpromise.success(function(data){
        		    	$scope.questRubEvaluation=data;
        		        });
        	    	};
        	    	//affichage du panel hide show
        	    	$scope.oneAtATime = true;
    	    		
        	    	//affichage de rubrique dans modal	
                	// recuperer les rubriques
        	    	var rubriquepromise = evaluationsFactory.ListeRubrique();
        	    	rubriquepromise.success(function(data){
        	    		$scope.rubriqueevaluations = data;
        	    		console.log("rub Evaluation"+$scope.rubriqueevaluations);
        	    	}); 
                    //$scope.items = ["item1", "item2", "item3"];
                    $scope.open = function(idEvaluation) {
                    	console.log(idEvaluation);
                    	$scope.idEvaluation = idEvaluation;
                      var modalInstance;
                      modalInstance = $modal.open({
                        templateUrl: "myModalContent.html",
                        controller: 'ModalInstanceRubriqueCtrl',
                        resolve: {
                          items: function() {
                            return $scope.rubriqueevaluations;
                          },
                          idEvaluation : function(){
                        	  return $scope.idEvaluation;
                          }
                        }
                      });
                      modalInstance.result.then((function(selectedItem) {
                        $scope.selected = selectedItem;
                      }), function() {
                        $log.info("Modal dismissed at: " + new Date());
                      });
                    };
        	    	//fin d'affichage

        	    	//affichage de rubrique dans modal        	    	
                    // recuperer les questions
        	    	var questionpromise = evaluationsFactory.ListeQuestion();
        	    	questionpromise.success(function(data){
        	    		$scope.questionEvaluations = data;
        	    		console.log("question Evaluation"+$scope.questionEvaluations);
        	    	});
        	    	
                    //$scope.items = ["item1", "item2", "item3"];
                    $scope.openQuestion = function(idEvaluation,idRubriqueEvaluation) {
                    	console.log(idEvaluation);
                    	console.log(idRubriqueEvaluation);
                    	$scope.idEvaluation = idEvaluation;
                    	$scope.idRubriqueQuestionEvaluation = idRubriqueEvaluation;
                      var modalInstance;
                      modalInstance = $modal.open({
                        templateUrl: "Question.html",
                        controller: 'ModalInstanceQuestionCtrl',
                        resolve: {
                          itemsQuestEval: function() {
                            return $scope.questionEvaluations;
                          },
                          idEvaluation : function(){
                        	  return $scope.idEvaluation;
                          },
                          idRubriqueQuestionEvaluation : function(){
                        	  return $scope.idRubriqueQuestionEvaluation;
                          }
                        }
                      });
                      modalInstance.result.then((function(selectedItemQuestion) {
                        $scope.selectedQuestion = selectedItemQuestion;
                      }), function() {
                        $log.info("Modal dismissed Question at: " + new Date());
                      });
                    };
                    
        	    	
        	    	
        	    	
        	    	//fin d'affichage
	    	    	// recuperer Ec d'une evaluation
		    		if ($scope.evaluation.codeEc != null){
		    			var promiseec = evaluationsFactory.getECInEvaluation($scope.evaluation.codeFormation,$scope.evaluation.codeUe,$scope.evaluation.codeEc);
		    			promiseec.success(function(data,status){
		    				$scope.selectedEC = data;
		    				console.log("ec retourné : "+$scope.selectedEC);
		    			}).error(function(data,status){
		    				console.log("impossible de recuperer la ue indiqué dans une evaluation");
		    			});
		    		} 
    	    		   	    		
    	    	}).error(function(data,status){
    	    		 console.log("impossible de recuperer l'evaluation");
    	    	});
    	    	    	    	
    	    	// recuperer la liste des des promotions
    	    	var Promotionpromise = evaluationsFactory.listePromotion();
    	    	Promotionpromise.success(function(data,status){
    	    		$scope.promotionevaluation = data ; 
    	    	}).error(function(data,status){
                	console.log("impossible de recuperer les promotions") ;
                }); 
	    		
    	    	// recuperer enseignant connecté
    	    	var ensPromise = evaluationsFactory.getConnectedUser($rootScope.idConnection);
    	    	ensPromise.success(function(data,status){
    	    		$scope.enseignantConnecte = data ; 	
    	    	}).error(function(data,status){
   	    		 console.log("impossible de recuperer l'enseignant connecté");
    	    	});
     	   	    		    	
    	    	}
	    	//supprimer rubrique evaluation
            $scope.supprimeRubriqueEvaluation = function(idRubriqueEval){
            	swal({
					   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
					   imageSize: '50x50',
					   title: "Etes vous sûr de vouloir la supprimée?",
					   text: "La suppression est irréversible !",
					   type: "warning",
					   showCancelButton: true,
					   confirmButtonText: "Oui, confirmer!",
					   closeOnConfirm: true}, 
					   function(){           	
			            	var promise = evaluationsFactory.deleteRubriqueEval(idRubriqueEval);
			            	promise.success(function(data,statut){
			            		$scope.serviceRE.rubriqueevaluation.removeValue("idRubriqueEvaluation",idRubriqueEval);
			            		//$scope.rubriqueevaluation.removeValue("idRubriqueEvaluation",idRubriqueEval);
			        			logger.logSuccess("Rubrique Evaluation supprimée avec succès");
			            	})
			            	.error(function(data,statut){
			            		console.log("impossible de supprimer la rubrique evaluation choisie");
			        			logger.logError("Supression impossible : " + data.message);
			            	});
					   });
            }
            //supprimer question  evaluation 
            $scope.supprimeQuestionEvaluation = function(idRubriqueEvaluation,idQuestionEvaluation){
            	swal({
					   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
					   imageSize: '50x50',
					   title: "Etes vous sûr de vouloir le supprimer?",
					   text: "La suppression est irréversible !",
					   type: "warning",
					   showCancelButton: true,
					   confirmButtonText: "Oui, confirmer!",
					   closeOnConfirm: true}, 
					   function(){
			            	var promiseQuestionEva = evaluationsFactory.deleteQuestionEval(idQuestionEvaluation);
			            	promiseQuestionEva.success(function(data,statut){
			            		$scope.serviceRE.rubriqueevaluation.retourValue('idRubriqueEvaluation',idRubriqueEvaluation).questionEvaluationCollection.removeValue("idQuestionEvaluation",idQuestionEvaluation);
			        			logger.logSuccess("Question Evaluation supprimée avec succès");
			            	})
			            	.error(function(data,statut){
			            		console.log("impossible de supprimer la question evaluation choisie");
			            		logger.logError("Suppression impossible : " + data.message);
			            	})
					   });
            }
            
    	    // lors du click sur le button valider , appel rest pour persister
			// l'evaluation
              $scope.submit = function(){
            	  console.log("on submit");      
            	  var dd = new Date($scope.evaluation.debutReponse).getTime();
            	  var df = new Date($scope.evaluation.finReponse).getTime();
            	  if ($scope.selectedEC == undefined)
            		  evaluationsFactory.set($scope.evaluation,undefined,$scope.selectedUE.uniteEnseignementPK.codeUe,$scope.selectedPromotion.promotionPK,$scope.enseignantConnecte,dd,df);    
            	  else {
            		  console.log($scope.selectedEC)
                	  evaluationsFactory.set($scope.evaluation,$scope.selectedEC.elementConstitutifPK.codeEc,$scope.selectedUE.uniteEnseignementPK.codeUe,$scope.selectedPromotion.promotionPK,$scope.enseignantConnecte,dd,df); 
            	  }
              $scope.edit = false;  
              $location.path('/ens/evaluations');
              logger.logSuccess("Insertion de l'evaluation réussie");
              }
	              
	              // annule l'ajout
	  		    $scope.cancel = function(){
	  		    		// retour a la page d'acceuil
	  				$location.path('/ens/evaluations'); 			
		  		}
		  		    
		  		$scope.onchange = function(){
		  			var UCpromise = evaluationsFactory.listeEC($scope.selectedUE.uniteEnseignementPK); 
	    	    	UCpromise.success(function(data,status){
	    	    		$scope.ecs = data ;
	    	    	}) .error(function(data,status){
	    	    		 console.log("impossible de recuperer la liste des UE");
	    	    	 });
		  			
		  		};
		  				  		
		  		$scope.onchangePro = function(){
		  			var promotionPk = $scope.selectedPromotion.promotionPK.codeFormation;
		  			var Uepromise = evaluationsFactory.ListeUE($scope.enseignantConnecte.noEnseignant,promotionPk); 
	    	    	Uepromise.success(function(data,status){ 	    		
	    	    		$scope.ues = data ;
	    	    	}) 
	    	    	 .error(function(data,status){
	    	    		 console.log("impossible de recuperer la liste des UE");
	    	    	});
		  		}
		  		
		  		$scope.edition = function(idEvaluation){
		  			$location.path('/ens/evaluations/'+idEvaluation);
		  		}
		  		
		  		$scope.$watch("ecs", function(val) {
		  			console.log("entrer watch ecs");
		  			if (val && $scope.selectedEC) {
			  			for (var i = 0 ; i< val.length ; i++) {
			  				console.log(val[i].elementConstitutifPK.codeEc);
			  				if ($scope.selectedEC.elementConstitutifPK.codeEc == val[i].elementConstitutifPK.codeEc ){
			  					$scope.selectedEC = val[i]; break ;
			  				}
			  					
			  			}
		  			}
		  		});
		  		
		  		
		  		$scope.$watch("ues", function(val) {
		  			console.log("entrer watch ues");
		  			if (val && $scope.selectedUE) {
			  			for (var i = 0 ; i< val.length ; i++) {
			  				if ($scope.selectedUE.uniteEnseignementPK.codeUe == val[i].uniteEnseignementPK.codeUe ){
			  					$scope.selectedUE = val[i]; break ;
			  				}
			  					
			  			};
		  			};
		  		});
		  		
		  		$scope.$watch("promotionevaluation", function(val) {
		  			console.log("entrer watch promotion");
		  			if (val && $scope.selectedPromotion) {
		  				console.log($scope.selectedPromotion)
		  				console.log("selected promotion"+$scope.selectedPromotion.promotionPK.codeFormation+"/"+$scope.selectedPromotion.promotionPK.anneeUniversitaire);
			  			for (var i = 0 ; i< val.length ; i++) {
			  				console.log("val promotion"+ val[i].promotionPK.codeFormation+ "/"+val[i].promotionPK.anneeUniversitaire);
			  				if ($scope.selectedPromotion.promotionPK.codeFormation == val[i].promotionPK.codeFormation && $scope.selectedPromotion.promotionPK.anneeUniversitaire == val[i].promotionPK.anneeUniversitaire ){
			  					$scope.selectedPromotion = val[i]; break ;
			  				}
			  					
			  			};
		  			};
		  		});	  		

		  		$scope.$watch("selectedPromotion", function(val) {
		  			console.log("entrer watch promotion");
		  			if (val && $scope.promotionevaluation) {
		  				console.log(val)
			  			for (var i = 0 ; i< $scope.promotionevaluation.length ; i++) {
			  				console.log("val promotion"+ val.promotionPK.codeFormation+ "/"+val.promotionPK.anneeUniversitaire);
			  				if (val.promotionPK.codeFormation == $scope.promotionevaluation[i].promotionPK.codeFormation 
			  						&& val.promotionPK.anneeUniversitaire == $scope.promotionevaluation[i].promotionPK.anneeUniversitaire ){
			  					$scope.selectedPromotion = $scope.promotionevaluation[i]; break ;
			  				}
			  					
			  			};
		  			};
		  		});	  		
    }]);
	app.controller('ModalInstanceRubriqueCtrl', [
	   	                              '$scope', '$route','$modalInstance', 'items', 'idEvaluation', 'evaluationsFactory','rubriqueService','questionService','logger',
	   	                              function($scope, $route, $modalInstance, items, idEvaluation, evaluationsFactory,rubriqueService,questionService,logger) {
	   	                            /*	'$scope', '$modalInstance', 'items', 'idEvaluation', 'evaluationsFactory', '$modalInstanceQuestion', 'idRubriqueEvaluation', 'itemsQuestEval',
		   	                              function($scope, $modalInstance, items, idEvaluation, evaluationsFactory, $modalInstanceQuestion, idRubriqueEvaluation, itemsQuestEval) {
		   	                              */
	   	                            	  $scope.items = items;
	   	                                $scope.idEvaluation = idEvaluation;
	   	                                $scope.ordre = null;
	   	                                $scope.selected = {
	   	                                  //item: $scope.items[0]
	   	                                };
	   	                                $scope.ok = function(ordrerubEval) {
	   	                                  //$scope.ordre = ordrerubEval;
	   	                                  $modalInstance.close($scope.selected.item);
	   	                                  console.log($scope.idEvaluation);
	   	                                  console.log($scope.idEvaluation + " "+ $scope.selected.item.idRubrique + " "+ $scope.selected.item.designation + " " + ordrerubEval);
	   	                                  //$scope.selected.item.designation
	   	                                  var designation = null;
	   	                                  var promiserubrique = evaluationsFactory.ajouterRubriqueEvalation($scope.idEvaluation,$scope.selected.item.idRubrique,ordrerubEval,designation);
	   	                                  promiserubrique.success(function(data,statut){
	   	                            	 
	   	                            	// recuperer les rubriques
	   	            	    	    	var rubpromise = evaluationsFactory.getRubriqueEvaluation($scope.idEvaluation);
	   	            	    	    	rubpromise.success(function(data){
	   	            	    	    		rubriqueService.rubriqueevaluation = data;
	   	            	    	    	}).error(function(data,status){
	   	            	    	    		console.log("impossible de recuperer les rubriques Evaluations de cette evaluation");
	   	            	    	    	});
		   	                        		//$scope.rubriqueevaluation.removeValue("idQuestionEvaluation",idQuestionEvaluation);
		   	                    			logger.logSuccess("Rubrique Evaluation ajouté avec succès");
		   	                        	})
		   	                        	.error(function(data,statut){
		   	                        		console.log("impossible d'ajouter la rubrique evaluation choisie");
		   	                    			logger.logError("Ajout impossible : " + data.message);
		   	                        	})
	   	                                 // $route.reload();
	   	                                };
	   	                                $scope.cancel = function() {
	   	                                   $modalInstance.dismiss("cancel");
	   	                                };
	   	                              }
	   	                            ]);
	
	app.controller('ModalInstanceQuestionCtrl', [
          '$scope', '$modalInstance', 'itemsQuestEval', 'idEvaluation', 'idRubriqueQuestionEvaluation' ,'evaluationsFactory','rubriqueService','questionService','logger',
          function($scope, $modalInstance, itemsQuestEval, idEvaluation, idRubriqueQuestionEvaluation ,evaluationsFactory,rubriqueService,questionService,logger) {
        /*	'$scope', '$modalInstance', 'items', 'idEvaluation', 'evaluationsFactory', '$modalInstanceQuestion', 'idRubriqueEvaluation', 'itemsQuestEval',
              function($scope, $modalInstance, items, idEvaluation, evaluationsFactory, $modalInstanceQuestion, idRubriqueEvaluation, itemsQuestEval) {
              */
        	$scope.itemsQuestEval = itemsQuestEval;
            $scope.idEvaluation = idEvaluation;
            $scope.idRubriqueQuestionEvaluation = idRubriqueQuestionEvaluation;
            $scope.ordre = null;
            $scope.selectedQuestion = {
              //itemsQuestEval: $scope.itemsQuestEval[0]
            };
            $scope.ok = function() {
              //$scope.ordre = ordrerubEval;
              $modalInstance.close($scope.selectedQuestion.itemQuestion);
              console.log($scope.idEvaluation);
              console.log("question evaluation " + $scope.selectedQuestion.itemQuestion);
              var ordreQuestion = 1;
              console.log($scope.idRubriqueQuestionEvaluation + " "+ $scope.selectedQuestion.itemQuestion.idQuestion + " "+ $scope.selectedQuestion.itemQuestion.intitule + " " + ordreQuestion);
              //$scope.selected.item.designation
              var intitule = null;
              var qualificatif = null;
              var promisequestion = evaluationsFactory.ajouterQuestionEvaluation($scope.idRubriqueQuestionEvaluation,$scope.selectedQuestion.itemQuestion.idQuestion,ordreQuestion,intitule,qualificatif)
              promisequestion.success(function(data,statut){
            	// recuperer les rubriques

    	    	var promiseQuestionRubrique = evaluationsFactory.getQuestionRubriqueEvaluation($scope.idRubriqueQuestionEvaluation);
    	    	promiseQuestionRubrique.success(function(data,status){
    	    		rubriqueService.rubriqueevaluation.retourValue('idRubriqueEvaluation',$scope.idRubriqueQuestionEvaluation).questionEvaluationCollection = data;
    	    	}).error(function(data,status){
    	    		console.log("impossible de recuperer la liste des questions");
    	    	});
	
        			logger.logSuccess("Question Evaluation ajouté avec succès");
            	})
            	.error(function(data,statut){
            		console.log("impossible d'ajouter la rubrique evaluation choisie");
        			logger.logError("Ajout impossible! : " + data.message);
            	})
            
            };
            $scope.cancel = function() {
              $modalInstance.dismiss("cancel");
            };
          }
       ]);
	
	app.controller('EvaluationsEtudiantController', 
		    ['$rootScope','$scope', '$routeParams','$filter','$location', 'evaluationsFactory','$http',
		    function($rootScope,$scope, $routeParams, $filter, $location, evaluationsFactory,$http){
		    	
		    	var noEtu= $rootScope.noEtudiant;
		    	
		    	$rootScope.$watch('noEtudiant',function() {
		    		console.log("no etudiant 2 :"+$rootScope.noEtudiant);
		    	});
		    	
		    	console.log("no etudiant :"+noEtu);
		    	
		    	//recuperer la promotion d'un etudiant
		    	var promoisePromotion = evaluationsFactory.findPromotionbyEtudiant(noEtu);
		    	promoisePromotion.success(function(data,status){
		    		$scope.promotion = data ;
		    		var init;
			    	//recuperer tous les evaluation qui concerne une promotion
			    	var promiseEvaluation = evaluationsFactory.findEvaluationPromotion($scope.promotion.promotionPK);
			    	promiseEvaluation.success(function(data,status){
			    		$scope.evaluations = data;
						
			    		$scope.searchKeywords = '';
			    		$scope.filteredEvaluation = [];
			    		$scope.row = '';
			    		$scope.select = function(page) {
			    			var end, start;
			    			start = (page - 1) * $scope.numPerPage;
			    			end = start + $scope.numPerPage;
			    			return $scope.currentPageEvaluation = $scope.filteredEvaluation.slice(start, end);
			    		};
			    		$scope.onFilterChange = function() {
			    			$scope.select(1);
			    			$scope.currentPage = 1;
			    			return $scope.row = '';
			    		};
			    		$scope.onNumPerPageChange = function() {
			    			$scope.select(1);
			    			return $scope.currentPage = 1;
			    		};
			    		$scope.onOrderChange = function() {
			    			$scope.select(1);
			    			return $scope.currentPage = 1;
			    		};
			    		$scope.search = function() {
			    			$scope.filteredEvaluation = $filter('filter')($scope.evaluations, $scope.searchKeywords);
			    			return $scope.onFilterChange();
			    		};
			    		$scope.order = function(rowName) {
			    			if ($scope.row === rowName) {
			    				return;
			    			}
			    			$scope.row = rowName;
			    			$scope.filteredEvaluation = $filter('orderBy')($scope.evaluations, rowName);
			    			return $scope.onOrderChange();
			    		};
			    		$scope.numPerPageOpt = [3, 5, 10, 20];
			    		$scope.numPerPage = $scope.numPerPageOpt[2];
			    		$scope.currentPage = 1;
			    		$scope.currentPageEvaluation = [];
			    		
			    		
			    		init = function() {
			    			$scope.search();
			    			return $scope.select($scope.currentPage);
			    		};
			    		return init();
			    	}).error(function(data,status){
			    		console.log("impossible de recuperer la liste des evaluations");
			    	});
		    		
		    	}).error(function(data,status){
		    		console.log("impossible de recuperer la promotion");
		    	}) ;
		    	
		    	
		    	$scope.showDetailsEtudEva = function(idEvaluation) {
		    		$location.path('/etudiant/evaluations/'+idEvaluation+"/details");
		    	}
		    	
		    }
		    ]);
	
	app.controller('EvaluationsEtudiantControllerDetails', 
		    ['$rootScope','$scope', '$routeParams','$filter','$location', 'evaluationsFactory','$http','rubriqueService','questionService',
		    function($rootScope,$scope, $routeParams, $filter, $location, evaluationsFactory,$http,rubriqueService,questionService){
		    	
		    	var noEtu= $rootScope.noEtudiant;
		    	
		    	$rootScope.$watch('noEtudiant',function() {
		    		console.log("no etudiant 2 :"+$rootScope.noEtudiant);
		    	});
		    	
		    	console.log("no etudiant :"+noEtu);
		    	

    	    	// si c'est pas nouveau : la modification
    	    	$scope.edit = true ;

    	    	// recupération de evaluation à modifier
    	    	var promoiseDetails = evaluationsFactory.get($routeParams.id);
    	    	promoiseDetails.success(function(data,status){
    	    		
    	    		$scope.evaluation = data; 
    	    			    	    		
		    		// recuperer la liste des etats
		    		var promiseEtatMeaning = evaluationsFactory.getMeaning($scope.evaluation.etat);
					promiseEtatMeaning.success(function(data,status){
						$scope.etatMeaning  = data;
						console.log("etatMeaning retourné : "+$scope.etatMeaning);
					});
					    	    		
					// recuperer l'ue d'une evaluation
		    		var promiseue = evaluationsFactory.getUEInEvaluation($scope.evaluation.codeFormation,$scope.evaluation.codeUe);
		    		promiseue.success(function(data,status){
		    			$scope.selectedUE = data;
		    			//$scope.onchange();
		    			console.log("ue retourné : "+$scope.selectedUE);
		    		}).error(function(data,status){
		    			console.log("impossible de recuperer la ue indiqué dans une evaluation");
		    		});
					
	    	    	// recuperer Ec d'une evaluation
		    		if ($scope.evaluation.codeEc != null){
		    			var promiseec = evaluationsFactory.getECInEvaluation($scope.evaluation.codeFormation,$scope.evaluation.codeUe,$scope.evaluation.codeEc);
		    			promiseec.success(function(data,status){
		    				$scope.selectedEC = data;
		    				console.log("ec retourné : "+$scope.selectedEC);
		    			}).error(function(data,status){
		    				console.log("impossible de recuperer la ue indiqué dans une evaluation");
		    			});
		    		} 
		    		
		    		// recuperer les rubriques
	    	    	var rubpromise = evaluationsFactory.getRubriqueEvaluation($scope.evaluation.idEvaluation);
	    	    	rubpromise.success(function(data){
	    	    		$scope.rubevaluation = data;
	    	    		console.log("rub Evaluation"+$scope.rubevaluation);
	    	    	}).error(function(data,status){
	    	    		console.log("impossible de recuperer les rubriqueEvaluatios de cette evaluation");
	    	    	});
	    	    	
	    	    	$scope.questionEvaluation = {};
	    	    	$scope.serviceRE = rubriqueService;
	    	    	$scope.serviceQE = questionService;
	    	    	// recuperer les rubriques de l'evaluation
        	    	var rubpromise = evaluationsFactory.getRubriqueEvaluation($scope.evaluation.idEvaluation);
        	    	rubpromise.success(function(data){
        	    		$scope.serviceRE.rubriqueevaluation = data;
        	    		console.log("qestion rubrique Evaluation "+$scope.questionEvaluation.intitule);
        	    	});
        	    	
        	    	
        	    	$scope.listeQuestionRubrique = function(idRubriqueEvaluation){
        	    		var questionRubEvaluationpromise = evaluationsFactory.getQuestionRubriqueEvaluation(idRubriqueEvaluation);
        		        questionRubEvaluationpromise.success(function(data){
        		    	$scope.questRubEvaluation=data;
        		        });
        	    	};
        	    	//affichage du panel hide show
        	    	$scope.oneAtATime = true;
    	    		
        	    	//affichage de rubrique dans modal	
                	// recuperer les rubriques
        	    	var rubriquepromise = evaluationsFactory.ListeRubrique();
        	    	rubriquepromise.success(function(data){
        	    		$scope.rubriqueevaluations = data;
        	    		console.log("rub Evaluation"+$scope.rubriqueevaluations);
        	    	}); 
                   
        	    	$scope.open = function(idEvaluation) {
                    	console.log(idEvaluation);
                    	$scope.idEvaluation = idEvaluation;
                      var modalInstance;
                      modalInstance = $modal.open({
                        templateUrl: "myModalContent.html",
                        controller: 'ModalInstanceRubriqueCtrl',
                        resolve: {
                          items: function() {
                            return $scope.rubriqueevaluations;
                          },
                          idEvaluation : function(){
                        	  return $scope.idEvaluation;
                          }
                        }
                      });
                      modalInstance.result.then((function(selectedItem) {
                        $scope.selected = selectedItem;
                      }), function() {
                        $log.info("Modal dismissed at: " + new Date());
                      });
                    };
        	    	//fin d'affichage

        	    	//affichage de rubrique dans modal        	    	
                    // recuperer les questions
        	    	var questionpromise = evaluationsFactory.ListeQuestion();
        	    	questionpromise.success(function(data){
        	    		$scope.questionEvaluations = data;
        	    		console.log("question Evaluation"+$scope.questionEvaluations);
        	    	});
        	    	
                    $scope.openQuestion = function(idEvaluation,idRubriqueEvaluation) {
                    	console.log(idEvaluation);
                    	console.log(idRubriqueEvaluation);
                    	$scope.idEvaluation = idEvaluation;
                    	$scope.idRubriqueQuestionEvaluation = idRubriqueEvaluation;
                      var modalInstance;
                      modalInstance = $modal.open({
                        templateUrl: "Question.html",
                        controller: 'ModalInstanceQuestionCtrl',
                        resolve: {
                          itemsQuestEval: function() {
                            return $scope.questionEvaluations;
                          },
                          idEvaluation : function(){
                        	  return $scope.idEvaluation;
                          },
                          idRubriqueQuestionEvaluation : function(){
                        	  return $scope.idRubriqueQuestionEvaluation;
                          }
                        }
                      });
                      modalInstance.result.then((function(selectedItemQuestion) {
                        $scope.selectedQuestion = selectedItemQuestion;
                      }), function() {
                        $log.info("Modal dismissed Question at: " + new Date());
                      });
                    };
                    
        	    	
        	    	
        	    	

    	    		   	    		
    	    	}).error(function(data,status){
    	    		 console.log("impossible de recuperer l'evaluation");
    	    	});
    	    	    	    	

	    		
               $scope.backEtudiant = function(){
            	   history.back();
               }
		    	
		    	
		    	
		    	
		    	
		    }]);
	
}).call(this);