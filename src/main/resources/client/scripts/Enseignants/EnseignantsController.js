(function() {
	'use strict';

	var app = angular.module('app.enseignants', []);

	// Methode utilisée en local pour supprimer un element d'une liste d'objets Json
	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; //clear original array
		this.push.apply(this, array); //push all elements except the one we want to delete
	}

	// Methode utilisée en local pour recuperer un element d'une liste d'objets Json
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	}
  
	// Factory Enseignant: Service partie Angular
	app.factory('enseignantsFactory', ['$http',function($http){		
		
		return {
			// renvoi la liste de tous les enseignants
			all:function() {
					return  $http.get('http://localhost:8090/enseignant/findallenseignant') 
				},
			// renvoi l'enseignant avec le no_enseignant demandé
			get: function(noEnseignant) {
				//return  $http.get('http://localhost:8090/enseignant/findEnseignantByNo/1');
					console.log("TODO : get enseignant",noEnseignant);
					return  $http.get('http://localhost:8090/enseignant/findenseignant/'+noEnseignant);
				},
			
			// fonction d'ajout et de modification d'un enseignant
			set: function(enseignant) {
				// Id de l'enseignant en cours de traitement
				var idx = enseignant.noEnseignant;
				
				// si modification d'un enseignant existant
				if(idx){
					console.log("TODO : update enseignant",idx);
					// La creation de l'enseignant à modifier 
					var newEnseignant = {
						"noEnseignant" : idx,
						"nom" : enseignant.nom,
						"prenom" : enseignant.prenom,
						"type" : enseignant.type,
						"sexe" : enseignant.sexe,
						"adresse" : enseignant.adresse,
						"codePostal" : enseignant.codePostal,
						"ville" : enseignant.ville,
						"pays" : enseignant.pays,
						"mobile" : enseignant.mobile,
						"telephone" : enseignant.telephone,
						"emailPerso" : enseignant.emailPerso,
						"emailUbo" : enseignant.emailUbo,
					};      	  
					return $http.post('http://localhost:8090/enseignant/updateenseignant',newEnseignant);
				}
				// si ajout d'un nouvel enseignant
				else {
					var newEnseignant = {
						"noEnseignant" : 0,
						"nom" : enseignant.nom,
						"prenom" : enseignant.prenom,
						"type" : enseignant.type,
						"sexe" : enseignant.sexe,
						"adresse" : enseignant.adresse,
						"codePostal" : enseignant.codePostal,
						"ville" : enseignant.ville,
						"pays" : enseignant.pays,
						"mobile" : enseignant.mobile,
						"telephone" : enseignant.telephone,
						"emailPerso" : enseignant.emailPerso,
						"emailUbo" : enseignant.emailUbo,
					};
					return $http.post('http://localhost:8090/enseignant/addenseignant',newEnseignant);
				}
			},
			
			delete: function(noEnseignant) { 
				// TODO Supprimer 
				console.log("TODO : supprimer enseignant",noEnseignant);
				return  $http.get('http://localhost:8090/enseignant/deleteenseignant/'+noEnseignant)
			},
			// renvoi le liste des promotions qui ont pour responsable l'enseignant avec le no_enseignant
			getPromotion : function(noEnseignant){
				var url = "http://localhost:8090/enseignant/findpromotionsbyenseignant/"+noEnseignant
				return $http.get(url);
			},
			// renvoi le liste des Unités d'enseignements qui ont pour responsable l'enseignant avec le no_enseignant
			getUE : function(noEnseignant){
				var url = "http://localhost:8090/enseignant/finduesbyenseignant/"+noEnseignant;
				return $http.get(url);
			},
			ListePays: function(){
			    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/PAYS')
			},
			getMeaning: function(rvLowValue){
			    return $http.get("http://localhost:8090/cgrefcodes/findmeaning/"+rvLowValue)
			},
			listeTypes: function(){
			    return $http.get("http://localhost:8090/cgrefcodes/findcgrefcodes/TYPE_ENSEIGNANT")
			} 
		};
	}]);

  
	//Controleur de la partie "lister les enseignants"
	app.controller('EnseignantsController', 
    ['$rootScope','$scope', '$filter','$location', 'enseignantsFactory','logger',
    function($rootScope,$scope, $filter, $location, enseignantsFactory, logger){
    	var init;
    	enseignantsFactory.all()
    		// dans le cas ou la requette arrive à s'executer
			.success(function(data) {
				$scope.enseignants = data;
				$scope.searchKeywords = '';
				$scope.enseignantSize = $scope.enseignants.length;
				$scope.filteredEnseignant = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageEnseignant = $scope.filteredEnseignant.slice(start, end);
				};
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredEnseignant = $filter('filter')($scope.enseignants, $scope.searchKeywords);
					return $scope.onFilterChange();
				};
				$scope.order = function(rowName) {
					if ($scope.row === rowName) {
						return;
					}
					$scope.row = rowName;
					$scope.filteredEnseignant = $filter('orderBy')($scope.enseignants, rowName);
					return $scope.onOrderChange();
				};
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageEnseignant = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();
			})
			// dans le cas ou la requette n'arrive pas à s'executer
			.error(function(data) {
				$scope.error = 'La recuperation de la liste des enseignants est impossible';
			});
    	$scope.testens = $rootScope.noenseignant;
    	
    	// la page permettant d'ajouter un enseignant
		$scope.ajoutEnseignant = function(){
			$location.path('/admin/enseignant/nouveau'); 
		}

		// la page permettant de modifier un enseignant
		$scope.updateEnseignant = function(noEnseignant){
			$location.path("/admin/enseignant/"+ noEnseignant);
		}
  
		// affiche les détail d'un enseignant
		// Lien vers la page permettant d'éditer un enseignant /admin/enseignant/ + no_enseignant
		$scope.consulter = function (noEnseignant){
			$location.path("/admin/enseignant/"+noEnseignant+"/details");
		}

		// supprime un enseignant
		$scope.supprime = function(noEnseignant){ 
			swal({
				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
				   imageSize: '50x50',
	    		   title: "Etes vous sûr de vouloir le supprimer?",
	    		   text: "La suppression est irréversible !",
	    		   type: "warning",
	    		   showCancelButton: true,
	    		   confirmButtonText: "Oui, confirmer!",
	    		   closeOnConfirm: true}, 
	    		   function(){ 
		 		   			var promise= enseignantsFactory.delete(noEnseignant);
		 		   			promise.success(function(data,statut){
		 		   				$scope.currentPageEnseignant.removeValue("noEnseignant",noEnseignant);
		 		   				$scope.enseignantSize=$scope.enseignantSize-1;
		 		   				$scope.filteredEnseignant.length=$scope.filteredEnseignant.length-1;
		 		   				logger.logSuccess("Enseignant supprimé avec succès");
		 		   			})
							.error(function(data,statut){
								logger.logError("Suppression impossible :" + data.message);
							});
 		   		}
			);
		}
    }]);

	//Controleur de la partie "consulter un enseignant"
	app.controller('EnsDetailsController', 
	['$scope', '$routeParams', '$location', 'enseignantsFactory','logger',
	function($scope, $routeParams, $location, enseignantsFactory, logger){      
		var typespromise = enseignantsFactory.listeTypes();
		typespromise.success(function(data,status){
    		$scope.types = data ; 
    	})
    	.error(function(data,status){
    		console.log("Impossible de recuperer les types d'enseignants") ;
    	}); 
		var payspromise = enseignantsFactory.ListePays();
		payspromise.success(function(data,status){
    		$scope.pays = data ; 
    	})
    	.error(function(data,status){
    		console.log("Impossible de recuperer la liste des pays") ;
    	});
		$scope.edit= false;
		$scope.updateEnseignant = function(noEnseignant){
			$location.path("/admin/enseignant/"+ noEnseignant);
		}
		// si creation d'un nouvel enseignant
		if($routeParams.id == "nouveau"){
			$scope.enseignant= { };
			$scope.edit= true;
		} 
		else if(($routeParams.details == "details")){ // sinon on edite un enseignant existant
			var promise = enseignantsFactory.get($routeParams.id);
			promise.success(function(data){
				$scope.enseignant = data;
				var promisePays = enseignantsFactory.getMeaning($scope.enseignant.pays);
				promisePays.success(function(data,statut){
					$scope.paysEnseignant = data ;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi (Pays)");
				});
				var promiseType = enseignantsFactory.getMeaning($scope.enseignant.type);
				promiseType.success(function(data,statut){
					$scope.typeEnseignant = data ;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi (Pays)");
				});
				var promise= enseignantsFactory.getPromotion($routeParams.id);
				promise.success(function(data,statut){
					if(data != 0)
						$scope.enseignant.promotions = data;
					else
						logger.logWarning("Cet enseignant n'est résponsable sur aucune promotion");
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi");
				});
				var promise= enseignantsFactory.getUE($routeParams.id);
					promise.success(function(data,statut){
						if(data != 0)
							$scope.enseignant.ue = data;
						else
							logger.logWarning("Cet enseignant n'est résponsable sur aucune unité d'enseignement");
					})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi");
				});
			})
			.error(function(data){
				console.log("impossible de recuperer les details de l'enseignant choisi");
			});
		}
		
		else{
			var promise = enseignantsFactory.get($routeParams.id);
			promise.success(function(data){
				$scope.enseignant = data;
				var promisePays = enseignantsFactory.getMeaning($scope.enseignant.pays);
				promisePays.success(function(data,statut){
					$scope.paysEnseignant = data ;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi (Pays)");
				});
				var promiseType = enseignantsFactory.getMeaning($scope.enseignant.type);
				promiseType.success(function(data,statut){
					$scope.typeEnseignant = data ;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi (Pays)");
				});
			})
			.error(function(data){
				console.log("impossible de recuperer les details de l'enseignant choisi");
			});
		}
		
		$scope.edition = function(){
			$scope.edit = true;
		}
  
		//valide le formulaire d'édition d'un enseignant
		$scope.submit = function(){    	 
			var promise = enseignantsFactory.set($scope.enseignant);        
			promise.success(function(data,statut){
				$scope.edit = false;
				logger.logSuccess("Succès : Insertion de l'enseignant réussie");
		        $location.path('/admin/enseignants'); 
   			})
			.error(function(data,statut){
				logger.logError("Impossible d'inserer l'enseignant");
			});
		}
		
		// annule l'édition
		$scope.cancel = function(){
				$location.path('/admin/enseignants');
		}      
    }]);
	
}).call(this);
