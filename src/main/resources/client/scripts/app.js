(function() {
	'use strict';
	
	
	var urlDefautParRole = {
			"ADM" : "/dashboardAdmin",
			"ENS" : "/dashboardENS",
			"ETU" : "/dashboardEtudiant"
	}
	
	var app = angular.module(
			'app',
			[ 'ngRoute', 'ngAnimate', 'ui.bootstrap', 'easypiechart',
					'mgo-angular-wizard', 'textAngular', 'ui.tree',
					'ngTagsInput','app.enseignants' , 
				    'app.promotions', 'app.rubriques',
					'app.formations', 'app.ue', 'app.ui.ctrls',
					'app.ui.directives', 'app.ui.services', 'app.controllers',
					'app.directives', 'app.form.validation',
					'app.ui.form.ctrls', 'app.ui.form.directives',
					'app.tables', 'app.task', 'app.localization',
					'app.chart.ctrls', 'app.chart.directives',
					'app.qualificatifs','app.questions','app.evaluations',
					'app.page.ctrls', 'app.auth' ]).config(
			[ '$routeProvider', function($routeProvider, $urlRouterProvider) {
				return $routeProvider.when('/', {
					redirectTo : '/dashboardAdmin'
				}).when('/admin/enseignants', {
					templateUrl : 'views/enseignants/list.html',
					rolesAutorises : ['ADM']
				}).when('/admin/enseignant/:id/:details', {
					templateUrl : 'views/enseignants/details.html',
					rolesAutorises : ['ADM']
				}).when('/admin/enseignant/:id', {
					templateUrl : 'views/enseignants/edit.html',
					rolesAutorises : ['ADM']
				}).when('/admin/formations', {
					templateUrl : 'views/formations/list.html',
					rolesAutorises : ['ADM']
				}).when('/admin/formation/:id/:details', {
					templateUrl : 'views/formations/details.html',
					rolesAutorises : ['ADM']
				}).when('/admin/formation/:id', {
					templateUrl : 'views/formations/edit.html',
					rolesAutorises : ['ADM']
				}).when('/admin/promotions', {
					templateUrl : 'views/promotions/list.html',
					rolesAutorises : ['ADM']
			    }).when( '/admin/promotion/:codeFormation/:anneeUniversitaire/details' , {
			    	templateUrl : 'views/promotions/details.html',
					rolesAutorises : ['ADM']
			    }).when( '/admin/promotion/:codeFormation/:anneeUniversitaire' , {
			    	templateUrl : 'views/promotions/edit.html',
					rolesAutorises : ['ADM']
			    }).when( '/admin/promotion/etudiant/:id/:codeFormation/:anneeUniversitaire/details' , {
			    	templateUrl : 'views/promotions/detailsEtudiant.html',
					rolesAutorises : ['ADM']
			    }).when( '/admin/promotion/etudiant/:id/:codeFormation/:anneeUniversitaire/edit' , {
			    	templateUrl : 'views/promotions/editEtudiant.html',
					rolesAutorises : ['ADM']
			     }).when('/admin/rubriques' , {
				    templateUrl : 'views/rubriques/list.html',
					rolesAutorises : ['ADM']
			     }).when('/admin/rubrique/:id', {
					templateUrl : 'views/rubriques/details.html',
					rolesAutorises : ['ADM']
				 }).when('/admin/qualificatifs' , {
				    templateUrl : 'views/qualificatifs/list.html',
					rolesAutorises : ['ADM']
			     }).when('/admin/qualificatifs/:id', {
					templateUrl : 'views/qualificatifs/details.html',
					rolesAutorises : ['ADM']
				 }).when('/admin/questions', {
					templateUrl : 'views/questions/list.html',
					rolesAutorises : ['ADM']
				}).when('/admin/question/:id', {
					templateUrl : 'views/questions/details.html',
					rolesAutorises : ['ADM']
				}).when('/admin/ue', {
					templateUrl : 'views/ue/list.html',
					rolesAutorises : ['ADM']
				}).when('/admin/ue/:id', {
					templateUrl : 'views/ue/details.html',
					rolesAutorises : ['ADM']
				}).when('/admin/ues/:id/:codeFormation', {
					templateUrl : 'views/ue/editUE.html',
					rolesAutorises : ['ADM']
				}).when('/admin/uesmodif/:codeFormation/:codeUe', {
					templateUrl : 'views/ue/editUE.html',
					rolesAutorises : ['ADM']
				}).when('/admin/uesdetail/:codeFormation/:codeUe/:detailsue', {
					templateUrl : 'views/formations/DetailsUE.html',
					rolesAutorises : ['ADM']
				}).when('/admin/ecmodif/:id/:codeUe/:codeEc', {
					templateUrl : 'views/ue/editEC.html',
					rolesAutorises : ['ADM']
				}).when('/admin/ec/:id/:codeFormation/:codeUe', {
					templateUrl : 'views/ue/editEC.html',
					rolesAutorises : ['ADM']
				}).when('/dashboardAdmin', {
					templateUrl : 'views/dashboard.html',
					rolesAutorises : ['ADM']
				}).when('/dashboardENS', {
					templateUrl : 'views/dashboardens.html',
					rolesAutorises : ['ENS']
				}).when('/dashboardEtudiant', {
					templateUrl : 'views/dashboardetudiant.html',
					rolesAutorises : ['ETU']
				}).when('/etudiant/evaluations', {
					templateUrl : 'views/evaluations/listEtudiant.html',
					rolesAutorises : ['ETU']
				}).when('/ui/typography', {
					templateUrl : 'views/ui/typography.html'
				}).when('/ui/buttons', {
					templateUrl : 'views/ui/buttons.html'
				}).when('/ui/icons', {
					templateUrl : 'views/ui/icons.html'
				}).when('/ui/grids', {
					templateUrl : 'views/ui/grids.html'
				}).when('/ui/widgets', {
					templateUrl : 'views/ui/widgets.html'
				}).when('/ui/components', {
					templateUrl : 'views/ui/components.html'
				}).when('/ui/timeline', {
					templateUrl : 'views/ui/timeline.html'
				}).when('/ui/nested-lists', {
					templateUrl : 'views/ui/nested-lists.html'
				}).when('/ui/pricing-tables', {
					templateUrl : 'views/ui/pricing-tables.html'
				}).when('/forms/elements', {
					templateUrl : 'views/forms/elements.html'
				}).when('/forms/layouts', {
					templateUrl : 'views/forms/layouts.html'
				}).when('/forms/validation', {
					templateUrl : 'views/forms/validation.html'
				}).when('/forms/wizard', {
					templateUrl : 'views/forms/wizard.html'
				}).when('/tables/static', {
					templateUrl : 'views/tables/static.html'
				}).when('/tables/responsive', {
					templateUrl : 'views/tables/responsive.html'
				}).when('/tables/dynamic', {
					templateUrl : 'views/tables/dynamic.html'
				}).when('/charts/others', {
					templateUrl : 'views/charts/charts.html'
				}).when('/charts/morris', {
					templateUrl : 'views/charts/morris.html'
				}).when('/charts/flot', {
					templateUrl : 'views/charts/flot.html'
				}).when('/mail/inbox', {
					templateUrl : 'views/mail/inbox.html'
				}).when('/mail/compose', {
					templateUrl : 'views/mail/compose.html'
				}).when('/mail/single', {
					templateUrl : 'views/mail/single.html'
				}).when('/pages/features', {
					templateUrl : 'views/pages/features.html'
				}).when('/pages/signin', {
					templateUrl : 'views/pages/signin.html',
					notLoggedNeeded : true
				}).when('/pages/signup', {
					templateUrl : 'views/pages/signup.html',
					notLoggedNeeded : true
				/*
				 * .when('/pages/signin', { templateUrl:
				 * 'views/pages/signin.html' }) .when('/pages/signup', {
				 * templateUrl: 'views/pages/signup.html'
				 */
				}).when('/pages/forgot', {
					templateUrl : 'views/pages/forgot-password.html'
				}).when('/pages/lock-screen', {
					templateUrl : 'views/pages/lock-screen.html'
				}).when('/pages/profile', {
					templateUrl : 'views/pages/profile.html'
				}).when('/404', {
					templateUrl : 'views/pages/404.html'
				}).when('/pages/500', {
					templateUrl : 'views/pages/500.html'
				}).when('/pages/blank', {
					templateUrl : 'views/pages/blank.html'
				}).when('/pages/invoice', {
					templateUrl : 'views/pages/invoice.html'
				}).when('/pages/services', {
					templateUrl : 'views/pages/services.html'
				}).when('/pages/about', {
					templateUrl : 'views/pages/about.html'
				}).when('/pages/contact', {
					templateUrl : 'views/pages/contact.html'
				}).when('/tasks', {
					templateUrl : 'views/tasks/tasks.html'
				}).when('/ens/evaluations/:id', {
					templateUrl : 'views/evaluations/edit.html',
					rolesAutorises : ['ENS']
			    }).when('/ens/evaluations/:id/details', {
					templateUrl : 'views/evaluations/details.html',
					rolesAutorises : ['ENS']
			    }).when('/etudiant/evaluations/:id/details', {
					templateUrl : 'views/evaluations/detailsEtudiantEvaluation.html',
					rolesAutorises : ['ETU']
			    }).when('/ens/evaluations', {
					templateUrl : 'views/evaluations/list.html',
					rolesAutorises : ['ENS']
			    }).when('/ens/rubriqueeval/:id/:noEvaluation', {
					templateUrl : 'views/evaluations/rubevaledit.html',
					rolesAutorises : ['ENS']
			    });/*
					 * .otherwise({ redirectTo: '/404' });
					 */
				
				$urlRouterProvider.otherwise(function($injector, $location) {
					var AuthService = $injector.get('AuthService');

					AuthService.getUser().success(function(data) {
						
						if (data) {
							var roleuser = data.role;
						    $location.path(urlDefautParRole[roleuser]);
							
						} else {
							$location.path("/pages/signin");
						}

					}).error(function(data) {
						$location.path("/pages/signin");
					});

				});
			} ]).run(function($rootScope, $route, $location, AuthService, logger) {
		$rootScope.$on("$routeChangeStart", function(e, to) {
			
			to.resolve = angular.extend( to.resolve || {}, {
		        authenticating: function($q) {
		        	var deferred = $q.defer();
		        	
		        	if (to.notLoggedNeeded) {
						return;
					}
					
					var promise = AuthService.getUser();
					var dataUser;
					promise.then(
							function(data) {
								if (data.data) {
									dataUser = data.data;
									var infouser = {
											"idConnection" : data.data.idConnection,
											"role" : data.data.role,
											"loginConnection" : data.data.loginConnection,
											"pseudoConnection" : data.data.pseudoConnection,
									};
									$rootScope.idConnection = data.data.idConnection;
									$rootScope.infouser = infouser;
									$rootScope.userole = data.data.role;
									
									if ($rootScope.userole == 'ENS'){
										return AuthService.getEns(data.data.idConnection);
									}
									else if ($rootScope.userole == 'ETU') {
										return AuthService.getEtud(data.data.idConnection);
									} else {
										console.log("role not supported");
										return;
									}
								} else {
									$location.path("/pages/signin");
								}
							},
							function() {
								$location.path("/pages/signin");
							}
					).then(
							function(data) {
								if ($rootScope.userole == 'ENS'){
									$rootScope.noEnseignant = data.data.noEnseignant;
									$rootScope.username = data.data.prenom;
								} else if ($rootScope.userole == 'ETU'){
									$rootScope.noEtudiant = data.data.noEtudiant;
									$rootScope.username = data.data.prenom;
								} else {
									console.log("role not supported");
								}
							},
							function() {
								$location.path("/pages/signin");
							}
					).then(
							function() {
								if(to.rolesAutorises && to.rolesAutorises.indexOf(dataUser.role) < 0 ) {
									logger.logError("Vous n avez pas le droit d acces à ces fonctionnalités");
									$location.path(urlDefautParRole[dataUser.role]);
								}
								deferred.resolve();
							}
					);
					return deferred.promise;
		        }
		    });

			
			
			
			
//			
//			AuthService.getUser().success(function(data) {
//				if (data) {
//					var infouser = {
//							"idConnection" : data.idConnection,
//							"role" : data.role,
//							"loginConnection" : data.loginConnection,
//							"pseudoConnection" : data.pseudoConnection,
//					};
//					$rootScope.idConnection = data.idConnection;
//					$rootScope.infouser = infouser;
//					$rootScope.userole = data.role;
//					
//			
//					
//					
//					if ($rootScope.userole == 'ENS'){
//						AuthService.getEns(data.idConnection).success(function(data) {
//							$rootScope.noEnseignant = data.noEnseignant;
//							$rootScope.username = data.prenom;
//						}).error(function(data) {
//							$location.path("/pages/signin");
//						});
//					}
//					else if ($rootScope.userole == 'ETU') {
//						AuthService.getEtud(data.idConnection).success(function(data) {
//							$rootScope.noEtudiant = data.noEtudiant;
//							$rootScope.username = data.prenom;
//						}).error(function(data) {
//							$location.path("/pages/signin");
//						});
//					} else {
//						console.log("role not supported");
//					}
//
//					if(to.rolesAutorises && to.rolesAutorises.indexOf(data.role) < 0 ) {
////						// TODO toast
////						toaster.pop({
////					        type: 'error',
////					        title: 'Refus d acces',
////					        body: 'Vous n avez pas le droit d acces à ces fonctionnalités',
////					        showCloseButton: true,
////					        closeHtml: '<button>Close</button>'
////						});
//						logger.logError("Vous n avez pas le droit d acces à ces fonctionnalités");
//						$location.path(urlDefautParRole[data.role]);
//					}
//					
//					
//				} else {
//					$location.path("/pages/signin");
//				}
//			}).error(function(data) {
//				$location.path("/pages/signin");
//			});
		});
	});

}).call(this);
