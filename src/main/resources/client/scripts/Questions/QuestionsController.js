(function() {
	'use strict';

	var app = angular.module('app.questions', []);

	// Methode utilisée en local pour supprimer un element d'une liste d'objets Json
	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; //clear original array
		this.push.apply(this, array); //push all elements except the one we want to delete
	}

	// Methode utilisée en local pour recuperer un element d'une liste d'objets Json
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	}
  
	// Factory Question: Service partie Angular
	app.factory('questionsFactory', ['$http',function($http){		
		
		 var list = [ 
		      // TODO Constituer la liste des questions ici
		      {
		    	  idQuestion : "1", type : "QUS", intitule : "Contenu", 
		    	  IdQualificatif: "1" ,Qualificatifmin :"Pauvre" ,Qualificatifmax :"Riche"
		    		  
		      },
		      {
		    	  idQuestion : "2", type : "QUS", intitule : "Intérêt", 
		    	  IdQualificatif:"2", Qualificatifmin :"Faible", Qualificatifmax :"Fort"
		      },
		      {
		    	  idQuestion : "3", type : "QUS", intitule : "Assimilité (Ce cours est-il facile à assimiler ?)", 
		    	  IdQualificatif:"8", Qualificatifmin :"Facile", Qualificatifmax :"Difficile"
		      }
		    ];
		 
		
		return {
			
			
			// renvoi la liste de tous les questions
			all: function() {
					//return list; 
					return $http.get('http://localhost:8090/question/findallquestion') 
				},
			// renvoi l'question avec le idQuestion demandé
			get: function(idQuestion) {
				//return  $http.get('http://localhost:8090/Question/findQuestionByNo/1');
					console.log("TODO : get question",idQuestion);
					return  $http.get('http://localhost:8090/question/findquestionbyid/'+idQuestion);
				},
			
			// fonction d'ajout et de modification d'un Question
			set: function(question) {
				// Id de l'question en cours de traitement
				var idx = question.idQuestion;
				
				// si modification d'un question existant
				if(idx){
					console.log("TODO : update question",idx);
					// La creation de l'question à modifier 
  
					var newQuestion = {
							"question":{"idQuestion":idx,"type":"QUS","intitule":question.intitule},
							"qualificatif":{"idQualificatif":question.Qualificatif}
							};
					return $http.post('http://localhost:8090/question/updatequestion',newQuestion);
				}
				// si ajout d'un nouvel question
				else {
					 //TODO ajouter un question à la liste

					var newQuestion = {
							"question":{"type":"QUS","intitule":question.intitule},
							"qualificatif":{"idQualificatif":question.Qualificatif}
							};
					return $http.post('http://localhost:8090/question/addquestion',newQuestion);
				}
			},
			
			delete: function(idQuestion) { 
				// TODO Supprimer 
				console.log("TODO : supprimer question",idQuestion);
				return  $http.get('http://localhost:8090/question/deletequestion/'+idQuestion)
			},
			listeQualificatif: function(){
				return $http.get('http://localhost:8090/qualificatif/findallqualificatif');
			},
			
		
		};
	}]);

  
	//Controleur de la partie "lister les questions"
	app.controller('QuestionsController', 
    ['$scope', '$filter','$location', 'questionsFactory','logger', 
    function($scope, $filter, $location, questionsFactory, logger){
    	var init;
    	var promisequestion = questionsFactory.all();
    		// dans le cas ou la requette arrive à s'executer
    	promisequestion.success(function(data) {
				$scope.questions = data;
				$scope.questionsSize=$scope.questions.length;
				$scope.searchKeywords = '';
				$scope.filteredQuestion = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageQuestion = $scope.filteredQuestion.slice(start, end);
				};
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredQuestion = $filter('filter')($scope.questions, $scope.searchKeywords);
					$scope.filteredQuestionSize=$scope.filteredQuestion.length;
					return $scope.onFilterChange();
				};
				$scope.order = function(rowName) {
					if ($scope.row === rowName) {
						return;
					}
					$scope.row = rowName;
					$scope.filteredQuestion = $filter('orderBy')($scope.questions, rowName);
					$scope.filteredQuestionSize=$scope.filteredQuestion.length;
					return $scope.onOrderChange();
				};
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageQuestion = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();
			})
			// dans le cas ou la requette n'arrive pas à s'executer
			.error(function(data) {
				$scope.error = 'La recuperation de la liste des question est impossible';
			});
    	    			
		$scope.ajoutQuestion = function(){
			$location.path('/admin/question/nouveau'); 
		}
  
		// affiche les détail d'une question
		// Lien vers la page permettant d'éditer un question /admin/question/ + no_question
		$scope.edit = function (idQuestion){
			$location.path("/admin/question/"+ idQuestion);
		}

		// supprime un question
		$scope.supprime = function(idQuestion){ 
			// TODO Suppression d'un question de la liste
			swal({
				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
				   imageSize: '50x50',
	    		   title: "Etes vous sûr de vouloir le supprimer?",
	    		   text: "La supression est irréversible !",
	    		   type: "warning",
	    		   showCancelButton: true,
	    		   confirmButtonText: "Oui, confirmer!",
	    		   closeOnConfirm: true}, 
	    		   function(){
						var promise= questionsFactory.delete(idQuestion);
						promise.success(function(data,statut){
							//$scope.question.promotions = data ;
							$scope.currentPageQuestion.removeValue("idQuestion",idQuestion);
							$scope.questionsSize=$scope.questions.length-1;
				        	$scope.filteredQuestionSize=$scope.filteredQuestion.length-1;
							logger.logSuccess("Question supprimée avec succès");
//							swal("Question supprimée avec succès");
						})
						.error(function(data,statut){
			//				console.log("impossible de supprimer la Question choisie");
							logger.logError("Suppression impossible : " + data.message);
//							swal("Supression impossible! : " + data.message);
						});
		    		});
		    	  		    	  
		      }
		    }]
		  );

	//Controleur de la partie "consulter un Question"
	app.controller('QuestionsDetailsController', 
	['$scope', '$routeParams', '$location', 'questionsFactory','logger',
	function($scope, $routeParams, $location, questionsFactory, logger){      
		$scope.edit= false;    
		// si creation d'un nouvel Question
		if($routeParams.id == "nouveau"){
			$scope.question= { }; 
			
				var promiseQualificatif = questionsFactory.listeQualificatif();
				promiseQualificatif.success(function(data,status){
					$scope.question.Qualificatif = 1;
		    		$scope.listqualificatif = data ; 
		    	})
	              .error(function(data,status){
	                    	console.log("impossible de recuperer la liste des qualificatifs") ;
	              }); 
			
			
			$scope.edit= true;    
		} 
		else { // sinon on edite un question existant
			var promise = questionsFactory.get($routeParams.id);
			promise.success(function(data){
				$scope.question = data ;
				$scope.question.Qualificatif = data.idQualificatif.idQualificatif;
				var promiseQualificatif = questionsFactory.listeQualificatif();
				promiseQualificatif.success(function(data,status){
		    		$scope.listqualificatif = data ; 
		    	})
	              .error(function(data,status){
	                    	console.log("impossible de recuperer la liste des qualificatifs") ;
	              }); 
			})
			.error(function(data){
				console.log("impossible de recuperer les details de la question choisi");
			});
		}
		
		$scope.edition = function(){
			$scope.edit = true;
		}
  
		//valide le formulaire d'édition d'un questiongnant
		$scope.submit = function(){    	 
			//questionsFactory.set($scope.question);   
			var promise = questionsFactory.set($scope.question);
			promise.success(function(data){
				$scope.question = data ;
				console.log(data.idQualificatif.idQualificatif);
				logger.logSuccess("Succès : Insertion de la question réussie");
				$location.path('/admin/questions');
//				swal("Succès!", "Insertion de la question réussie!", "success");
			})
			.error(function(data){
				console.log("impossible de recuperer les details de la question choisi");
				
			});
			//$scope.edit = false;    
//			logger.logSuccess("Succès!", "Insertion de la question réussie!");
		}
		
		// annule l'édition
		$scope.cancel = function(){
			// si ajout d'un nouvel question => retour à la liste des questions
			//if(!$scope.question.idQuestion){
				$location.path('/admin/questions');
			/*} 
			else {
				//var e = questionsFactory.get($routeParams.id);
				var promise = questionsFactory.get($routeParams.id);
				promise.success(function(data){
					$scope.question = data ;
					var promiseQualificatif = questionsFactory.listeQualificatif();
					promiseQualificatif.success(function(data,status){
			    		$scope.listqualificatif = data ; 
			    	})
		              .error(function(data,status){
		                    	console.log("impossible de recuperer la liste des qualificatifs") ;
		              }); 
				})
				.error(function(data){
					console.log("impossible de recuperer les details de la question choisi");
				});
				//$scope.question = JSON.parse(JSON.stringify(e));
				$scope.edit = false;
			}*/
		}      
    }]);
	
}).call(this);
