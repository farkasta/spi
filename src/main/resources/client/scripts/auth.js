angular.module('app.auth', [])

/**
 * A simple example service that returns some data.
 */
.factory('AuthService', function($http, $window) {

	return {
		authLocal : function(requestAuth) {
			config = {
				url : '/singin',
				method : "POST",
				data : requestAuth
			};
			return $http(config);
		},
		getUser : function() {
			config = {
				url : '/user',
				method : "GET"
			}
			return $http(config);
		},
		getEns : function(idConnection) {
			config = {
				url : '/user/enseignant/'+idConnection,
				method : "GET"
			}
			return $http(config);
		},
		getEtud : function(idConnection) {
			config = {
				url : '/user/etudiant/'+idConnection,
				method : "GET"
			}
			return $http(config);
		},
		deconnexion : function() {
			config = {
				url : '/singout',
				method : "GET"
			}
			return $http(config);
		}
	}

})

/**
 * Controleur pour la page d'authentification.
 */
.controller(
		'AuthenticationController',
		[ '$scope', '$location', '$animate', 'AuthService','logger',
				function($scope, $location, $animate, AuthService,logger) {
					$scope.login = {};
					//$scope.username = {};
					/*
					 * // Nom utilisateur et image (affichés dans le header)
					 * $scope.username = auth.username(); $scope.userimg = ""; //
					 * Réception de l'événement de login (voir le service
					 * 'auth') $scope.$on('login-success', function(){
					 * $scope.username = auth.username(); $scope.userimg =
					 * auth.userimg(); })
					 */

					// Executé lors du click sur le bouton de login
					this.submit = function() {
						var authuser = {
								"idConnection" : null,
								"role" : "",
								"loginConnection" : $scope.login.username,
								"pseudoConnection" : $scope.login.username,
								"motPasse" : $scope.login.password
														
							};
						AuthService.authLocal(authuser).success(function() {
							var promiseAuth = AuthService.getUser();
							promiseAuth.success(function(data){
								//console.log("avant promise");
								//$scope.username = data.pseudoConnection;
								//console.log("apres promise "+ $scope.username);
								var roleuser = data.role;
								if (roleuser=="ADM"){
								$location.path('/');
								}else if (roleuser=="ENS"){
									$location.path("/dashboardENS");
								}else if(roleuser=="ETU"){
									$location.path("/dashboardEtudiant")
								}else {
									$location.path("/pages/signin");
								}
							}).error(function(data,statut){
								console.log("impossible de recuperer les details de l'authentification");
							});
							
						})
						.error(function(data) {
							// si la connexion a échoué : "secoue" le formulaire
							// de connexion
							// TODO : afficher un message d'erreur de connexion
							var elt = angular.element('.form-container');
							$animate.addClass(elt, 'shake', function() {
								$animate.removeClass(elt, 'shake');
							});
							logger.logError(data.message);
							//logger.logError("impossible de recuperer les details de l'authentification");
						});
					}
				} ]);
;