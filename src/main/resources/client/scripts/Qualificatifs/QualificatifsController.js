(function() {
	'use strict';
	
	var app = angular.module('app.qualificatifs', []);
	
	// Methode utilisée en local pour supprimer un element d'une liste d'objets Json
	Array.prototype.removeValue = function(name, value){
		   var array = $.map(this, function(v,i){
		      return v[name] === value ? null : v;
		   });
		   this.length = 0; //clear original array
		   this.push.apply(this, array); //push all elements except the one we want to delete
		}
	// Methode utilisée en local pour recuperer un element d'une liste d'objets Json  
	Array.prototype.retourValue = function(name, value){
		   var array = $.map(this, function(v,i){
		      return v[name] === value ? v : null;
		   });
		   return array[0];
		}
	
	// Factory Qualificatif: Service partie Angular  	  
	app.factory('qualificatifsFactory', ['$http',function($http){
			       
		return {
			// renvoi la liste de tous les qualificatifs
			all: function(){
				return $http.get('http://localhost:8090/qualificatif/findallqualificatif'); 
			},
//				list,
				
			
			// renvoi le qualificatif avec l id qualificatif demandé
			get: function(idQualificatif) {
				
			//	console.log("TODO : get qualificatif",idQualificatif);
				return  $http.get('http://localhost:8090/qualificatif/findqualificatifbyid/'+idQualificatif);
			},
			
			
			// fonction d'ajout et de modification d'un qualificatif
			set: function(qualificatif) {
				// Id du qualificatif en cours de traitement
				var idx = qualificatif.idQualificatif;
				
				// si modification d'un qualificatif existant
				if(idx){
					//console.log("TODO : update qualificatif",idx);
					// La creation d'un qualificatif à modifier 
					var newQualificatif = {
						"idQualificatif" : qualificatif.idQualificatif,
						"minimal" : qualificatif.minimal,
						"maximal" : qualificatif.maximal
					};      	  
					$http.post('http://localhost:8090/qualificatif/updatequalificatif',newQualificatif);
				}
				// si ajout d'un nouvel qualificatif
				else {					
					var newQualificatif = {
							"idQualificatif" : 0, 
							"minimal" : qualificatif.minimal,
							"maximal" : qualificatif.maximal
					};      	  
					$http.post('http://localhost:8090/qualificatif/addqualificatif',newQualificatif);
				}
			},
				      
			delete: function(idQualificatif) { 
				console.log("TODO : supprimer qualificatif",idQualificatif);
		    	return  $http.get('http://localhost:8090/qualificatif/deletequalificatif/'+idQualificatif)
			}
		};
	}]);
	
	 //Controleur de la partie "lister les qualificatifs"
	  app.controller('QualificatifsController', 
			    ['$scope', '$filter','$location', 'qualificatifsFactory', 'logger',
			    function($scope, $filter, $location, qualificatifsFactory, logger,$confirm){
			    	var init;
			    	var promisequalificatif = qualificatifsFactory.all();
			    		// dans le cas ou la requette arrive à s'executer
			    	promisequalificatif.success(function(data) {
							$scope.qualificatifs = data;
							$scope.qualificatifsSize=$scope.qualificatifs.length;
							$scope.searchKeywords = '';
							$scope.filteredQualificatif = [];
							$scope.row = '';
							$scope.select = function(page) {
								var end, start;
								start = (page - 1) * $scope.numPerPage;
								end = start + $scope.numPerPage;
								return $scope.currentPageQualificatif = $scope.filteredQualificatif.slice(start, end);
							};
							$scope.onFilterChange = function() {
								$scope.select(1);
								$scope.currentPage = 1;
								return $scope.row = '';
							};
							$scope.onNumPerPageChange = function() {
								$scope.select(1);
								return $scope.currentPage = 1;
							};
							$scope.onOrderChange = function() {
								$scope.select(1);
								return $scope.currentPage = 1;
							};
							$scope.search = function() {
								$scope.filteredQualificatif = $filter('filter')($scope.qualificatifs, $scope.searchKeywords);
								$scope.filteredQualificatifSize = $scope.filteredQualificatif.length;
								return $scope.onFilterChange();
							};
							$scope.order = function(rowName) {
								if ($scope.row === rowName) {
									return;
								}
								$scope.row = rowName;
								$scope.filteredQualificatif = $filter('orderBy')($scope.qualificatifs, rowName);
								$scope.filteredQualificatifSize = $scope.filteredQualificatif.length;
								return $scope.onOrderChange();
							};
							$scope.numPerPageOpt = [3, 5, 10, 20];
							$scope.numPerPage = $scope.numPerPageOpt[2];
							$scope.currentPage = 1;
							$scope.currentPageQualificatif = [];
							init = function() {
								$scope.search();
								return $scope.select($scope.currentPage);
							};
							return init();
						})
						// dans le cas ou la requette n'arrive pas à s'executer
						.error(function(data) {
							$scope.error = 'La recuperation de la liste des qualificatifs est impossible';
						});

			      
			      // Créé la page permettant d'ajouter un qualificatif
			      // TODO Lien vers la page permettant de créer un qualificatif /admin/qualificatif/nouveau
			      $scope.addQualificatif = function(){
			          $location.path('/admin/qualificatifs/nouveau'); 
			       }
			      
			   // Créé la page permettant de modifier un qualificatif
			      // TODO Lien vers la page permettant de modifier un qualificatif /admin/qualificatif/nouveau
			      $scope.updateQualificatif = function(idQualificatif){
			          $location.path("/admin/qualificatifs/"+ idQualificatif); 
			       }

			      // supprime un qualificatif
			      $scope.supprime = function(qualificatif){ 
			    	// TODO Suppression d'un qualificatif de la liste
			    	  swal({
						   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
						   imageSize: '50x50',
			    		   title: "Etes vous sûr de vouloir le supprimer?",
			    		   text: "La suppression est irréversible !",
			    		   type: "warning",
			    		   showCancelButton: true,
			    		   confirmButtonText: "Oui, confirmer!",
			    		   closeOnConfirm: true}, 
			    		   function(){ 
						    	  var promise= qualificatifsFactory.delete(qualificatif.idQualificatif);
						          promise.success(function(data,statut){
						        	  console.log("success : suppression qualificatif");
						        	  $scope.currentPageQualificatif.removeValue("idQualificatif",qualificatif.idQualificatif);
						        	  $scope.qualificatifsSize=$scope.qualificatifs.length-1;
						        	  $scope.filteredQualificatifSize=$scope.filteredQualificatif.length-1;
						        	  logger.logSuccess("Qualificatif supprimé avec succès");
						        	  $scope.path("/admin/qualificatifs");
						          })
						          .error(function(data,statut){
						        	  logger.logError("Suppression impossible : " + data.message);
						          });
			    		   });        
		    	  
		      }
			    	       
		    }]);
	
	  app.controller('QualificatifDetailsController', 
			    ['$scope', '$routeParams', '$location', 'qualificatifsFactory','logger',
			    function($scope, $routeParams, $location, qualificatifsFactory, logger){      
			      $scope.edit= false;    

			      
			      // si creation d'un nouveau qualificatif
			      if($routeParams.id == "nouveau"){
			        $scope.qualificatif= { };
			        $scope.edit= true;   
			        
			      } 
			      else { 
			    // sinon on edite un qualificatif existant
			        var promise = qualificatifsFactory.get($routeParams.id);
//			        console.log($routeParams.id);
			        promise.success(function(data){
			    	  	 $scope.qualificatif = data ;
			        	 $scope.edit = true;
//			        	 logger.logSuccess("Qualificatif ajouté avec succès!");
			      	  
			        })
			        .error(function(data){
			      	  console.log("impossible de recuperer les details du qualificatif choisi");
			      	  
			        });
			      }


			      // valide le formulaire d'édition d'un qualificatif
			      $scope.submit = function(){    	 
			        qualificatifsFactory.set($scope.qualificatif);        
//			        $scope.edit = false; 
			        logger.logSuccess("Insertion du Qualificatif réussie");
			        $location.path('/admin/qualificatifs');
			      
			      }

			      // annule l'édition
			      $scope.cancel = function(){
			        // si ajout d'un nouveau qualificatif => retour à la liste des qualificatifs
			        if(!$scope.qualificatif.idQualificatif){
			          $location.path('/admin/qualificatifs');
			        } else {
			          var q = qualificatifsFactory.get($routeParams.id);
			          q.success(function(data) {
			        	  $scope.qualificatif = JSON.parse(JSON.stringify(data));
//			        	  $scope.edit = false;
			        	  $location.path('/admin/qualificatifs');
			          })
			          .error(function(data){
			        	  console.log("impossible de re-recuperer le qualificatif choisi");
			          });
			        }
			      }      
			    }]
			  );
	
}).call(this);