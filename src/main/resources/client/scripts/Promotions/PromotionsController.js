(function() {
	'use strict';

	var app = angular.module('app.promotions', []);
	
	// Methode utilisée en local pour supprimer un element d'une liste d'objets
	// Json
	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; // clear original array
		this.push.apply(this, array); // push all elements except the one we
										// want to delete
	}

	// Methode utilisée en local pour recuperer un element d'une liste d'objets
	// Json
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	}
  
	// Factory Promotion: Service partie Angular
	app.factory('promotionsFactory', ['$http',function($http){
	
		return {
			// renvoi la liste de tous les promotions
			all:function(){
				return $http.get('http://localhost:8090/Promotion/findAllPromotion'); 
			},
			
			// renvoi la promotion avec le promotionPK demandé
			get: function(codeFormation,anneeUniversitaire) {
				// return
				var promotionpk = {
						"codeFormation" : codeFormation,
						"anneeUniversitaire" : anneeUniversitaire, };
					
				console.log("TODO : get promotion",promotionpk);
				return  $http.post('http://localhost:8090/Promotion/findPromotion',promotionpk);
			},
			
			
			set: function(promotion) {
				console.log("set enseignant :"+promotion.noEnseignant);
				var enseignant = null ;
				if (promotion.noEnseignant){
					 enseignant =  "http://localhost:8090/enseignant/findenseignant/"+promotion.noEnseignant.noEnseignant;			
				}
				var newPromotion = {
						"promotionPK" : {
							"codeFormation" : promotion.promotionPK.codeFormation,
							"anneeUniversitaire" : promotion.promotionPK.anneeUniversitaire
						},
						"nbMaxEtudiant" : promotion.nbMaxEtudiant,
						"noEnseignant" : enseignant,
						"siglePromotion" : promotion.siglePromotion,
						"dateReponseLp" : promotion.dateReponseLp,
						"dateReponseLalp" : promotion.dateReponseLalp,
						"dateRentree" : promotion.dateRentree,
						"lieuRentree": promotion.lieuRentree,
						"processusStage" : promotion.processusStage,
						"commentaire" : promotion.commentaire
						
				}
				return $http.post('http://localhost:8090/promotions/addpromotion',newPromotion);
			},
			
			
			delete: function(promotionPK) { 
             return $http.post('http://localhost:8090/promotions/deletepromotion',promotionPK);

			},
			
          // return la liste des etudiants de la promotion concerner
			getEtudiant : function(codeFormation,anneeUniversitaire){
				var promotionpk = {
						"codeFormation" : codeFormation,
						"anneeUniversitaire" : anneeUniversitaire,
					};
				console.log("TODO : get promotion",promotionpk);
				
				return $http.post('http://localhost:8090/Promotion/findEtudiantsByPromotion',promotionpk);
				
			},
			
			getMeaning: function(rvLowValue){
				
			return $http.get('http://localhost:8090/cgrefcodes/findmeaning/'+rvLowValue);
			},
  
			// retourner les infos d'un étudiant
			getEtudiantInfo: function(noEtudiant) {
				
				console.log("TODO : get étudiant",noEtudiant);
				return  $http.get('http://localhost:8090/etudiant/findetudiant/'+noEtudiant);
			},
			
			ListePays: function(){
			    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/PAYS')
			},
			
			ListeUniv: function(){
			    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/UNIVERSITE')
			},
			
			ListePS: function(){
			    return $http.get('http://localhost:8090/cgrefcodes/findcgrefcodes/PROCESSUS_STAGE')
			},
			
			// supprimer un étudiant avec son numero etudiant
			deleteEtudiant: function(noEtudiant) { 
				// TODO Supprimer
				console.log("TODO : supprimer étudiant",noEtudiant);
				return  $http.get('http://localhost:8090/etudiant/delete/'+noEtudiant)
			},
			
			
			setEtudiant: function(etudiant,codeFormation,anneeUniversitaire) {
				var promotionPK = {
						"codeFormation" : codeFormation,
						"anneeUniversitaire" :anneeUniversitaire
				}
				var newObject = {
						"etudiant" : etudiant,
						"promotionPK" : promotionPK
				};
				console.log(newObject);
			  	  return $http.post('http://localhost:8090/etudiants/addetudiant',newObject);
			   },
		}
		
	}]);

	// Controller responsable sur la consultation de la table promotion
	app.controller('PromotionsController', 
		    ['$scope', '$filter','$location', 'promotionsFactory','logger',
		    function($scope, $filter, $location, promotionsFactory, logger){
		    	var init;
		    	promotionsFactory.all()
		    		// dans le cas ou la requette arrive à s'executer
					.success(function(data) {
						$scope.promotions = data;
						$scope.searchKeywords = '';
						$scope.filteredPromotion = [];
						$scope.row = '';
						$scope.select = function(page) {
							var end, start;
							start = (page - 1) * $scope.numPerPage;
							end = start + $scope.numPerPage;
							return $scope.currentPagePromotion = $scope.filteredPromotion.slice(start, end);
						};
						$scope.onFilterChange = function() {
							$scope.select(1);
							$scope.currentPage = 1;
							return $scope.row = '';
						};
						$scope.onNumPerPageChange = function() {
							$scope.select(1);
							return $scope.currentPage = 1;
						};
						$scope.onOrderChange = function() {
							$scope.select(1);
							return $scope.currentPage = 1;
						};
						$scope.search = function() {
							$scope.filteredPromotion = $filter('filter')($scope.promotions, $scope.searchKeywords);
							return $scope.onFilterChange();
						};
						$scope.order = function(rowName) {
							if ($scope.row === rowName) {
								return;
							}
							$scope.row = rowName;
							$scope.filteredPromotion = $filter('orderBy')($scope.promotions, rowName);
							return $scope.onOrderChange();
						};
						$scope.numPerPageOpt = [3, 5, 10, 20];
						$scope.numPerPage = $scope.numPerPageOpt[2];
						$scope.currentPage = 1;
						$scope.currentPagePromotion = [];
						init = function() {
							$scope.search();
							return $scope.select($scope.currentPage);
						};
						return init();
					})
					// dans le cas ou la requette n'arrive pas à s'executer
    		.error(function(data) {
    			 $scope.error = 'unable to get the poneys';
    		  }
    		);
    	    			

  
		// affiche les détail d'une promotion
		// TODO Lien vers la page permettant d'éditer une promotion
		 
    	       $scope.showDetails = function(promotion){
    	    	   var promotionpk = {
   						"codeFormation" : promotion.promotionPK.codeFormation,
   						"anneeUniversitaire" : promotion.promotionPK.anneeUniversitaire,
   					};
    	        $location.path("/admin/promotion/"+promotion.promotionPK.codeFormation+"/"+promotion.promotionPK.anneeUniversitaire+"/details");
    	      };
    	       
    	       
    	       $scope.ajoutPromotion = function(){
    	    	   $location.path("/admin/promotion/nouveau/nouveau");
    	       };
    	       
    	       $scope.supprime = function(promotionPK){

    	    	   swal({
    				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
    				   imageSize: '50x50',
    	    		   title: "Etes vous sûr de vouloir le supprimer?",
    	    		   text: "La supression est irréversible !",
    	    		   type: "warning",
    	    		   showCancelButton: true,
    	    		   confirmButtonText: "Oui, confirmer!",
    	    		   closeOnConfirm: true}, 
    	    		   function(){
			    	    	   var promiseDelete = promotionsFactory.delete(promotionPK);
			    	    	   promiseDelete.success(function(data,status){
			    	    		   console.log("suppression avec success");
			    	    		   $scope.currentPagePromotion.removeValue("promotionPK",promotionPK);
//			    	    		   swal("Etudiant supprimée avec succès");
			    	    		   logger.logSuccess("Promotion supprimée avec succès");
			    	    	   }).error(function(data,status){
			    	    		  console.log("impossible de supprimer cette promotion"); 
//			    	    		  swal("Supression impossible!" + data.message);
			    	    		  logger.logError("Suppression impossible : "+ data.message);
			    	    	   });
    	    		   });
    	       };
    	       
    	       $scope.edit = function(promotionPK){
    	    	   $location.path("/admin/promotion/"+promotionPK.codeFormation+"/"+promotionPK.anneeUniversitaire);
    	       };

		
	
    }]);

	
/*
 * Contoller responsable sur l'affichage du detail plus les etudiants de la
 * promotion
 */	
	app.controller('PromotionDetailsController', 
	['$scope','$filter', '$routeParams', '$location', 'promotionsFactory','logger','enseignantsFactory','formationsFactory',
	function($scope, $filter, $routeParams, $location, promotionsFactory, logger,enseignantsFactory,formationsFactory){      
		$scope.edit= false; 
		$scope.edition = function(promotionPK){
    	   $location.path("/admin/promotion/"+promotionPK.codeFormation+"/"+promotionPK.anneeUniversitaire);
		};
		$scope.date1;
		$scope.date2;
		$scope.date3;
		//recuperation de la liste des enseignants
		var promiseEnseignant = enseignantsFactory.all();
		promiseEnseignant.success(function(data,status){
			$scope.enseignants = data ;
		});
		
		//recuperation des formations
		var promoisePromotion = formationsFactory.all();
		promoisePromotion.success(function(data,status){
			$scope.formations = data;
		});
		
		//recuperation des valeurs domaine processus stage
		var promisePS = promotionsFactory.ListePS();
		promisePS.success(function (data,status){
			$scope.processusStage = data;
		});
		
		// si creation d'un nouvelle promotion		
		if($routeParams.codeFormation == "nouveau" && $routeParams.anneeUniversitaire == "nouveau" ){
			$scope.promotion= { };
			$scope.edit= true;    
		} 
		else { // sinon on edite une Promotion existante
			   //  
			
			  var promise = promotionsFactory.get($routeParams.codeFormation,$routeParams.anneeUniversitaire);
			  promise.success(function(data,status)
					  { 
				  var init;
				$scope.promotion = data ;
				console.log($scope.promotion.dateReponseLp);
			    $scope.promotion.dateReponseLp= new Date(data.dateReponseLp);
		  		$scope.promotion.dateReponseLalp= new Date(data.dateReponseLalp);
		  		$scope.promotion.dateRentree= new Date(data.dateRentree);
				 
				var PSpromise = promotionsFactory.getMeaning($scope.promotion.processusStage);
				PSpromise.success(function(data,status){
					$scope.PS = data;
				})
				.error(function(data,statut){
					console.log("impossible de recuperer les details de l'enseignant choisi (PS)");
				});
				  
			  var promiseEtudiants= promotionsFactory.getEtudiant($routeParams.codeFormation,$routeParams.anneeUniversitaire);
			  promiseEtudiants.success(function(data,status){
				    console.log(data);
					$scope.promotion.etudiants = data ;
					$scope.etudiantsSize = $scope.promotion.etudiants.length;
					$scope.searchKeywords = '';
					$scope.filteredEtudiant = [];
					$scope.row = '';
					$scope.select = function(page) {
						var end, start;
						start = (page - 1) * $scope.numPerPage;
						end = start + $scope.numPerPage;
						return $scope.currentPageEtudiant = $scope.filteredEtudiant.slice(start, end);
					};
					$scope.onFilterChange = function() {
						$scope.select(1);
						$scope.currentPage = 1;
						return $scope.row = '';
					};
					$scope.onNumPerPageChange = function() {
						$scope.select(1);
						return $scope.currentPage = 1;
					};
					$scope.onOrderChange = function() {
						$scope.select(1);
						return $scope.currentPage = 1;
					};
					$scope.search = function() {
						$scope.filteredEtudiant = $filter('filter')($scope.promotion.etudiants, $scope.searchKeywords);
						$scope.filteredEtudiantSize = $scope.filteredEtudiant.length;
						return $scope.onFilterChange();
					};
					$scope.order = function(rowName) {
						if ($scope.row === rowName) {
							return;
						}
						$scope.row = rowName;
						$scope.filteredEtudiant = $filter('orderBy')($scope.promotion.etudiants, rowName);
						$scope.filteredEtudiantSize = $scope.filteredEtudiant.length;
						return $scope.onOrderChange();
					};
					$scope.numPerPageOpt = [3, 5, 10, 20];
					$scope.numPerPage = $scope.numPerPageOpt[2];
					$scope.currentPage = 1;
					$scope.currentPageEtudiant = [];
					init = function() {
						$scope.search();
						return $scope.select($scope.currentPage);
					};
					return init();
				})
				.error(function(data,status){
					console.log("impossible de recuperer les details des etudiants choisi");
				});
			  
			  		// recuperation du Meaning
					var Payspromise = promotionsFactory.ListePays();
		  	    	Payspromise.success(function(data,status){
		  	    		$scope.paysEtudiant = data ; 
		  	    	}).error(function(data,status){
		              	console.log("impossible de recuperer le pays d'origine  de l'etudiant") ;
		            }); 
		  	    	
			  		$scope.$watch("enseignants", function(val) {
			  			console.log("entrer watch enseignants");
			  			console.log("promotion : "+$scope.promotion);
			  			if (val && $scope.promotion) {
				  			for (var i = 0 ; i< val.length ; i++) {
				  				console.log("boucle : "+val[i].noEnseignant);
				  				if ($scope.promotion.noEnseignant.noEnseignant == val[i].noEnseignant){
				  					$scope.promotion.noEnseignant = val[i];
				  					console.log($scope.promotion.noEnseignant+"  sort mnt");
				  					break ;
				  				}
				  					
				  			}
			  			}
			  		});
		  	    	
			  }).error(function(data){ 
				   console.log("impossible de recupererles details de la promotion choisi" ) ; 
			    });
		     }
  
			// valide le formulaire d'édition d'une promotion
			$scope.submit = function(){    	 
				
				var promiseAjout = promotionsFactory.set($scope.promotion);   
				promiseAjout.success(function(data,status){
				    $scope.promotion.dateReponseLp= new Date($scope.promotion.dateReponseLp).getTime();
			  		$scope.promotion.dateReponseLalp= new Date($scope.promotion.dateReponseLalp).getTime();
			  		$scope.promotion.dateRentree= new Date($scope.promotion.dateRentree).getTime();
					logger.logSuccess("Succès! Insertion de promotion réussie!");
					$scope.edit = false;        
					$location.path('/admin/promotions');
				}).error(function(data,status){
					logger.logError("Insertion impossible : " + data.message);
				});

			}
		
		    		
		
		   // annule l'édition
		    $scope.cancel = function(){
				history.back();
			}
		 
		    $scope.editEtudiant = function(noEtudiant,codeFormation,anneeUniversitaire){
		    	 console.log("edit",noEtudiant);
		          $location.path("/admin/promotion/etudiant/"+ noEtudiant+"/"+$scope.promotion.promotionPK.codeFormation+"/"+$scope.promotion.promotionPK.anneeUniversitaire+"/edit"); 
		       }
		 // affiche les détail d'un etudiant
			 
	       $scope.showDetails = function(noEtudiant,codeFormation,anneeUniversitaire){
	    	 console.log("avant details etudiant",noEtudiant);
	        $location.path("/admin/promotion/etudiant/"+noEtudiant+"/"+codeFormation+"/"+anneeUniversitaire+"/details");
	      }
	       
	       $scope.addEtudiants = function(codeFormation,anneeUniversitaire){
		          $location.path("/admin/promotion/etudiant/nouveau/"+codeFormation+"/"+anneeUniversitaire+"/edit"); 
		       }
	       
	       $scope.supprimeEtudiant = function(noEtudiant){ 
		    	// TODO Suppression d'un etudiantde la liste		
	    	   swal({
				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
				   imageSize: '50x50',
	    		   title: "Etes vous sûr de vouloir le supprimer?",
	    		   text: "La suppression est irréversible !",
	    		   type: "warning",
	    		   showCancelButton: true,
	    		   confirmButtonText: "Oui, confirmer!",
	    		   closeOnConfirm: true}, 
	    		   function(){ 
				    	  var promise= promotionsFactory.deleteEtudiant(noEtudiant);
				          promise.success(function(data,statut){
				        	  $scope.currentPageEtudiant.removeValue("noEtudiant",noEtudiant);
				        	  logger.logSuccess("Etudiant supprimé avec succès");
				        	  $scope.etudiantsSize=$scope.promotion.etudiants.length-1;
				        	  $scope.filteredEtudiantSize=$scope.filteredEtudiant.length-1;			        	  
				          })
				          .error(function(data,statut){
				        	  logger.logError("Suppression impossible : " + data.message);

				          });
	    		   });        
    	  
      }
	    	       
    }]);
	
	
	/*
	 * Controller pour les étudiants
	 */	
		app.controller('EtudiantDetailsController',
		['$scope','$filter', '$routeParams', 'logger' , '$location', 'promotionsFactory',
		function($scope, $filter, $routeParams, logger, $location, promotionsFactory){      
				//$scope.showDetails= false;    	
			 $scope.editEtudiant = function(noEtudiant,codeFormation,anneeUniversitaire){
		    	 console.log("edit",noEtudiant);
		          $location.path("/admin/promotion/etudiant/"+ noEtudiant+"/"+codeFormation+"/"+anneeUniversitaire+"/edit"); 
		       }
			//recuperer le Pays d'un etudiant
	    	var Payspromise = promotionsFactory.ListePays();
	    	Payspromise.success(function(data,status){
	    		$scope.paysEtudiant = data ; 
	    	})
              .error(function(data,status){
                    	console.log("impossible de recuperer le pays d'origine  de l'etudiant") ;
              }); 
	    	
	    	//recuperer l'univ origine d'un etudiant
	    	var Univpromise = promotionsFactory.ListeUniv();
	    	Univpromise.success(function(data,status){
	    		console.log("listeUniv", data);
	    		$scope.univOrigine = data ; 
	    	})
              .error(function(data,status){
                    	console.log("impossible de recuperer l'univ d'origine  de l'etudiant") ;
              }); 
	    	
	    	
			if($routeParams.id == "nouveau"){
				$scope.etudiant= { }; 	
				$scope.codeFormation = $routeParams.codeFormation;
				$scope.anneeUniversitaire = $routeParams.anneeUniversitaire;
				$scope.numetudiant = null;
				$scope.edit= true;  
				
 
    	    	
			}else{
				var Etudiantpromise = promotionsFactory.getEtudiantInfo($routeParams.id);
		        console.log("routeParams",$routeParams.id);
		        Etudiantpromise.success(function(data){
		        	$scope.codeFormation = $routeParams.codeFormation;
					$scope.anneeUniversitaire = $routeParams.anneeUniversitaire;
		    	  	 $scope.etudiant = data ;
		    	  	 $scope.etudiant.dateNaissance = new Date(data.dateNaissance);
		    	  	var promisePays = promotionsFactory.getMeaning($scope.etudiant.paysOrigine);
					promisePays.success(function(data,statut){
						$scope.paysOrigineEtudiant = data ;
					})
					.error(function(data,statut){
						console.log("impossible de recuperer les details de l'étudiant choisi (Pays d'origine)");
					});
					var promiseUniversite = promotionsFactory.getMeaning($scope.etudiant.universiteOrigine);
					promiseUniversite.success(function(data,statut){
						$scope.universiteEtudiant = data ;
					})
					.error(function(data,statut){
						console.log("impossible de recuperer les details de l'étudiant choisi (Universite d'origine)");
					});
		    	  	$scope.numetudiant = data.noEtudiant;
		        	 $scope.showDetails = true;	    
		        	 console.log("sexe etu"+$scope.etudiant.sexe);
		        })
		        .error(function(data){
		      	  console.log("impossible de recuperer les details du etudiant choisi");
		      	  
		        });
				
			} 

			// valide le formulaire d'édition d'une promotion
			$scope.submit = function(){    
				console.log("en submit + "+$scope.etudiant.dateNaissance);
				 var d = new Date($scope.etudiant.dateNaissance).getTime();
				 $scope.etudiant.dateNaissance = d ;
				 console.log("en submit 2 + "+d);
				var promiseEtudAjout =  promotionsFactory.setEtudiant($scope.etudiant,$scope.codeFormation,$scope.anneeUniversitaire);  
				promiseEtudAjout.success(function(data,status){
					logger.logSuccess("Succès : Insertion de l'étudiant réussie");
					console.log("ajout success");
					history.back();
				});
				$scope.showDetails = false;        
			}
			
			
			   // annule l'édition
			   // annule l'ajout
  		    $scope.cancel = function(){
  		    		// retour a la page d'acceuil 	 
  				$location.path('/admin/promotion');
  			
	  		}
  		    
  		    $scope.backPromotion = function(){
  		    	history.back();
  		    }
			   
		 
	    }]);

	
	
	
}).call(this);
