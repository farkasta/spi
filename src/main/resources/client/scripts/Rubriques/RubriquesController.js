(function() {
	'use strict';

	var app = angular.module('app.rubriques', []);

	// Methode utilisée en local pour supprimer un element d'une liste d'objets Json
	Array.prototype.removeValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? null : v;
		});
		this.length = 0; //clear original array
		this.push.apply(this, array); //push all elements except the one we want to delete
	}

	// Methode utilisée en local pour recuperer un element d'une liste d'objets Json
	Array.prototype.retourValue = function(name, value){
		var array = $.map(this, function(v,i){
			return v[name] === value ? v : null;
		});
		return array[0];
	}
  
	// Factory Rubriques: Service partie Angular
	app.factory('rubriquesFactory', ['$http',function($http){		
//		var rub = {
//			"type" : "RBS",
//			"designation" : "Cours",
//			"ordre" : "2"
//		};
//		var list = [
//		    {
//			"type" : "RBS",
//			"designation" : "Cours",
//			"ordre" : "2"},
//			{
//				"type" : "RBS",
//				"designation" : "TD",
//				"ordre" : "5"}
//		];
//		
//		var types = [
//            {"type" : "RBS"},
//			{"type" : "RBP"}
//       	];
		
		return {
			// renvoi la liste de toutes les rubriques
			all:function() {
					return  $http.get('http://localhost:8090/rubrique/findallrubrique') 
			},
			// renvoi la rubrique avec le id_rubrique demandé
			get:function(idRubrique) {
					console.log("TODO : get rubrique",idRubrique);
					return  $http.get('http://localhost:8090/rubrique/findrubrique/'+idRubrique);
			},
//			getTest:rub,
//			getTypes:types,
				
			// fonction d'ajout et de modification d'une rubrique
			set: function(rubrique) {
				// Id de la rubrique en cours de traitement
				var idx = rubrique.idRubrique;
				
				// si modification d'une rubrique existante
				if(idx){
					console.log("TODO : update rubrique",idx);
					// La creation de la rubrique à modifier 
					var newRubrique = {
						"idRubrique" : idx,
						"designation" : rubrique.designation,
						"type" : "RBS",//rubrique.type,
						"ordre" : rubrique.ordre
					};      	  
					return $http.post('http://localhost:8090/rubrique/updaterubrique',newRubrique);
				}
				// si ajout d'une nouvelle rubrique
				else {
					 //TODO ajouter une rubrique à la liste
					var newRubrique = {
						"designation" : rubrique.designation,
						"type" : "RBS",//rubrique.type,
						"ordre" : rubrique.ordre
					};      	  
					return $http.post('http://localhost:8090/rubrique/addrubrique',newRubrique);
				}
			},
			
			delete: function(idRubrique) { 
				// TODO Supprimer 
				console.log("TODO : supprimer rubrique",idRubrique);
				return  $http.get('http://localhost:8090/rubrique/deleterubrique/'+idRubrique)
			}
		};
	}]);

  
	//Controleur de la partie "lister les rubriques"
	app.controller('RubriquesController', 
    ['$rootScope','$scope', '$filter','$location', 'rubriquesFactory', 'logger',
    function($rootScope,$scope, $filter, $location, rubriquesFactory, logger){
    	var init;
    	
    	var idConnection = $rootScope.idConnection;
    	rubriquesFactory.all()
    		// dans le cas ou la requette arrive à s'executer
			.success(function(data) {
				$scope.rubriques = data;
				$scope.rubriquesSize=$scope.rubriques.length;
				$scope.searchKeywords = '';
				$scope.filteredRubriques = [];
				$scope.row = '';
				$scope.select = function(page) {
					var end, start;
					start = (page - 1) * $scope.numPerPage;
					end = start + $scope.numPerPage;
					return $scope.currentPageRubriques = $scope.filteredRubriques.slice(start, end);
				};
				$scope.onFilterChange = function() {
					$scope.select(1);
					$scope.currentPage = 1;
					return $scope.row = '';
				};
				$scope.onNumPerPageChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.onOrderChange = function() {
					$scope.select(1);
					return $scope.currentPage = 1;
				};
				$scope.search = function() {
					$scope.filteredRubriques = $filter('filter')($scope.rubriques, $scope.searchKeywords);
					$scope.filteredRubriquesSize = $scope.filteredRubriques.length;
					return $scope.onFilterChange();
				};
				$scope.order = function(rowName) {
					if ($scope.row === rowName) {
						return;
					}
					$scope.row = rowName;
					$scope.filteredRubriques = $filter('orderBy')($scope.rubriques, rowName);
					$scope.filteredRubriquesSize = $scope.filteredRubriques.length;
					return $scope.onOrderChange();
				};
				$scope.numPerPageOpt = [3, 5, 10, 20];
				$scope.numPerPage = $scope.numPerPageOpt[2];
				$scope.currentPage = 1;
				$scope.currentPageRubriques = [];
				init = function() {
					$scope.search();
					return $scope.select($scope.currentPage);
				};
				return init();
			})
			// dans le cas ou la requette n'arrive pas à s'executer
			.error(function(data) {
				$scope.error = 'La recuperation de la liste des rubrique est impossible';
			});
//    	$scope.rubriques = rubriquesFactory.allTEst;
		
    	$scope.ajoutRubrique = function(){
			$location.path('/admin/rubrique/nouveau'); 
		}

		// affiche les détail d'une rubrique
		// Lien vers la page permettant d'éditer une rubrique /admin/rubrique/ + id_rubrique
		$scope.edit = function (idRubrique){
			$location.path("/admin/rubrique/"+ idRubrique);
		}

		// supprime un rubrique
		$scope.supprime = function(idRubrique){ 
			// TODO Suppression d'une rubrique de la liste
			swal({
				   imageUrl: "https://cdn3.iconfinder.com/data/icons/ose/Warning.png",
				   imageSize: '50x50',
	    		   title: "Etes vous sûr de vouloir le supprimer?",
	    		   text: "La suppression est irréversible !",
	    		   type: "warning",
	    		   showCancelButton: true,
	    		   confirmButtonText: "Oui, confirmer!",
	    		   closeOnConfirm: true}, 
	    		   function(){ 
						var promise= rubriquesFactory.delete(idRubrique);
						promise.success(function(data,statut){
							$scope.currentPageRubriques.removeValue("idRubrique",idRubrique);
							$scope.rubriquesSize=$scope.rubriques.length-1;
				        	$scope.filteredRubriquesSize=$scope.filteredRubriques.length-1;
							logger.logSuccess("Rubrique supprimée avec succès");
//							swal("Rubrique supprimée avec succès");
						})
						.error(function(data,statut){
							console.log("impossible de supprimer la rubrique choisie");
							logger.logError("Suppression impossible : " + data.message);
//							swal("Rubrique supprimée avec succès");
						});
	    		   });
		}
    }]);

	//Controleur de la partie "consulter une rubrique"
	app.controller('RubriqueDetailsController', 
	['$rootScope','$scope', '$routeParams', '$location', 'rubriquesFactory', 'logger',
	function($rootScope ,$scope, $routeParams, $location, rubriquesFactory, logger){    
		var idConnection = $rootScope.idConnection;
		$scope.edit= false;    
		// si creation d'une nouvelle rubrique
		if($routeParams.id == "nouveau"){
			$scope.rubrique= { };
			$scope.edit= true;
		} 
		else { // sinon on edite une rubrique existante
			var promise = rubriquesFactory.get($routeParams.id);
			promise.success(function(data){
				$scope.rubrique = data ;
				$scope.edit = true;
//				if($scope.rubrique.type == "RBS")
//					$scope.typeRubrique = "Rubrique standard";
//				else
//					$scope.typeRubrique = "Rubrique personnalisée";
			})
			.error(function(data){
				console.log("impossible de recuperer les details de la rubrique choisie");
			});
//			$scope.rubrique = rubriquesFactory.getTest;
		}
		
		$scope.edition = function(){
			$scope.edit = true;
		}
  
		//valide le formulaire d'édition d'une rubrique
		$scope.submit = function(){
//			var promise = 
			rubriquesFactory.set($scope.rubrique);  
//			promise.success(function(data){
//				$scope.rubrique = data ;
//				swal("Succès!", "Insertion de la rubrique réussie!", "success");
				$location.path('/admin/rubriques');
				logger.logSuccess("Succès : Insertion de la rubrique réussie");
//			})
//			.error(function(data){
//				console.log("impossible de recuperer les details de la rubrique choisi");
//				
//			});
//			$scope.edit = false;   

		}
		
		// annule l'édition
		$scope.cancel = function(){
			// si ajout d'une nouvelle rubrique => retour à la liste des rubriques
			if(!$scope.rubrique.idRubrique){
				$location.path('/admin/rubriques');
			}
			else {
				var r = rubriquesFactory.get($routeParams.id);
				r.success(function(data) {
					$scope.rubrique = JSON.parse(JSON.stringify(data));
//					$scope.edit = false;
					$location.path('/admin/rubriques');
				})
				.error(function(data){
					console.log("impossible de re-recuperer la rubrique choisie");
				});
			}
		}      
    }]);
	
}).call(this);
